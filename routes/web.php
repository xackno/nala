<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();

//###################para catalogo############################
Route::get("/catalogo","catalogoController@index");
Route::post('/catalogo','catalogoController@search_for_catalogo')->name('buscar_catalogo');
Route::post('/busqueda','catalogoController@buscar_opciones')->name('buscar_opciones');
Route::post("/buscar_articulo","catalogoController@buscar_articulo");


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'verified'])->group(function () {

    //para creditos
    Route::post("/credito","creditosController@completar_credito");    

    //ir a view cajas
    Route::get("/cajas","generalController@administrar_cajas");
    Route::post("/registrar_caja","generalController@registrar_caja");
    Route::post("/actualizar_caja","generalController@actualizar_caja");
    Route::post("/eliminar_caja","generalController@eliminar_caja");


  //registrar usuarios
    Route::get("/list-user","generalController@listar_usuarios");
    Route::get("/registrar", "generalController@registrar"); //para mostrar la vista de regsitrar
    // Route::post("register", "Auth\RegisterController@register")->name("register"); //ejecutar form
    Route::post("register", "generalController@registrar_user")->name("register"); //ejecutar form
    Route::post("/actualizar_user","generalController@actualizar_user");
    Route::get("/eliminar_user/{id}","generalController@eliminar_user");

    //para salidas
    Route::post("/storesalida", "generalController@storesalidas");
    Route::post("/buscarsalidas", "generalController@buscarsalidas");
    Route::post("/eliminarsalida", "generalController@eliminarsalida");
    Route::post("/editar_salida", "generalController@editar_salida");
    //para turnos
    Route::post("/storeturno", "generalController@storeturno");
    Route::post("/cerrarturno", "generalController@cerrarturno");

  
    //para venta
    Route::get("/ventas", "ventasController@index");
    Route::post("/ejecutar_venta", "ventasController@ejecutar_venta");
    Route::post("/cotizacion", "ventasController@cotizacion");
    Route::post("/buscar_venta", "ventasController@buscar_venta"); //buscar ventas por id
    Route::post("/devolucion", "ventasController@devolucion");
    Route::post("/last_venta","ventasController@data_last_venta");
    Route::post("/reimprimir","ventasController@reimprimir");

    //para articulos##############
    Route::get("/articulos", "articulosController@index");
    Route::get("/buscar_articulos","articulosController@buscar_articulos");
    Route::get("/registrar_articulo","articulosController@registrar_articulo");
    Route::get("/editar_articulo/{id}","articulosController@edit");
    Route::post("/update_articulo/{id}","articulosController@update");
    Route::get("/listar_articulos","articulosController@listar_articulos");
    
    Route::post("articulo", "articulosController@store")->name('store_articulo');
    Route::get("buscarArticulo", "articulosController@buscarArticulos")->name('buscarArtiulos');
    Route::post("/buscar_x_codigo", "articulosController@buscar_x_codigo");

    Route::post("buscarExistencia", "articulosController@buscarExistencia")->name('buscarExistencia');
    Route::post("updateExistenca", "articulosController@updateExistenca")->name('updateExistenca');
    Route::post("eliminarArticulo", "articulosController@eliminarArticulo")->name("eliminarArticulo");

    Route::post("marcas", "articulosController@store_marca")->name("store_marca");
    Route::post("provedor", "articulosController@store_provedor")->name("store_provedor");

    Route::post('/uploadImg','articulosController@uploadImg');

    Route::get('/storeCategoria','articulosController@createCategoria')->name('storeCategoria');
    Route::get('/storeLinea','articulosController@createLinea')->name('storeLinea');
    Route::get('/storeMarca','articulosController@createMarca')->name('storeMarca');


    //################clientes##############
    Route::get("/clientes", "clientesController@index");
    Route::post("cliente", "clientesController@store")->name("storeCliente");
    Route::get("/edit_clientes/{id}","clientesController@edit_clientes");
    Route::post("/actualizar-cliente","clientesController@actualizar");
    Route::post("destroycliente", "clientesController@destroy")->name("destroyCliente");
    Route::post("showCliente", "clientesController@showCliente")->name("showCliente");
    Route::post("/updateCliente", "clientesController@update");
    Route::post('/uploadimgCliente','clientesController@uploadimgCliente');
    //-------------------------------RUTAS-----------------------------------------------------
    Route::get("/rutas","clientesController@rutas");
    Route::post("/guardar_ruta","clientesController@guardar_ruta")->name('guardar_ruta');
    Route::post("/actualizar_ruta","clientesController@actualizar_ruta")->name('actualizar_ruta');
    Route::post("/see_ruta","clientesController@see_ruta")->name('see_ruta');
    Route::post("/edit_ruta","clientesController@edit_ruta")->name('edit_ruta');
    Route::post("/destroy_ruta","clientesController@destroy_ruta")->name('destroy_ruta');
    

    //------------------para pedidos--------------------------
    Route::get('/pedidos', 'pedidosController@index')->name('pedidos');
    Route::get("/nuevo_pedido","pedidosController@nuevo_pedido");
    Route::post("/buscar_cliente","pedidosController@buscar_clientes");
    Route::post("/buscar_clientes_x_id","pedidosController@buscar_clientes_x_id");
    Route::post("/generando_pedido","pedidosController@generando_pedido");
    Route::post("/eliminar_pedido","pedidosController@destroy");
    Route::post("/editar_info_pedido","pedidosController@editar_info_pedido");
    Route::post("/change_status","pedidosController@change_status");
    Route::get("/retomar/{id}","pedidosController@retomar");
    Route::post("/guardar_cambios_pedido","pedidosController@guardar_cambios_pedido");
    Route::get("/imprimir_ticket/{id}","pedidosController@imprimir");

    Route::get('/verPedido/{id}', 'pedidosController@verPedido')->name('verPedido');
    Route::get('/paraPedidos', 'ventasController@paraPedidos')->name('paraPedidos');
    Route::post('/crearpedido', 'pedidosController@crearpedido')->name('crearpedido');
    Route::post("actualizar-pedido",'pedidosController@actualizar')->name('actualizar-pedido');
    Route::post("/verProdutosPedido",'pedidosController@verProPedido')->name("verProdutosPedido");
    
    

    
    //buscar cliente para nuevo pedido--------------------------
    Route::post("/buscarClientes",'clientesController@buscarClientes')->name("buscarClientes");





    //####################para informes############################
    Route::get("/movimientos", "informesController@movimientos");
    Route::get("/salidas", "informesController@salidas");
    Route::get("/turnos", "informesController@turnos");
    Route::get("/lista_venta", "informesController@ventas");
    //####################para marcas########################
    Route::post("/update_marca", "articulosController@update_marca");
    Route::post("/delete_marca", "articulosController@delete_marca");
    //#####################para provedor#############################
    Route::post("/update_provedor", "articulosController@update_provedor");
    Route::post("/delete_provedor", "articulosController@delete_provedor");
    //////#################informes#############################
    Route::get("/informes","HomeController@get_informes");
    Route::get("/informes_venta","ventasController@informes_ventas");


    //para ofertas#################################
    Route::resource('ofertas','ofertasController');
    Route::post('/crearOferta','ofertasController@createOferta')->name('crearOferta');
    Route::post('/buscarProducto','ofertasController@buscarProducto')->name('buscarProducto');


    //#####################para movimientos#######################

    Route::get("/movimientos","movimientosController@index");
    Route::post("/buscar_info_cliente","movimientosController@buscar_info_cliente");
    Route::get("/load_movimientos/{id}","movimientosController@load_movimientos");
    Route::get("/get_por_cobrar/{id}","movimientosController@get_por_cobrar");

    //#########################para abono###################################
    Route::post("/abono","movimientosController@abono");
    Route::get("movimiento_pdf/{id}/{tipo}","movimientosController@pdf");
    Route::get("/movimiento_excel/{id}/{tipo}","movimientosController@excel");
    Route::post("/liquidar","movimientosController@liquidar");
    //#####################para turnos""########################################
    Route::get("/imprimir_corte_turno/{id}","generalController@imprimir_corte_turno");
    Route::post("/total_venta","ventasController@total_venta_x_turno");

    //###############################BD#############################################
    Route::get("/bd","generalController@bd");
    Route::post("/update_database","generalController@update_database");

    //#############################inventarios#########################
    Route::get("/inventarios","inventariosController@index");
    Route::post("/buscar_en_inventario","inventariosController@buscar_en_inventario");

});



Route::get("/verificador","verificadorController@index")->name("verificador");
Route::post("verificador","verificadorController@buscar")->name("buscar");;
