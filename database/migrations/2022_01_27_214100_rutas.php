<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rutas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_ruta');
            $table->integer('longitud_ruta')->nullable();
            $table->string('tiempo_recorrido_total')->nullable();
            $table->string('origen');
            $table->string('destino');
            $table->text('id_secciones')->nullable();
            $table->text('nombre_secciones')->nullable();
            $table->text('tiempo_secciones')->nullable();
            $table->text('latitud_seccion')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutas');
    }
}
