<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ofertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_producto')->unique();
            $table->string("unidad");
            $table->text('descripcion_producto');
            $table->decimal('precio_venta',10,2);
            $table->decimal('precio_oferta',10,2);
            $table->date('fecha_valido');
            $table->string('foto')->nullable();
            $table->text('url')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}
