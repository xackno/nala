<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Articulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave')->nullable();
            $table->text('descripcion_articulo');
            $table->string('unidad');
            $table->decimal('inv_min',10,2)->nullable();
            $table->decimal('inv_max',10,2)->nullable();
            $table->decimal('existencia_bodega',10,2)->nullable();
            $table->decimal('existencia_stock',10,2)->nullable();
            $table->decimal('inv_compra',10,2)->nullable();

            $table->string('codigo_barra')->nullable();
            $table->string('codigo_sat')->nullable();
            $table->string('a_granel')->nullable();
            $table->string('en_catalogo')->nullable();
            $table->string('ventas_negativas')->nullable();

            $table->decimal('precio_caja',10,2)->nullable();
            $table->decimal('num_paquete',10,2)->nullable();
            $table->decimal('pza_x_caja',10,2)->nullable();
            $table->decimal('mayoreo_apartir',10,2)->nullable();

            $table->decimal('ganancia_unidad',10,2)->nullable();
            $table->decimal('ganancia_caja',10,2)->nullable();
            $table->decimal('ganancia_mayoreo',10,2)->nullable();
            $table->decimal('ganancia_membrecia',10,2)->nullable();

            $table->decimal('pre_compra',10,2);
            $table->decimal('pre_unidad',10,2);
            $table->decimal('pre_caja',10,2)->nullable();
            $table->decimal('pre_paquete',10,2)->nullable();
            $table->decimal('pre_mayoreo',10,2)->nullable();
            $table->decimal('pre_membrecia',10,2)->nullable();

            
            $table->integer('proveedor')->nullable();
            $table->integer('categoria')->nullable();
            $table->integer('linea')->nullable();
            $table->integer('marca')->nullable();
            
            $table->integer('local')->nullable();
            $table->text('ubicacion_producto')->nullable();
            $table->text('palabra_clave')->nullable();
            $table->date('caducidad')->nullable();
            $table->text('url')->nullable();
            $table->text('fotos')->nullable();
            $table->text('descripcion_catalogo')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
