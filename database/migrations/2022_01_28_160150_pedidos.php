<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_cliente')->nullable();
            $table->string("cajero",25)->nullable();
            $table->string("caja",20)->nullable();
            $table->string("turno",20)->nullable();
            $table->text("referencia")->nullable();
            $table->integer('id_ruta')->nullable();
            $table->string('status',25)->nullable();
            $table->string('status_recuperacion',25)->nullable();
            $table->text('historial_status')->nullable();
            $table->string("factura")->nullable();
            $table->text("nota")->nullable();

            $table->decimal('subtotal_pedido', 12, 2);//precio de todos los productos
            $table->decimal('costo_envio', 12, 2)->nullable();
            $table->decimal('pago_inicial', 12, 2)->nullable();
            $table->string('descuento',5)->nullable();//aplica a un descuento de 20pesos
            $table->integer("interes")->nullable();
            $table->date("fecha_limite")->nullable();

            $table->text("status_disminucion");
            $table->text("id_articulos");
            $table->text("cantidad");
            $table->text("unidad");
            $table->text("descripcion");
            $table->text("precio_vendido");
            $table->text("tipo_precio_vendido");
            $table->text("subtotal_articulos");

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
