<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('clientes', function (Blueprint $table) {
            $table->id("id");
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('municipio');
            $table->string('localidad');
            $table->string('calle')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('usuarioApp')->nullable();
            $table->string('password')->nullable();
            $table->string('status')->nullable();
            $table->string('foto')->nullable();
            $table->string('nivel')->nullable();
            $table->integer('calificacion')->nullable();
            $table->decimal('monedero',10,2)->nullable();
            $table->string('identificacion')->nullable();
            $table->integer('id_ruta')->nullable();
            $table->decimal('limite_credito',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
