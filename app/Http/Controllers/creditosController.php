<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\clientes;
class creditosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function completar_credito(Request $data){
        date_default_timezone_set('america/mexico_city');
        $id_p        = $data->get("credito_id_p");
        $cantidad    = $data->get("credito_cantidad");
        $unidad      = $data->get("credito_unidad");
        $descripcion = $data->get("credito_descripcion");
        $precio      = $data->get("credito_precio");
        $subtotal    = $data->get("credito_subtotal");
        $turno=      $data->get("turno");

        $id_p        = explode(",", $id_p);
        $cantidad    = explode(",", $cantidad);
        $unidad      = explode(",", $unidad);
        $precio      = explode(",", $precio);
        $subtotal    = explode(",", $subtotal);
        $descripcion = explode("=>", $descripcion);
        $clientes=clientes::all();
        return view('forms.make_creditos',compact('id_p','cantidad','unidad','precio','subtotal','descripcion','turno','clientes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
