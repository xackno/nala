<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ofertas;
use App\Models\articulos;
class ofertasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="OFERTAS";
        $ofertas=ofertas::all();
         return view('forms.ofertas',compact('title','ofertas'));
    }

    public function createOferta( Request $request)
    {
        if ($request['id_producto'] == !null && $request['descripcion_producto'] ==!null) {
            $this->validate($request,[ 'id_producto'=>'required', 'descripcion_producto'=>'required']);
            ofertas::create($request->all());
            return back()->with('success','Se agregó a ofertas.');
        }else{
            return back()->with('error','Error al agregar.');
        }
        
    }

    public function buscarProducto(Request $request){
        $busqueda=$request->input('busqueda');

        // $productos=$busqueda;
    $productos=articulos::select('articulos.id as id_p','clave','descripcion_articulo','unidad','precio_caja','pre_compra','pre_unidad','pre_caja','pre_paquete','pre_mayoreo','pre_membrecia','codigo_sat', 'existencia','ubicacion_producto','fotos','codigo_barra','caducidad','descripcion_catalogo','articulos.updated_at')
        ->orWhere('descripcion_articulo','LIKE', "%$busqueda%")
        ->orWhere('palabra_clave','LIKE', "%$busqueda%")
        ->orWhere('clave','LIKE', "$busqueda")
        ->orWhere('codigo_barra','LIKE', "$busqueda")
        ->orWhere('caducidad','LIKE', "$busqueda")
        ->orWhere('marca','LIKE', "%$busqueda%")
        ->get();
    
    return json_encode($productos);
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto=ofertas::find($id);
        $producto->delete();
        return back()->with('success','Eliminado correctamente');
    }
}
