<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\articulos;
use App\Models\pedidos;
use App\Models\productos_pedidos;
use App\Models\clientes;
use Illuminate\Support\Facades\DB;
use App\Models\Rutas;
use App\Models\turnos;
use App\Models\Cajas;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class pedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="LOGISTICA DE PEDIDOS";
        $pedidos=pedidos::orderBy("id","DESC")->get();

        foreach($pedidos as $r){
            $cliente=Clientes::find($r->id_cliente);
            $r->nombre=$cliente->nombre;
            $r->apellidos=$cliente->apellidos;
            $r->telefono=$cliente->telefono;
            $r->correo=$cliente->correo;
            $r->localidad=$cliente->localidad;
            $r->municipio=$cliente->municipio;
            if(isset($cliente->id_ruta)){
                $ruta=rutas::find($cliente->id_ruta);
                $r->ruta=$ruta->nombre_ruta;
            }else{
                $r->ruta='';
            }
        }
        $guardados=$pedidos->where("status","guardado");
        $apartados=$pedidos->where("status","apartado");
        $surtiendo=$pedidos->where("status","surtiendo");
        $revision=$pedidos->where("status","revision");
        $pendiente=$pedidos->where("status","pendiente");

        $devolucion_total=$pedidos->where("status","devolucion_total");
        $recibido_con_devolucion=$pedidos->where("status","recibido_con_devolucion");
        $recibido_x_pagar=$pedidos->where("status","recibido_x_pagar");
        $enviado_x_cobrar=$pedidos->where("status","enviado_x_cobrar");
        $recibido_y_pagado=$pedidos->where("status","recibido_y_pagado");
        $enviado_y_pagado=$pedidos->where("status","enviado_y_pagado");
        $pagado_x_enviar=$pedidos->where("status","pagado_x_enviar");


        return view('forms.pedidos.pedidos',compact('title','pedidos','guardados','apartados','surtiendo','revision','pendiente','devolucion_total','recibido_con_devolucion','recibido_x_pagar','enviado_x_cobrar','recibido_y_pagado','enviado_y_pagado','pagado_x_enviar'));
    }

    public function nuevo_pedido(){
        if(Auth::User()->caja!=null){
            $title="Nuevo pedido";
            $turno = turnos::where("usuario", "=", Auth::user()->id)
                ->where("status", "=", "abierto")
                ->orderBy("id", "DESC")
                ->get();

            $caja=0;
            $user=User::find(Auth::user()->id);
            $caja=$user->caja;
            if(isset($turno[0])){
                $turno=$turno[0]->id; 
                 
            }else{$turno=0;}

            return view("forms.pedidos.nuevo_pedido",compact('title','caja','turno')); 
        }else{
             return back()->with("error","Necesita esta agregado a una caja e iniciar turno para poder hacer esta operación.");
        }
         
    }


    public function buscar_clientes(Request $data){
        $buscar=$data->Get('buscador');

        $clientes=Clientes::where(DB::raw("CONCAT(nombre,' ',apellidos)"),"like","%$buscar%")
        ->orWhere("identificacion",$buscar)
        ->get();

        return json_encode($clientes);

    }
    public function buscar_clientes_x_id(Request $data){
        $buscar=$data->Get('buscador');

        $clientes=Clientes::where("id",$buscar)
        ->get();

        return json_encode($clientes);

    }


    public function generando_pedido(Request $e){//solo guarda
        date_default_timezone_set('america/mexico_city');
        $cliente=clientes::find($e->get("id_cliente"));

        if(!isset($cliente->id_ruta)){
            $id_ruta=0;
        }else{
            $id_ruta=$cliente->id_ruta;
        }
        $id_art=$e->get("id_articulos");
        $status_disminucion=[];
        foreach($id_art as $r){
            array_push($status_disminucion,0);
        }
        try {
            \DB::beginTransaction();
            $pedido = new pedidos();
            $pedido->fill([
                'id_cliente'=>$e->get("id_cliente"),
                "cajero"=>$e->get("cajero"),
                "caja"=>$e->get("caja"),
                "turno"=>$e->get("turno"),
                "referencia"=>$e->get("referencia"),
                'id_ruta'=>$id_ruta,
                'status'=>"guardado",
                'status_recuperacion'=>"pendiente",
                'historial_status'=>"guardado%".now(),
                "factura"=>"no",
                "nota"=>$e->get("nota"),

                'subtotal_pedido'=>$e->get("subtotal_pedido"),//precio de todos los productos
                'costo_envio'=>$e->get("envio"),
                'pago_inicial'=>$e->get("pago"),
                'descuento'=>$e->get("descuento"),//aplica a un descuento de 20pesos
                'interes'=>$e->get("interes"),
                'fecha_limite'=>$e->get("fecha_limite"),

                'status_disminucion'=>implode(",",$status_disminucion),
                "id_articulos"=>implode(",",$e->get("id_articulos")),
                "cantidad"=>implode(",",$e->get("cantidad")),
                "unidad"=>implode(",",$e->get("unidad")),
                "descripcion"=>implode(",",$e->get("descripcion")),
                "precio_vendido"=>implode(",",$e->get("precio_vendido")),
                "tipo_precio_vendido"=> implode(",",$e->get("tipo_precio")),
                "subtotal_articulos"=> implode(",",$e->get("total_articulos"))
            ]);
            $pedido->push();
            \DB::commit();
            $id_pedido=$pedido->id;
            $id_pedido=str_pad($id_pedido, 8, '0', STR_PAD_LEFT);
            $status="success";
        } catch (Exception $e) {
            $status="error";
            $id_pedido=0;
        }
         

         $array=[$status,$id_pedido];
         return json_encode($array);      
    }


 public function change_status(Request $data){
        date_default_timezone_set('america/mexico_city');
        $id_pedido=$data->get("id");
        $status=$data->get("status");
        $pedido=pedidos::find($id_pedido);
        try {
            if($status=="surtiendo"){
                $new_status_disminucion= $this->status_surtiendo($id_pedido);
            }else{
                $new_status_disminucion=$pedido->status_disminucion;
                $new_status_disminucion=explode(",", $new_status_disminucion);
            }
            
            $pedido->update(
                [
                "status"=>$status,
                "historial_status"=>$pedido->historial_status.",".$status."%".now(),
                "status_disminucion"=>implode(",",$new_status_disminucion)
                ]
            );
            $status="success";
            return json_encode($status);
        } catch (Exception $e) {
            $status="fail";
            return json_encode($status);
        }
    }

    function status_surtiendo($id_pedido){
        $pedido=pedidos::find($id_pedido);

        $cantidad=explode(",",$pedido->cantidad);
        $id_p=explode(",", $pedido->id_articulos);
        $status_disminucion=explode(",", $pedido->status_disminucion);
        for($x=0;$x<count($id_p);$x++){//$completa el array de status_disminucion en 0 si se vuelve a a gregar mas articulos al pedido
            if(!isset($status_disminucion[$x])){
                array_push($status_disminucion,0);
            }
        }
        $new_status_disminucion=[];
        $x=0;
        foreach($id_p as $r){//recorre los articulos para hacer la resta 
            if($status_disminucion[$x]==0 || $pedido->status_disminucion==null){//si aun no se ha restado en existencia
                $articulo=articulos::find($r);
                $articulo->update(["existencia_stock"=>$articulo->existencia_stock-$cantidad[$x]]);
                array_push($new_status_disminucion,1);
            }else{
                array_push($new_status_disminucion,$status_disminucion[$x]);
            }
            
           $x++;
        }
            
       return $new_status_disminucion;
    }

    public function destroy(Request $data){

        $pedido=pedidos::find($data->get('id'));
        try {
            $pedido->delete();
            $status="success";
            return json_encode($status);
        } catch (Exception $e) {
            $status="error";
            return json_encode($status);
        }
    }

    public function editar_info_pedido(Request $data){
        $id=$data->get("id_pedido");
        $pedido=pedidos::find($id);
        $pedido->update($data->all());
        return back()->with("success","Pedido #".str_pad($id, 8, '0', STR_PAD_LEFT)." modificado correctamente.");
    }

    public function retomar($id){
        if(Auth::User()->caja!=null){
            $pedido=pedidos::find($id);
            $cliente=clientes::find($pedido->id_cliente);
            $title="Retomar #".str_pad($id, 8, '0', STR_PAD_LEFT);
            $turno = turnos::where("usuario", "=", Auth::user()->id)
                ->where("status", "=", "abierto")
                ->orderBy("id", "DESC")
                ->get();
            $caja=0;
            $user=User::find(Auth::user()->id);
            $caja=$user->caja;
            if(isset($turno[0])){
                $turno=$turno[0]->id; 
                 
            }else{$turno=0;}

            return view("forms.pedidos.retomar",compact("title",'pedido',"caja","turno","cliente"));
        }else{
            return back()->with("error","Necesita esta agregado a una caja e iniciar turno para poder hacer esta operación.");
        }  
    }

    public function guardar_cambios_pedido(Request $e){
        $id_pedido=$e->get("id_pedido");
        $pedido=pedidos::find($id_pedido);
        try {
            $pedido->update(
            [
                'id_cliente'=>$e->get("id_cliente"),
                "cajero"=>$e->get("cajero"),
                "caja"=>$e->get("caja"),
                "turno"=>$e->get("turno"),
                "referencia"=>$e->get("referencia"),
                // 'status'=>"guardado",
                'historial_status'=>$e->get("status"),
                "factura"=>"no",
                "nota"=>$e->get("nota"),

                'subtotal_pedido'=>$e->get("subtotal_pedido"),//precio de todos los productos
                'costo_envio'=>$e->get("costo_envio"),
                'pago_inicial'=>$e->get("pago_inicial"),
                'descuento'=>$e->get("descuento"),//aplica a un descuento de 20pesos

                "id_articulos"=>implode(",",$e->get("id_articulos")),
                "cantidad"=>implode(",",$e->get("cantidad")),
                "unidad"=>implode(",",$e->get("unidad")),
                "descripcion"=>implode(",",$e->get("descripcion")),
                "precio_vendido"=>implode(",",$e->get("precio_vendido")),
                "tipo_precio_vendido"=> implode(",",$e->get("tipo_precio_vendido")),
                "subtotal_articulos"=> implode(",",$e->get("subtotal_articulos"))
            ]
        );
            $status="success";
        } catch (Exception $e) {
            $status="error";
        }
        $array=[$status,str_pad($id_pedido, 8, '0', STR_PAD_LEFT)];
         return json_encode($array);
    }

    public function imprimir($id){
        date_default_timezone_set('america/mexico_city');
        $pedido=pedidos::find($id);
        echo $pedido->id;
        $cliente=clientes::find($pedido->id_cliente);
        $ruta=$cliente->id_ruta;
        if(isset($ruta->nombre_ruta)){
            $nombre_ruta=$ruta->nombre_ruta;
        }else{
            $nombre_ruta="no especificado";
        }

        $por_pagar=(floatval($pedido->subtotal_pedido)+floatval($pedido->costo_envio));
        $por_pagar=$por_pagar-floatval($pedido->pago_inicial)-floatval($pedido->descuento);
        $por_pagar=floatval($por_pagar);
    
        try {
            $contents = \Storage::disk("files")->get('imprimir.json');
            $proceso = json_decode($contents, true);
            $proceso=$proceso["proceso"]+1;

            $ticket_json=array(
            "proceso"=>$proceso,
            "tipo"=>"pedido",
            "caja"=>$pedido->caja,
            "turno"=>$pedido->turno,
            'cajero'=>$pedido->cajero,
            "fecha"=>date("d-m-Y h:i:s"),
            'pedido'=>intval($id),
            "ruta"=>$nombre_ruta,
            "nombre_cliente"=>$cliente->nombre." ".$cliente->apellidos,
            "descripcion_lugar_envio"=>$pedido->referencia,
            "nota"=>$pedido->nota,
            "status"=>$pedido->status,

            "subtotal_pedido" =>floatval($pedido->subtotal_pedido),
            "costo_envio" => floatval($pedido->costo_envio),
            "pago_inicial"=>floatval($pedido->pago_inicial),
            "descuento" => floatval($pedido->descuento),
            "por_pagar" => $por_pagar,
                
            "id_articulos"=>explode(",",$pedido->id_articulos),
            "cantidad"=>explode(",",$pedido->cantidad),
            "unidad"=>explode(",",$pedido->unidad),
            "desc_pro"=>explode(",",$pedido->descripcion),
            "precio"=>explode(",",$pedido->precio_vendido),
            "tipo_precio_vendido"=>explode(",",$pedido->tipo_precio_vendido),
            "subtotalProducto"=>explode(",",$pedido->subtotal_articulos),
            );
        
            $json_pedido=json_encode($ticket_json,JSON_PRETTY_PRINT);
            \Storage::disk('files')->put("imprimir.json",$json_pedido);
            $id=str_pad($id, 8, '0', STR_PAD_LEFT);
            return back()->with("success","Se mandó a imprimir el pedido #".$id);
        } catch (Exception $e) {
            return back()->with("error","No fue posible imprimir el pedido #".$id);
        }
    }



//#########################################################################################################




















    function GetMAC(){
        ob_start();
        system('getmac');
        $Content = ob_get_contents();
        ob_clean();
        return substr($Content, strpos($Content,'\\')-20, 17);
    }


    public function retomar2(Request $data){
        $dia   = date("N");
        $mes   = date("m");
        $dias  = ["Lun", "Mar", "Mie", "Juv", "Vie", "Sab", "Dom"];
        $meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
        // $fecha = $dias[$dia - 1] . "  " . date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $fecha = date("d") . " " . $meses[$mes - 1];


        $id_pedido=$data->id_pedido;
        // echo $id_pedido;
        $pedido=pedidos::find($id_pedido);

        $prod_pedidos=productos_pedidos::where("id_pedido","=",$id_pedido)->get();
        foreach($prod_pedidos as $r){
            $pro=articulos::find($r->id_producto);
            $r->descripcion=$pro->descripcion_articulo;
            $r->precio_venta=$pro->precio_venta;
            $r->mayoreo_1=$pro->mayoreo_1;
            $r->mayoreo_2=$pro->mayoreo_2;
            $r->mayoreo_3=$pro->mayoreo_3;
            $r->mayoreo_4=$pro->mayoreo_4;
        }
        
        $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->get();
        if(isset($turno[0])){
            $turno=$turno[0]->id;    
        }else{$turno=0;}

        $mac=$this->GetMAC();

        $caja=Cajas::where("mac","=",$mac)->first();

        if ($caja!=null) {
            $mimac=$caja->mac;
        }else{
            $mimac="";
        }

        if($mimac==$mac){
            return view("forms.ventas",compact('id_pedido','prod_pedidos','pedido','caja','fecha','turno'));
        }else{
            $msj="No esposible ir a la vista de VENTA. Es necesario agregar su equipo como una caja.";
            return back()->with("msj",$msj);
        } 





    }

    public function verPedido($id){
        date_default_timezone_set('america/mexico_city');
        $pedido=pedidos::find($id);
        $rutas=rutas::all();
        $articulos=productos_pedidos::where("id_pedido","=",$id)
        ->join("productos","productos_pedidos.id_producto","=","productos.id")
        ->get();
        return view('forms.edit-pedidos',compact('pedido','rutas','productos'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
    }
    public function verProPedido(Request $request){
      
      $idPedido=$request->input('idPedido');
      $productos_pedidos=DB::select('SELECT productos.id,id_pedido,id_producto,productos_pedidos.cantidad,productos.unidad,descripcion_articulo,precio,importe,fotos 
         FROM productos_pedidos INNER JOIN productos ON productos_pedidos.id_producto = productos.id WHERE id_pedido=?',[$idPedido]);

      return json_encode($productos_pedidos);
    }


    public function actualizar(Request $request){
      
        $id=$request->get('id');
        $pedido=pedidos::find($id);
          try {
              $pedido->update(
                [
                    "id_ruta"       =>$request->get('id_ruta'),
                    "referencia"    =>$request->get('referencia'),
                    "costo_envio"   =>$request->get('envio'),
                    "descuento"     =>$request->get('descuento'),
                    "pago_inicial"  =>$request->get('pago_inicial'),
                ]
              );
              return back()->with('success','PEDIDO #'.str_pad($request->get('id'), 6, '0', STR_PAD_LEFT).' modificado correctamente.');
          } catch (Exception $e) {
                return back()->with('error','ERROR al intentar modificar este pedido.');
          }

      return view('forms.edit-pedidos',compact('pedido','msj'));
    }


   


    public function crearpedido(Request $request){
      date_default_timezone_set('america/mexico_city');
      $caja=$request->get("caja");
      $turno=$request->get("turno");
      $cajero=$request->get("cajero");
      $id_pedido=$request->get("id_pedido");

       $importe=$request->input("importe");
       $subtotal=$request->input("subtotal");
       $descuento=$request->input("descuento");
       $pago_inicial=$request->get("pago_inicial");
       $por_pagar=$request->input("por_pagar");

       //datos de los areglos de productos
       $idP=$request->input("idP");
       $cantidad=$request->input("cantidad");
       $unidad=$request->input("unidad");
       $descripcion=$request->input("descripcion");
       $precio=$request->input("precio");
       $subtotalProducto=$request->input("subtotalProducto");

       $status="";
       $estadoPedido="";
       $orden=0;        //pendiente=1, surtiendo=2, enviado=3, entregado=4,  apartado=5, guardado=6,
       if($request->input("accion")==1){
        $estadoPedido="guardado";
        $orden=12;

       }if ($request->input("accion")==2) {
           $estadoPedido="pendiente";
           $orden=1;
       }
       if ($request->input("accion")==3) {
           $estadoPedido="apartado";
           $orden=11;
       }

       if($request->input("idCliente")!=0){
        $cliente=clientes::find($request->input("idCliente"));
        $nombre_cliente=$cliente->nombre." ".$cliente->apellidos;
        $tipo_precio=$cliente->nivel;
        $id_ruta=$cliente->id_ruta;
        $ruta=rutas::find($id_ruta);
       }else{
        $nombre_cliente="Publico";
        $tipo_precio="publico";
        $ruta="no especificado";
       }
       if($id_pedido!=0){//SI ESTA RETOMANDO UN PEDIDO#############################################################

        //----------------------------eliminando articulos del pedidos para despues volver a agregarlos.----------------------
        $productos_pedidos=productos_pedidos::where("id_pedido","=",$id_pedido)->get();
        foreach($productos_pedidos as $r){
            $pro_pedido=productos_pedidos::find($r->id);
            $pro_pedido->delete();
        }
        //----------------------------------modificando datos del pedido-----------------------
        $pedido=pedidos::find($id_pedido);
        $estado_anterior=$pedido->status;
        $pedido->update([
            "caja"=>$caja,
            "turno"=>$turno,
            "referencia"=>$request->get("descripcion_lugar"),
            "id_ruta"=>$id_ruta,
            "costo_envio"=>$request->get("costo_envio"),
            "subtotal"=>$request->get("subtotal"),
            "pago_inicial"=>$request->get("pago_inicial"),
            "descuento"=>$descuento,
            "status"=>$estado_anterior."=>".$estadoPedido."&".now(),
            "orden"=>$orden,
            "cajero"=>$request->input("cajero"),
            "factura"       =>"no",
            "updated_at"=>now()
        ]);
            //-------------------agregando los productos------------------------------
            $cantProd=count($idP);
            try {
                for ($x=0; $x<$cantProd; $x++) { 
                    $pro_pedidos=productos_pedidos::create([
                    "id_pedido"=> $id_pedido,
                    "id_producto"=>$idP[$x],
                    "cantidad"=>$cantidad[$x],
                    "unidad"=>$unidad[$x],
                    "precio"=>$precio[$x],
                    "importe"=>$subtotalProducto[$x],
                   ]);
                } //fin for
            $status="success"; 
           } catch (Exception $e) {
               $status="fail";
           }//fin catch

           //##############################buscar nombre ruta##################
                if(isset($ruta->nombre_ruta)){
                    $nombre_ruta=$ruta->nombre_ruta;
                }else{
                    $nombre_ruta="no especificado";
                }

                //###############################################################################################
                $contents = \Storage::disk("files")->get('imprimir.json');
                $proceso = json_decode($contents, true);
                $proceso=$proceso["proceso"]+1;

                $ticket_json=array(
                  "proceso"=>$proceso,
                  "tipo"=>"pedido",
                  "caja"=>$caja,
                  "turno"=>$turno,
                  'cajero'=>$request->input("cajero"),
                  "fecha"=>$request->input("fecha"),
                  'pedido'=>$id_pedido,

                  "nombre_cliente"=>$nombre_cliente,
                  "descripcion_lugar_envio"=>$request->input("descripcion_lugar"),
                  "importe" => $importe,
                  "costo_envio" => $request->input("costo_envio"),
                  "subtotal" => $subtotal,
                  "pago_inicial"=>$request->get("pago_inicial"),
                  "descuento" => $descuento,
                  "por_pagar" => $por_pagar,
                  "ruta"=>$nombre_ruta,
                  "desc_pro"=>$descripcion,
                  "cantidad"=>$cantidad,
                  "unidad"=>$unidad,
                  "precio"=>$precio,
                  "subtotalProducto"=>$subtotalProducto,
                );

                $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
                \Storage::disk('files')->put("imprimir.json",$json_venta);
                $id_pedido=str_pad($id_pedido, 8, '0', STR_PAD_LEFT);
            return json_encode(compact('status','cantProd','descuento','importe','id_pedido')); 


       }else{
       //ES UN PEDIDO NUEVO#################################################
                    try {
                        \DB::beginTransaction();
                                $pedido = new pedidos();
                                $pedido->fill([
                                    "caja"          =>$caja,
                                    "turno"         =>$turno,
                                    "id_cliente"    =>$request->input("idCliente"),
                                    'nombre_cliente'=>$nombre_cliente,
                                    'tipo_precio'   =>$tipo_precio,
                                    "referencia"    =>$request->get("descripcion_lugar"),
                                    "id_ruta"       =>$id_ruta,
                                    "costo_envio"   => $request->get("costo_envio"),
                                    "subtotal"      => $request->get("subtotal"),
                                    "pago_inicial"  =>$request->get("pago_inicial"),
                                    "descuento"     => $descuento,
                                    "status"        => $estadoPedido."&".now(),
                                    "orden"         =>$orden,
                                    "cajero"        =>$request->input("cajero"),
                                    "factura"       =>"no",
                                ]);
                                $pedido->push();
                                \DB::commit();


                            $status="success";
                       } catch (Exception $e) {
                           $status="fail";
                       }

                        $ultimoP=$pedido->id;
                        $prodd="";

                       if ($status=="success" && $request->input("idCliente")!=0) {
                        //#############restando en el monedero del cliente
                            $monedero_cliente=$cliente->monedero;
                            if(isset($descuento) && $monedero_cliente>=20){
                                $cliente=$cliente->update([
                                  "monedero"=>$monedero_cliente-$descuento
                                ]);
                            }
                            
                        $cantProd=count($idP);
                           try {
                            for ($x=0; $x<$cantProd; $x++) { 
                                $prodd=$prodd." ".$descripcion[$x];
                                $pro_pedidos=productos_pedidos::create([
                                "id_pedido"=> $ultimoP,
                                "id_producto"=>$idP[$x],
                                "cantidad"=>$cantidad[$x],
                                "unidad"=>$unidad[$x],
                                "precio"=>$precio[$x],
                                "importe"=>$subtotalProducto[$x],
                               ]);
                            } //fin for
                            $status="success"; 
                           } catch (Exception $e) {
                               $status="fail";
                           }//fin catch
                       }//fin if


                       //##############################buscar nombre ruta##################
                        if(isset($ruta->nombre_ruta)){
                            $nombre_ruta=$ruta->nombre_ruta;
                        }else{
                            $nombre_ruta="no especificado";
                        }



                       //#################guardando datos en imprimir.json#############################################################
                        $contents = \Storage::disk("files")->get('imprimir.json');
                        $proceso = json_decode($contents, true);
                        $proceso=$proceso["proceso"]+1;

                        $ticket_json=array(
                          "proceso"=>$proceso,
                          "tipo"=>"pedido",
                          "caja"=>$caja,
                          "turno"=>$turno,
                          'cajero'=>$request->input("cajero"),
                          "fecha"=>$request->input("fecha"),
                          'pedido'=>$ultimoP,

                          "nombre_cliente"=>$nombre_cliente,
                          "descripcion_lugar_envio"=>$request->input("descripcion_lugar"),
                          "importe" => $importe,
                          "costo_envio" => $request->input("costo_envio"),
                          "subtotal" => $subtotal,
                          "pago_inicial"=>$request->get("pago_inicial"),
                          "descuento" => $descuento,
                          "por_pagar" => $por_pagar,
                          "ruta"=>$nombre_ruta,
                          "desc_pro"=>$descripcion,
                          "cantidad"=>$cantidad,
                          "unidad"=>$unidad,
                          "precio"=>$precio,
                          "subtotalProducto"=>$subtotalProducto,
                        );
                        $id_pedido=str_pad($ultimoP, 8, '0', STR_PAD_LEFT);

                        $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
                        \Storage::disk('files')->put("imprimir.json",$json_venta);
                return json_encode(compact('status','cantProd','prodd','descuento','importe','id_pedido')); 
       }


       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
}
