<?php

namespace App\Http\Controllers;

use App\Models\articulos;
use App\Models\marcas;
use App\Models\provedores;
use App\Models\Categorias;
use App\Models\lineas;
use Illuminate\Http\Request;

class articulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title="ARTICULOS";
        $articulos = articulos::orderBy("descripcion_articulo","ASC")->paginate(10);
        
        foreach($articulos as $r){
            if($r->proveedor!=null){
                $provedor=$r->proveedor;
                $provedor=provedores::find($provedor);
                $r->provedor=$provedor->nombre;
            }else{$r->provedor=null;}
            if($r->marca!=null){
                $marca=$r->marca;
                $marca=marcas::find($marca);
                $r->marca=$marca->nombre;
            }else{$r->marca=null;}  
        }
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $lineas=lineas::all();
        $marcas=marcas::all();

        return view("forms/articulos.articulos",compact('title','articulos','provedores','categorias','lineas','marcas'));
    }



    public function buscar_articulos(Request $data){
        $title="ARTICULOS";
        $busqueda=$data->get("busqueda");
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $lineas=lineas::all();
        $marcas=marcas::all(); 

        $busqueda=strtolower($busqueda);
            # Si hay búsqueda, agregamos el filtro
            $articulos=articulos::whereRaw('LOWER(descripcion_articulo) LIKE (?) ',["%{$busqueda}%"])
            ->orWhereRaw('LOWER(clave) LIKE (?) ',["%{$busqueda}%"])
            ->orWhereRaw('LOWER(codigo_barra) LIKE (?) ',["%{$busqueda}%"])
            ->orWhereRaw('LOWER(palabra_clave) LIKE (?) ',["%{$busqueda}%"])
            ->orderBy("descripcion_articulo","ASC")
            ->get();

            foreach($articulos as $r){
            if($r->proveedor!=null){
                $provedor=$r->proveedor;
                $provedor=provedores::find($provedor);
                $r->provedor=$provedor->nombre;
            }else{$r->provedor=null;}
            if($r->marca!=null){
                $marca=$r->marca;
                $marca=marcas::find($marca);
                $r->marca=$marca->nombre;
            }else{$r->marca=null;}  
        }

        return view("forms/articulos.articulos",compact('title','articulos','provedores','categorias','lineas','marcas','busqueda')); 
    }
    



    public function listar_articulos(){
        $articulos=articulos::all();
        echo json_encode($articulos);
    }


    
    public function buscarExistencia(Request $data)
    {
        $buscar    = $data->get('buscar');
        $buscar=strtolower($buscar);
        $articulos = articulos::select("id","clave","descripcion_articulo","unidad","existencia_stock","precio_caja","pre_compra","pre_unidad","pre_caja","pre_paquete","pre_mayoreo","pre_membrecia","a_granel","mayoreo_apartir")
            ->whereRaw('LOWER(descripcion_articulo) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(palabra_clave) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(clave) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(codigo_barra) LIKE (?) ',["%{$buscar}%"])
            // ->orWhere("palabra_clave", "=", "$buscar")
            // ->orWhere("clave", "=", "%$buscar%")
            // ->orWhere("codigo_barra","like","%$buscar%")
            ->get();
        return json_encode($articulos);
    }

    public function buscar_x_codigo(Request $data)
    {
        $buscar    = $data->get('codigo');
        $buscar=strtolower($buscar);
        $articulos = articulos::select("id","clave","descripcion_articulo","unidad","existencia_stock","precio_caja","pre_compra","pre_unidad","pre_caja","pre_paquete","pre_mayoreo","pre_membrecia","a_granel","mayoreo_apartir")
            ->whereRaw('LOWER(codigo_barra) = (?) ',["{$buscar}"])
            ->orWhere("id",$buscar)
            // orWhere("codigo_barra", "=", "$buscar")
            ->get();
        return json_encode($articulos);
    }


    public function buscarArticulos(Request $data)
    {
        $buscar = $data->get('input');
        $buscar=strtolower($buscar);
        $articulos = articulos::whereRaw('LOWER(descripcion_articulo) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(codigo) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(clave) LIKE (?) ',["%{$buscar}%"])
            ->orWhereRaw('LOWER(cod_barra) LIKE (?) ',["%{$buscar}%"])
            // orWhere("descripcion", "LIKE", "%$buscar%")
            // ->orWhere("codigo", "=", "$buscar")
            // ->orWhere("clave", "=", "$buscar")
            // ->orWhere("cod_barra", "=", "$buscar")
            ->get();
        $marcas     = marcas::all();
        $provedores = provedores::all();
        return view("forms.articulos", compact('articulos', 'marcas', 'provedores'));
    }

    public function updateExistenca(Request $data)
    {
        $id       = $data->get('id');
        $cantidad = $data->get('cantidad');
        try {
            for ($y = 0; $y < count($id); $y++) {
                $articulo = articulos::find($id[$y]);
                $cant     = $articulo->existencia + $cantidad[$y];
                $articulo->update([
                    "existencia" => $cant,
                ]);
            }
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }
    public function eliminarArticulo(Request $data)
    {
        try {
            $articulo = articulos::find($data->get('id'));
            $articulo->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $title="EDITAR ARTICULO";
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $lineas=Lineas::all();
        $marcas=marcas::all();
        $articulo=articulos::find($id);
        return view("forms/articulos.update-articulo",compact('title','articulo','provedores','categorias','lineas','marcas'));
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id){


        try {
            $request=$request;
            articulos::find($id)->update([
              'clave' => $request->get("clave"),
              'descripcion_articulo'=> $request->get("descripcion_articulo"),
              'unidad'=> $request->get("unidad"),
              'inv_min'=> $request->get("inv_min"),
              'inv_max'=> $request->get("inv_max"),
              'existencia_bodega'=> $request->get("existencia_bodega"),
              'existencia_stock'=> $request->get("existencia_stock"),
              'inv_compra'=> $request->get("inv_compra"),

              'codigo_barra'=> $request->get("codigo_barra"),
              'codigo_sat'=> $request->get("codigo_sat"),
              'a_granel'=> $request->get("a_granel"),
              'en_catalogo'=> $request->get("en_catalogo"),
              'ventas_negativas'=> $request->get("ventas_negativas"),
              
              'precio_caja'=> $request->get("precio_caja"),
              'num_paquete'=> $request->get("num_paquete"),
              'pza_x_caja'=> $request->get("pza_x_caja"),
              'mayoreo_apartir'=> $request->get("mayoreo_apartir"),
              'ganancia_unidad'=> $request->get("ganancia_unidad"),
              'ganancia_caja'=> $request->get("ganancia_caja"),
              'ganancia_mayoreo'=> $request->get("ganancia_mayoreo"),
              'ganancia_membrecia'=> $request->get("ganancia_membrecia"),
              'pre_compra'=> $request->get("pre_compra"),
              'pre_unidad'=> $request->get("pre_unidad"),
              'pre_caja'=> $request->get("pre_caja"),
              'pre_paquete'=> $request->get("pre_paquete"),
              'pre_mayoreo'=> $request->get("pre_mayoreo"),
              'pre_membrecia'=> $request->get("pre_membrecia"),
              'proveedor'=> $request->get("proveedor"),
              'categoria'=> $request->get("categoria"),
              'linea'=> $request->get("linea"),
              'marca'=> $request->get("marca"),
              'local'=> $request->get("local"),
              'ubicacion_producto'=> $request->get("ubicacion_producto"),
              'palabra_clave'=> $request->get("palabra_clave"),
              'caducidad'=> $request->get("caducidad"),
              'url'=> $request->get("url"),
              'fotos'=> $request->get("fotos"),
              'descripcion_catalogo'=> $request->get("descripcion_catalogo"),

            ]);
            return back()->with("success","Articulo modificado correctamente.");
        } catch (\PDOException  $e) {
            return back()->with("error","ERROR al intentar hacer modificaciones de esta articulo.");
        }

    }

    public function registrar_articulo(){
        $title="REGISTRAR ARTICULO";
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $lineas=Lineas::all();
        $marcas=marcas::all();
        return view("forms/articulos.registrar-articulos",compact('title','provedores','categorias','lineas','marcas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try {
            articulos::create($request->all());
            return back()->with("success","Articulo agregado correctamente.");
        } catch (\PDOException $e) {
           if ($e->getCode()== 23505) {
                return back()->with("error","ERROR, \"Codigo de Barras \" duplicados");
             }else{
                
                return back()->with("error","ERROR, error al registrar este producto, verifique la consistencia de los datos proporcionado.");
             }
             return back()->with("error","ERROR, error al registrar este producto, verifique la consistencia de los datos proporcionado.");
        } 
    }


     public function uploadImg(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        $nombre_articulo=$request->get("nombre");
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();

            \Storage::disk('articulos')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }


    public function store_marca(Request $request)
    {
        try {
            $marca  = marcas::create($request->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function update_marca(Request $request)
    {
        $id          = $request->get("id_marca");
        $nombre      = $request->get("nombre");
        $descripcion = $request->get("descripcion");
        $provedor    = $request->get("provedor");
        try {
            $marca = marcas::find($id);
            $marca->update([
                "nombre"      => $nombre,
                "descripcion" => $descripcion,
                "provedor"    => $provedor,
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function delete_marca(Request $request)
    {
        $id = $request->get("id");
        try {
            $marca = marcas::find($id);
            $marca->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }

    public function update_provedor(Request $request)
    {
        $id          = $request->get("id_provedor");
        $nombre      = $request->get("nombre");
        $descripcion = $request->get("descripcion");
        $rfc         = $request->get("rfc");
        $domicilio   = $request->get("domicilio");
        $tel         = $request->get("tel");
        $email       = $request->get("email");
        try {
            $marca = provedores::find($id);
            $marca->update([
                "nombre"      => $nombre,
                "descripcion" => $descripcion,
                "rfc"         => $rfc,
                "domicilio"   => $domicilio,
                "tel"         => $tel,
                "email"       => $email,
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function delete_provedor(Request $request)
    {
        $id = $request->get("id");
        try {
            $provedor = provedores::find($id);
            $provedor->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }

    public function store_provedor(Request $request)
    {
        try {
            $marca  = provedores::create($request->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }



    public function createCategoria(Request $request)
    {
       if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            categorias::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createLinea(Request $request)
    {
            if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            lineas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createMarca(Request $request)
    {
            
        if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            marcas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }











    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
