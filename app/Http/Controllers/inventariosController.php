<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\articulos;
class inventariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articulos=articulos::paginate(20);
        $title="Logìstica de inventarios";
        return view("forms/inventarios.inventario",compact("title","articulos"));
    }
    public function buscar_en_inventario(Request $data){
        $title="Logìstica de inventarios";

        $busqueda=$data->get("busqueda");
        $articulos=articulos::where("descripcion_articulo","like","%$busqueda%")
        ->orWhere("codigo_barra",$busqueda)
        ->paginate(20);

         return view("forms/inventarios.inventario",compact("title","articulos","busqueda"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
