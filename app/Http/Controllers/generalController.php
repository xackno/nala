<?php

namespace App\Http\Controllers;

use App\Models\articulos;
use App\Models\salidas;
use App\Models\turnos;
use Illuminate\Http\Request;
use App\Models\Cajas;
use App\Models\User;
use App\Models\ventas;
use App\Models\recuperaciones;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpseclib3\Net\SFTP;

class generalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function bd(){
        $title="Actualización de BD";
        
        return view("forms.bd",compact("title"));
    }

    public function update_database(){

        if($this->testServidorWeb("https://nalaexpress.com")){//si hay conexion con el servidor
            $generar_backup_remoto=$this->sumit_backup_remote();

            $get_file=$this->get_last_update();
            if($generar_backup_remoto=="success"){
                if( $get_file=="success"){
                    $status="success";
                }else{$status="error";}
            }else{
                $status="error";
            }
         }else {
            $status="error";
        }
         $status="success";
        //----------------------------------ejecutando sql bajado del sftp---------------
        if($status=="success"){
             $this->sumit_sql_articulo();
            $status="success";
        }else{
            $status="error";
        }
           
        return back()->with("status",$status); 
        
    }

    function sumit_sql_articulo(){

        try {
            $articulos=articulos::all();
            foreach($articulos as $r){
                $r->delete();
            }
        } catch (Exception $e) {
            
        }
        
        //______________remplazando en el archivo .bat________________________
        $path_where_psql=shell_exec("where psql");
        $path_where_psql=explode('\\',$path_where_psql);
        $path_psql="";
        for($i=0;$i<count($path_where_psql)-1;$i++){
            $path_psql.=$path_where_psql[$i]."/";
        }
        $path_psql='"'.$path_psql."psql".'"';

        $paralinea6="SET PGPATH=".$path_psql."\n";
        $file = file(storage_path('files')."/importar_sql.bat"); 
        $data = 'set archivo='.storage_path('files')."/last_update.sql";
        foreach($file as $index => $line){
            if($index == 5){//linea 6
               $file[$index] = $paralinea6;
           }
           if($index == 8){//linea 9
               $file[$index] = $data . "\n";
           }
        }
        $content = implode($file);
        file_put_contents(storage_path('files')."/importar_sql.bat", $content);


        ######################ejecutando archivo bat###############################
        try {
            $importar=shell_exec(storage_path('files')."/importar_sql.bat");
            $status="success";
        } catch (Exception $e) {
            $status="error";
        }
        return $status;
        
    }

    function testServidorWeb($servidor) {
        $a = @get_headers($servidor);
         
        if (is_array($a)) {
            return true;
        } else {
            return false;
        }
    }

    function sumit_backup_remote(){
         $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
            ),
        ); 
         try {
             // $file=file_get_contents("http://172.16.14.156/respaldo.php", false, stream_context_create($arrContextOptions));
            $file=file_get_contents("https://nalaexpress.com/UPDATE_NALA_SERVER/respaldo.php", false, stream_context_create($arrContextOptions));
            $status="success";
         } catch (Exception $e) {
             $status="error";
         }
        
        return $status;
    }
   function get_last_update(){
        $sftp = new SFTP('74.208.50.65');//62.151.176.26
        if (!$sftp->login('root', 'Famercrs1@')) {
            $status="error";
            exit('Login Failed');
        }else{
            $all_file=$sftp->nlist("/var/www/vhosts/nalaexpress.com/httpdocs/UPDATE_NALA_SERVER/respaldos/");
            $all_file = array_diff($all_file, array(".",".."));
            $new_array_files=[];
            foreach($all_file as $file){
                array_push($new_array_files, $file);
            }
            // print_r($new_array_files);
            // print_r(count($new_array_files));
            $name_last_file=$new_array_files[count($new_array_files)-1];
            $sftp->get('/var/www/vhosts/nalaexpress.com/httpdocs/UPDATE_NALA_SERVER/respaldos/'.$name_last_file,storage_path('files')."/last_update.sql"); 
            $status="success";
        }
        return $status;
}





    public function listar_usuarios(){
        $users=User::all();
        $cajas=cajas::all();
        $title="Usuarios";
        return view("auth.list-user",compact('users','cajas','title'));
    }
     protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    protected function registrar_user(Request $data){
         User::create([
            'name' => $data->get('name'),
            'user' => $data->get('user'),
            'caja' => $data->get("caja"),
            'type' => $data->get('type'),
            'email' => $data->get('email'),
            'password' => Hash::make($data->get('password')),
        ]);
         $users=User::all();
         $cajas=Cajas::all();
         return view("auth.list-user",compact('users','cajas'));
    }
    public function eliminar_user($id){
        User::find($id)->delete();
        return back()->with("success","Eliminado correctamente");
    }
    public function actualizar_user(Request $data){
        $user=User::find($data->get('id'));
        try {
            $user->update($data->all());
        } catch (Exception $e) {
            var_dump($e);
        }
        return back();
    }


    public function administrar_cajas(){
        $cajas=cajas::all();
        return view("forms.cajas",compact('cajas'));
    }
    public function actualizar_caja(Request $data){
        $caja=cajas::find($data->get('id'));
        try {
            $caja->update($data->all());
            $status="success";
        } catch (Exception $e) {
            $status="error";
        }
        return back();
    }
    public function eliminar_caja(Request $data){
        
        try {
            $caja=cajas::find($data->get('id'));
            $caja->delete();

            //###########guardando todas las cajas en el archivo cajas.json..............................
            $contents = \Storage::disk("files")->get('cajas.json');
            $cajas=cajas::select("nombre","ip")->get();
            $cajas=json_encode($cajas);
            \Storage::disk('files')->put("cajas.json",$cajas);


            $status="success";

            // $usuarios=User::where("caja","=",$data->id);
            // $usuarios->update(["caja"=>NULL]);
        } catch (Exception $e) {
            $status="fails";
        }


        return json_encode($status);
    }
    public function registrar_caja(Request $data){

        try {
            $cajas=cajas::create([
                "nombre"=>$data->get("nombre"),
                "ip"=>$data->get("ip"),
                "marca_impresora"=>$data->get("marca_impresora"),
                "nombre_impresora"=>$data->get("nombre_impresora"),
                "mm_impresora"=>$data->get("mm_impresora"),
            ]);
            //###########guardando todas las cajas en el archivo cajas.json..............................
            $contents = \Storage::disk("files")->get('cajas.json');
            $cajas=cajas::select("nombre","ip")->get();
            $cajas=json_encode($cajas);
            \Storage::disk('files')->put("cajas.json",$cajas);
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        // var_dump($data->nombre);
        return json_encode($status);
    }

    public function registrar()
    {
        $title="REGISTRAR";
        return view('auth.register',compact("title"));
    }




    public function imprimir_corte_turno($id){
        $turno=turnos::find($id);
        $salidas=salidas::select(DB::raw("SUM(cantidad) as total"))->where("turno",$id)->first();
        $lista_salidas=salidas::select("cantidad","concepto")->where("turno",$id)->get();
        $ventas=ventas::select(DB::raw("SUM(total_venta) as total"))->where("id_turno",$id)->first();
        $usuario=User::find($turno->usuario);
        $caja=$turno->caja;

        $recuperaciones=recuperaciones::select(DB::raw("SUM(cantidad) as total_rec"))->where("turno",$id)->first();
        $total_recuperaciones=$recuperaciones->total_rec;

         $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $oldDate = strtotime($turno->created_at);
        $inicio_turno = date('Y-m-d H:i:s',$oldDate);


        $ticket_json=array(
            "proceso"=>$proceso,
            "tipo"=>"corte_turno",
            'cajero'=>$usuario->name,
            "caja"=>$caja,
            "id_turno"=>"T".str_pad($id, 6, '0', STR_PAD_LEFT),
            "inicio"=>$inicio_turno,
            'fin'=>$turno->fecha_fin,
            'total_venta'=>$ventas->total,
            "total_salidas"=>$salidas->total,
            "efectivo_inicio"=>$turno->inicio,
            "efectivo_cierre_system"=>$turno->cierre_system,
            "efectivo_cierre_conteo"=>$turno->cierre_conteo,
            "total_recuperaciones"=>$total_recuperaciones,
            "conteo_inicio"=>$turno->conteo_inicio,
            "conteo_final"=>$turno->conteo_final,
            "comentario1"=>$turno->comentario1,
            "comentario2"=>$turno->comentario2,
            "status"=>$turno->status,
            "lista_salidas"=>$lista_salidas,
        );

        $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
        \Storage::disk('files')->put("imprimir.json",$json_venta);

        return back()->with("success","Se mandó los datos del turno #".str_pad($id, 8, '0', STR_PAD_LEFT)."  para su impresión.");
    }




    public function storeturno(Request $data)
    {
        date_default_timezone_set('America/Mexico_City');
        try {
            $turno  = turnos::create($data->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }
    public function buscarsalidas(Request $data)
    {
        $user    = $data->get("user");
        $turno   = $data->get("turno");
        $salidas = salidas::where("turno", "=", $turno)
            ->where("usuario", "=", $user)->get();
        return json_encode($salidas);
    }
    public function editar_salida(Request $data)
    {
        $id       = $data->get("id");
        $concepto = $data->get("concepto");
        $cantidad = $data->get("cantidad");
        try {
            $salida = salidas::find($id);
            $salida->update($data->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function eliminarsalida(Request $data)
    {
        $id = $data->get("id");
        try {
            $salida = salidas::find($id);
            $salida->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function storesalidas(Request $data)
    {
        try {
            $salida = salidas::create($data->all());
            $status = "SE REGISTRO CORRECTAMENTE";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }

    public function cerrarturno(Request $data)
    {
        date_default_timezone_set('America/Mexico_City');
        $id     = $data->get("id");
        $cerrar = turnos::find($id);
        $cerrar->update(
            [
                "cierre_system" => $data->get("cierre_system"),
                "cierre_conteo" =>$data->get("cierre_conteo"),
                "conteo_final"=> $data->get("conteo_final"),
                "comentario2" => $data->get("comentario2"),
                "fecha_fin"   => now(),
                "status"      => $data->get("status"),
            ]
        );
        $this->imprimir_corte_turno($id);
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

