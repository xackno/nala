<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\provedores;
use App\Models\Categorias;
use App\Models\lineas;
use App\Models\marcas;
use App\Models\articulos;

class catalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="CATÁLOGO";
         $provedores=provedores::all();
         $categorias=Categorias::all();
         $lineas=lineas::all();
         $marcas=marcas::all();
         $productos=articulos::paginate(20);

         $total=articulos::all();
         $total=count($total);

        return view("forms.catalogo",compact('title','provedores','categorias','lineas','marcas','productos','total'));
    }


    public function buscar_articulo(Request $r){
        $id=$r->get("id");
        $articulo=articulos::find($id);
        $marca=""; $categoria=""; $provedor="";
        if($articulo->marca!=NULL || $articulo->marca!=''){
            $marca=marcas::find($articulo->marca);
            $articulo->marca=$marca->nombre;
        }
        if($articulo->categoria!=NULL || $articulo->categoria!=''){
            $categoria=Categorias::find($articulo->categoria);
            $articulo->categoria=$categoria->nombre;
        }
        if($articulo->proveedor!=NULL || $articulo->proveedor!=''){
            $provedor=provedores::find($articulo->proveedor);
            $articulo->provedor=$provedor->nombre;
        }
        

        return json_encode($articulo);
    }

    public function search_for_catalogo(Request $request){
        $title="CATÁLOGO";
        $tipo=$request->get('tipo');
        $nombre=$request->get('nombre');

        if($tipo=="categorias"){
            $categoria=Categorias::where("nombre","=",$nombre)->get();
            $productos=articulos::where("categoria","=",$categoria[0]->id)
            ->orderBy('id','DESC')
            ->paginate(20);
        }else if($tipo=="marcas"){
            $marca=marcas::where("nombre","=",$nombre)->get();
            $productos=articulos::where("marca","=",$marca[0]->id)
            ->orderBy('id','DESC')
            ->paginate(20);
        }else if($tipo=="provedores"){
            $provedor=provedores::where("nombre","=",$nombre)->get();
            $productos=articulos::where("proveedor","=",$provedor[0]->id)
            ->orderBy('id','DESC')
            ->paginate(20);
        }else{
            $busqueda=$request->get('input_buscar');
            $busqueda=strtolower($busqueda);
        $productos=articulos::whereRaw('LOWER(descripcion_articulo) LIKE (?) ',["%{$busqueda}%"])
        // where("descripcion_articulo","like","%$busqueda%")
        ->orderBy('id','DESC')
        ->paginate(20);    
        }
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $total=articulos::all();
         $total=count($total);


        return view("forms.catalogo",compact('title','provedores','categorias','lineas','marcas','productos','total'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
