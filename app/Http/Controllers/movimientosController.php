<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\clientes;
use App\Models\pedidos;
use App\Models\turnos;
use App\Models\recuperaciones;
use DB;
use App\Models\abonos;
use App\Exports\movimientosExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use DateTime;
class movimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="MOVIMIENTOS";
        $caja=0; $turno=0;
        if(Auth::user()->caja!=null){
            $caja=Auth::user()->caja;
            $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->first();
            if(isset($turno)){
                 $turno=$turno->id;
             }else{
                $turno=0;
             }
           
        }
        return view("forms.movimientos",compact('title','turno','caja'));
    }
    
    public function load_movimientos($id){
        $title="MOVIMIENTOS";
        $movimiento="todos";
        $cliente=clientes::find($id);
        $pedidos=pedidos::where("id_cliente",$id)->orderBy("id","DESC")->get();
        $recuperaciones=recuperaciones::where("id_cliente",$id)->orderBy("id","DESC")->get();

        //agregando fecha de liquidacion si tiene
        foreach($pedidos as $r){
            $recupe=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
            $abonos_pedido=recuperaciones::where("id_pedido",$r->id)->where("tipo","abono")->get();
            $para_diferencia=recuperaciones::where("id_pedido",$r->id)->get();
            $total_abonos=0;
            foreach($abonos_pedido as $a){
                $total_abonos+=$a->cantidad;
            }
           $r->total_abonos=$total_abonos;
           //"____________para calcular la diferencia-___________"
           $diferencia=0;
            foreach($para_diferencia as $a){
                $diferencia+=$a->cantidad;
            }
           
           //""""""""""""""""""""""""""""""""""""""""""""""""

            if (isset($recupe[0])) {
                $r->fechaliquidacion=$recupe[0]->created_at;
            }else{
                $r->fechaliquidacion="";
            }

            $descuento=0;
            if($r->descuento=='true'){
                $descuento=20;
            }else{ $descuento=0;}
            $por_cobrar2=($r->subtotal_pedido+$r->costo_envio)-($descuento + $r->pago_inicial);
            $int=($r->interes/100)*$por_cobrar2;
            $r->intereses_diarios=$int;//agregando intereses diarios

            $dias_transcurrido=0;
            $fecha_limite=new DateTime($r->fecha_limite);
            // $ahora=new DateTime(date('Y-m-d'));
            if($r->status_recuperacion=="liquidado"){
                $fecha_liquidacion=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
                $fecha_liquidacion=$fecha_liquidacion[0]->created_at;
                $fecha_liquidacion=explode(" ", $fecha_liquidacion);
                $fecha_liquidacion=$fecha_liquidacion[0];
                $ahora=new DateTime($fecha_liquidacion);
            }else{
               $ahora=new DateTime(date('Y-m-d')); 
            }
            
            if($fecha_limite < $ahora){
                $diff2 = $fecha_limite->diff($ahora);
                $dias_transcurrido=$diff2->days;
            }else{
                $dias_transcurrido=0;
            }
            $r->por_cobrar2=$por_cobrar2;
            $r->intereses=$int*$dias_transcurrido;//agregar total intereses
            $r->total_con_intereses=($por_cobrar2)+($int*$dias_transcurrido);
            $r->dias_transcurrido=$dias_transcurrido;//agregando dias transcurrido
            $por_cobrar=number_format(($por_cobrar2)+($int*$dias_transcurrido),2);
            $r->por_cobrar=$por_cobrar;//agregando columna por cobrar


            $r->pendiente=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$total_abonos,2);
            $r->diferencia=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$diferencia,2);

        }

        $caja=0; $turno=0;
        if(Auth::user()->caja!=null){
            $caja=Auth::user()->caja;
            $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->first();
            if(isset($turno)){
                 $turno=$turno->id;
             }else{
                $turno=0;
             }
        }

        return view("forms.movimientos",compact('title','cliente','pedidos','id','movimiento','recuperaciones','turno','caja') );
    }
    public function get_por_cobrar($id){
        $title="MOVIMIENTOS";
        $movimiento="por_cobrar";
        $cliente=clientes::find($id);
        $pedidos=pedidos::Where("id_cliente",$id)->where('status_recuperacion',"pendiente")->orderBy("id","DESC")->get();
        $pedidosArray=[];
        foreach($pedidos as $p){
            array_push($pedidosArray, $p->id);
        }
        $recuperaciones=recuperaciones::where("id_cliente",$id)->whereIn("id_pedido",$pedidosArray)->get();


        ////#######################lo mismo que todos######################
        /// //agregando fecha de liquidacion si tiene
        foreach($pedidos as $r){
            $recupe=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
            $abonos_pedido=recuperaciones::where("id_pedido",$r->id)->where("tipo","abono")->get();
            $para_diferencia=recuperaciones::where("id_pedido",$r->id)->get();
            $total_abonos=0;
            foreach($abonos_pedido as $a){
                $total_abonos+=$a->cantidad;
            }
           $r->total_abonos=$total_abonos;
           //"____________para calcular la diferencia-___________"
           $diferencia=0;
            foreach($para_diferencia as $a){
                $diferencia+=$a->cantidad;
            }
           
           //""""""""""""""""""""""""""""""""""""""""""""""""

            if (isset($recupe[0])) {
                $r->fechaliquidacion=$recupe[0]->created_at;
            }else{
                $r->fechaliquidacion="";
            }

            $descuento=0;
            if($r->descuento=='true'){
                $descuento=20;
            }else{ $descuento=0;}
            $por_cobrar2=($r->subtotal_pedido+$r->costo_envio)-($descuento + $r->pago_inicial);
            $int=($r->interes/100)*$por_cobrar2;
            $r->intereses_diarios=$int;//agregando intereses diarios

            $dias_transcurrido=0;
            $fecha_limite=new DateTime($r->fecha_limite);
            // $ahora=new DateTime(date('Y-m-d'));
            if($r->status_recuperacion=="liquidado"){
                $fecha_liquidacion=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
                $fecha_liquidacion=$fecha_liquidacion[0]->created_at;
                $fecha_liquidacion=explode(" ", $fecha_liquidacion);
                $fecha_liquidacion=$fecha_liquidacion[0];
                $ahora=new DateTime($fecha_liquidacion);
            }else{
               $ahora=new DateTime(date('Y-m-d')); 
            }
            
            if($fecha_limite < $ahora){
                $diff2 = $fecha_limite->diff($ahora);
                $dias_transcurrido=$diff2->days;
            }else{
                $dias_transcurrido=0;
            }
            $r->por_cobrar2=$por_cobrar2;
            $r->intereses=$int*$dias_transcurrido;//agregar total intereses
            $r->total_con_intereses=($por_cobrar2)+($int*$dias_transcurrido);
            $r->dias_transcurrido=$dias_transcurrido;//agregando dias transcurrido
            $por_cobrar=number_format(($por_cobrar2)+($int*$dias_transcurrido),2);
            $r->por_cobrar=$por_cobrar;//agregando columna por cobrar


            $r->pendiente=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$total_abonos,2);
            $r->diferencia=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$diferencia,2);

        }














        $caja=0; $turno=0;
        if(Auth::user()->caja!=null){
            $caja=Auth::user()->caja;
            $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->first();
            $turno=$turno->id;
        }
        return view("forms.movimientos",compact('title','cliente','pedidos','id','movimiento','recuperaciones','turno','caja') );
    }
    public function liquidar(Request $data){
        date_default_timezone_set('america/mexico_city');
        $id_cliente=$data->get("id_cliente");
        $pendiente=$data->get("pendiente");
        try {
                $abono=recuperaciones::create([
                    "id_cliente"=>$id_cliente,
                    "cajero"=>$data->get("cajero"),
                    "caja"=>$data->get("caja"),
                    "turno"=>$data->get("turno"),
                    "tipo"=>"liquidacion",
                    "id_pedido"=>$data->get("pedido"),
                    "cantidad"=>$pendiente,
                ]);

                pedidos::find($data->get("pedido"))->update(["status_recuperacion"=>"liquidado"]);

                $this->print_ticket_liquidar($data->all());
                return redirect()->back()->with("success","Pedido liquidado con $".number_format($pendiente,2));
            } catch (\PDOException $e) {
                return redirect()->back()->with("error","ERROR al intentar liquidar este pedido.");
            }

    }


    public function abono(Request $data){
        date_default_timezone_set('america/mexico_city');
        $id=$data->get("id_cliente");
        $cantidad=$data->get("cantidad");
        $pedido=pedidos::find($data->get("pedido"));

        if($data->get("pedido")!=null){
            //"_________________________para calcular el total que debe del credito con intereces------"
            $descuento=0;
            if($pedido->descuento=='true'){
                $descuento=20;
            }else{ $descuento=0;}
            $por_cobrar2=($pedido->subtotal_pedido+$pedido->costo_envio)-($descuento + $pedido->pago_inicial);
            $int=($pedido->interes/100)*$por_cobrar2;
            $dias_transcurrido=0;
            $fecha_limite=new DateTime($pedido->fecha_limite);
            $ahora=new DateTime(date('Y-m-d'));
            
            if($fecha_limite < $ahora){
                $diff2 = $fecha_limite->diff($ahora);
                $dias_transcurrido=$diff2->days;
            }else{
                $dias_transcurrido=0;
            }
            $por_cobrar=number_format(($por_cobrar2)+($int*$dias_transcurrido),2);
            
            $abonos_pedido=recuperaciones::where("id_pedido",$data->get("pedido"))->where("tipo","abono")->get();
            $total_abonos=0;
            foreach($abonos_pedido as $a){
                $total_abonos+=$a->cantidad;
            }
           $pendiente=$por_cobrar-$total_abonos;
            //____________________________________________________________________________
        


        if($cantidad<$pendiente){
            try {
                $abono=recuperaciones::create([
                    "id_cliente"=>$id,
                    "cajero"=>$data->get("cajero"),
                    "caja"=>$data->get("caja"),
                    "turno"=>$data->get("turno"),
                    "tipo"=>"abono",
                    "id_pedido"=>$data->get("pedido"),
                    "cantidad"=>$cantidad,
                ]);
                $this->print_ticket_abono($data->all(),$por_cobrar);
                return redirect()->back()->with("success","ABONO registrado correntamente");
            } catch (\PDOException $e) {
                return redirect()->back()->with("error","ERROR al registrar el abono.");
            }
        }else{
            return redirect()->back()->with("error","La cantidad a abonar no puede ser mayor o igual a la cuenta pendiente.");
        }
        }else{
             return redirect()->back()->with("error","Debe de seleccionar un pedido.");
         }
    }

    function print_ticket_liquidar($data){
        $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $cajero=User::find($data["cajero"]);
        $cliente=clientes::find($data["id_cliente"]);
        $nombrecliente=strtoupper($cliente->nombre." ".$cliente->apellidos);

        $ticket_json=array("proceso"=>$proceso,"tipo"=>"liquidacion","caja"=>$data['caja'],'cajero'=>$cajero->name,'turno'=>$data["turno"],"fecha"=>date("d-m-Y"),'hora'=>date("h:i:s"),'id_cliente'=>$data["id_cliente"],"nombre_cliente"=>$nombrecliente,"cantidad"=>$data['pendiente']);

        $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
        \Storage::disk('files')->put("imprimir.json",$json_venta);
    }


    function print_ticket_abono($data,$por_cobrar){
        $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $cajero=User::find($data["cajero"]);
        $cliente=clientes::find($data["id_cliente"]);
        $nombrecliente=strtoupper($cliente->nombre." ".$cliente->apellidos);
        $pendiente=$por_cobrar-$data["cantidad"];
        $pendiente=number_format($pendiente,2);

        $ticket_json=array("proceso"=>$proceso,"tipo"=>"abono","caja"=>$data['caja'],'cajero'=>$cajero->name,'turno'=>$data["turno"],"fecha"=>date("d-m-Y"),'hora'=>date("h:i:s"),'id_cliente'=>$data["id_cliente"],"nombre_cliente"=>$nombrecliente,"cuenta"=>number_format($por_cobrar,2),"abonado"=>number_format($data["cantidad"],2),"pendiente"=>$pendiente);

        $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
        \Storage::disk('files')->put("imprimir.json",$json_venta);
    }



    public function buscar_info_cliente(Request $data){
        $buscar=$data->get("buscar");
        $tipo=$data->get("tipo");
        if($tipo=="nombre"){
            $clientes=clientes::where("nombre","like","%$buscar%")
        ->orWhere("apellidos","like","%$buscar%")
        ->orWhere("id","=",$buscar)
        ->get();

            if(count($clientes)==1){
                return json_encode($clientes[0]);
            }else{
                return json_encode($clientes);
            }
        }else if($tipo=="id"){
            $clientes=clientes::find($buscar);
            return json_encode($clientes);
        }
    }

    public function pdf($id,$tipo){

        $cliente=clientes::find($id);

           $pedidos=pedidos::where("id_cliente",$id)->orderBy("id","DESC")->get(); 
           //agregando fecha de liquidacion si tiene
        foreach($pedidos as $r){
            $recupe=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
            $abonos_pedido=recuperaciones::where("id_pedido",$r->id)->where("tipo","abono")->get();
            $para_diferencia=recuperaciones::where("id_pedido",$r->id)->get();
            $total_abonos=0;
            foreach($abonos_pedido as $a){
                $total_abonos+=$a->cantidad;
            }
           $r->total_abonos=$total_abonos;
           //"____________para calcular la diferencia-___________"
           $diferencia=0;
            foreach($para_diferencia as $a){
                $diferencia+=$a->cantidad;
            }
           
           //""""""""""""""""""""""""""""""""""""""""""""""""

            if (isset($recupe[0])) {
                $r->fechaliquidacion=$recupe[0]->created_at;
            }else{
                $r->fechaliquidacion="";
            }

            $descuento=0;
            if($r->descuento=='true'){
                $descuento=20;
            }else{ $descuento=0;}
            $por_cobrar2=($r->subtotal_pedido+$r->costo_envio)-($descuento + $r->pago_inicial);
            $int=($r->interes/100)*$por_cobrar2;
            $r->intereses_diarios=$int;//agregando intereses diarios

            $dias_transcurrido=0;
            $fecha_limite=new DateTime($r->fecha_limite);
            $ahora=new DateTime(date('Y-m-d'));
            
            if($fecha_limite < $ahora){
                $diff2 = $fecha_limite->diff($ahora);
                $dias_transcurrido=$diff2->days;
            }else{
                $dias_transcurrido=0;
            }
            $r->$por_cobrar2=$por_cobrar2;
            $r->intereses=$int*$dias_transcurrido;//agregar total intereses
            $r->total_con_intereses=($por_cobrar2)+($int*$dias_transcurrido);
            $r->dias_transcurrido=$dias_transcurrido;//agregando dias transcurrido
            $por_cobrar=number_format(($por_cobrar2)+($int*$dias_transcurrido),2);
            $r->por_cobrar=$por_cobrar;//agregando columna por cobrar


            $r->pendiente=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$total_abonos,2);
            $r->diferencia=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$diferencia,2);

        }

        
        $recuperaciones=recuperaciones::where("id_cliente",$id)->get();

        $view = \View::make('plantillas.movimiento', compact('id','cliente','pedidos','recuperaciones'))->render();

        // return view('plantillas.movimiento',compact('id','cliente','pedidos','abonos'));

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Estado de cuenta-'.$cliente->nombre." ".$cliente->apellidos.".pdf");
    }
    public function excel($id,$tipo){
        $cliente=clientes::find($id);
        $pedidos=pedidos::where("id_cliente",$id)->orderBy("id","DESC")->get(); 
           //agregando fecha de liquidacion si tiene
        foreach($pedidos as $r){
            $recupe=recuperaciones::where("id_pedido",$r->id)->where("tipo","liquidacion")->get();
            $abonos_pedido=recuperaciones::where("id_pedido",$r->id)->where("tipo","abono")->get();
            $para_diferencia=recuperaciones::where("id_pedido",$r->id)->get();
            $total_abonos=0;
            foreach($abonos_pedido as $a){
                $total_abonos+=$a->cantidad;
            }
           $r->total_abonos=$total_abonos;
           //"____________para calcular la diferencia-___________"
           $diferencia=0;
            foreach($para_diferencia as $a){
                $diferencia+=$a->cantidad;
            }
           
           //""""""""""""""""""""""""""""""""""""""""""""""""

            if (isset($recupe[0])) {
                $r->fechaliquidacion=$recupe[0]->created_at;
            }else{
                $r->fechaliquidacion="";
            }

            $descuento=0;
            if($r->descuento=='true'){
                $descuento=20;
            }else{ $descuento=0;}
            $por_cobrar2=($r->subtotal_pedido+$r->costo_envio)-($descuento + $r->pago_inicial);
            $int=($r->interes/100)*$por_cobrar2;
            $r->intereses_diarios=$int;//agregando intereses diarios

            $dias_transcurrido=0;
            $fecha_limite=new DateTime($r->fecha_limite);
            $ahora=new DateTime(date('Y-m-d'));
            
            if($fecha_limite < $ahora){
                $diff2 = $fecha_limite->diff($ahora);
                $dias_transcurrido=$diff2->days;
            }else{
                $dias_transcurrido=0;
            }
            $r->$por_cobrar2=$por_cobrar2;
            $r->intereses=$int*$dias_transcurrido;//agregar total intereses
            $r->total_con_intereses=($por_cobrar2)+($int*$dias_transcurrido);
            $r->dias_transcurrido=$dias_transcurrido;//agregando dias transcurrido
            $por_cobrar=number_format(($por_cobrar2)+($int*$dias_transcurrido),2);
            $r->por_cobrar=$por_cobrar;//agregando columna por cobrar


            $r->pendiente=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$total_abonos,2);
            $r->diferencia=number_format(($por_cobrar2)+($int*$dias_transcurrido)-$diferencia,2);

        }
       
        $abonos=recuperaciones::where("id_cliente",$id)->get();

        return Excel::download(new movimientosExport($id,$cliente,$pedidos,$abonos), 'Estado de cuenta-'." ".$cliente->nombre." ".$cliente->apellidos.'.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
