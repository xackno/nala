<?php

namespace App\Http\Controllers;

use App\Models\articulos;
use App\Models\turnos;
use App\Models\ventas;
use App\Models\salidas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cajas;
use App\Models\clientes;
use App\Models\Rutas;
use App\Models\recuperaciones;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Illuminate\Support\Facades\DB;
class ventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dia   = date("N");
        $mes   = date("m");
        $dias  = ["Lun", "Mar", "Mie", "Juv", "Vie", "Sab", "Dom"];
        $meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
        // $fecha = $dias[$dia - 1] . "  " . date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $fecha = date("d") . " " . $meses[$mes - 1];

        $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->get();
        $info_turno=$turno;    
        if(isset($turno[0])){
            $turno=$turno[0]->id;    
        }else{$turno=0;}

        if(Auth::User()->caja!=null){
            $caja=Auth::User()->caja;
            return view('forms.ventas', compact("turno","info_turno", "fecha","caja"));
        }else{
            $msj="No esposible ir a la página de VENTA. Es necesario estar logeado con un usuario que pertenezca a esta CAJA.";
            return back()->with("msj",$msj);
        }   
    }

    public function data_last_venta(Request $data){
        $last_venta=ventas::where("caja",$data->get("caja"))->orderBy("id","DESC")->first();
        $last_venta->idcero=str_pad($last_venta->id, 8, '0', STR_PAD_LEFT);
        return json_encode($last_venta);
    }

    public function reimprimir(Request $data){
        $id=$data->get("id");

        $venta=ventas::find($id);
        $nombre_caja=$venta->caja;

        $id_p=$venta->id_productos;
        $id_p=explode(",", $id_p);
        $cantidad=$venta->cantidad_pro;
        $cantidad=explode(",", $cantidad);
        $precio=$venta->precio_vendido;
        $precio=explode(",", $precio);
        $subtotal=[];
        for($x=0;$x<count($cantidad);$x++){
            $sub=$cantidad[$x]*$precio[$x];
            array_push($subtotal, $sub);
        }

        $unidad=[];
        $descripcion=[];

        foreach($id_p as $r){
            $articulo=articulos::find($r);
            array_push($unidad, $articulo->unidad);
            array_push($descripcion,$articulo->descripcion_articulo);
        }
        
        $this->ticket_for_client_printer($venta->usuario,$nombre_caja,$id,$venta->efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal,$venta->created_at);
        return json_encode(true);
        
     
    }
    public function total_venta_x_turno(Request $data){
        $id=$data->get("id");
        $turno=turnos::find($id);

        $ventas=ventas::select(DB::raw("SUM(total_venta) as total"))->where("id_turno",$id)->first();
        $total_venta=$ventas->total;
        $salidas=salidas::select(DB::raw("SUM(cantidad) as total_salida"))->where("turno",$id)->first();
        $total_salida=$salidas->total_salida;

        $recuperaciones=recuperaciones::select(DB::raw("SUM(cantidad) as total_rec"))->where("turno",$id)->first();
        $total_recuperaciones=$recuperaciones->total_rec;

        $total=($total_venta + $turno->inicio) - $total_salida;
        $total+=$total_recuperaciones;
        return json_encode($total);
    }

    public function paraPedidos()
    {
        $dia   = date("N");
        $mes   = date("m");
        $dias  = ["Lun", "Mar", "Mie", "Juv", "Vie", "Sab", "Dom"];
        $meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
        // $fecha = $dias[$dia - 1] . "  " . date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $fecha = date("d") . " " . $meses[$mes - 1];

        $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->get();
        $info_turno=$turno;    
        if(isset($turno[0])){
            $turno=$turno[0]->id;    
        }else{$turno=0;}


        $ip=$_SERVER['REMOTE_ADDR'];
        $caja=Cajas::where("ip","=",$ip)->first();
        if ($caja!=null) {
            $miip=$caja->ip;
        }else{
            $miip="";
        }

        $msj="F8";
        $clientes=clientes::all();
        $rutas=rutas::all();

        if(Auth::User()->caja!=null){
            $caja=Auth::User()->caja;
            return view('forms.ventas', compact("turno","info_turno", "fecha","caja","msj","clientes","rutas"));
        }else{
            $msj="No esposible ir a la página de PEDIDOS. Es necesario estar logeado con un usuario que pertenezca a esta CAJA.";
            return back()->with("msj",$msj);
        }
    }



    function GetMAC(){
        ob_start();
        system('getmac');
        $Content = ob_get_contents();
        ob_clean();
        return substr($Content, strpos($Content,'\\')-20, 17);
    }


    public function informes_ventas(){
        $ventas=ventas::orderBy("id","DESC")->paginate(25);
        return view("forms.list_ventas",compact('ventas'));
    }










    public function buscar_venta(Request $data)
    {
        $id_venta = $data->get("id_venta");
        $venta    = ventas::find($id_venta);
        $fecha    = $venta->created_at;
        $fecha    = date("Y-m-d H:i:s", strtotime($fecha));

        $id_pro   = $venta->id_productos;
        $id_pro   = explode(",", $id_pro);
        $cantidad = $venta->cantidad_pro;
        $cantidad = explode(",", $cantidad);
        $precio = $venta->precio_vendido;
        $precio = explode(",", $precio);
        $tipo_precio=$venta->tipo_precio_vendido;
        $tipo_precio=explode(",",$tipo_precio);

        $unidad=[];
        $nombres=[];
        for($x=0;$x<count($id_pro);$x++){
            $articulo=articulos::find($id_pro[$x]);
            array_push($unidad,$articulo->unidad);
            array_push($nombres,$articulo->descripcion_articulo);
        }
        return compact('venta', "fecha",'id_pro','cantidad','precio','tipo_precio', 'unidad','nombres');
    }
    public function devolucion(Request $data)
    {
        $id_venta    = $data->get("id_venta");
        $total_venta = $data->get("total_venta");
        $id_p        = $data->get("ip_p");
        $cantidad    = $data->get("cantidad");

        //buscar la venta
        $venta = ventas::find($id_venta);
        $cantidades=$venta->cantidad_pro;
        $cantidades=explode(",",$cantidades);
        $y=0;
        $nueva_cantidades=[];
        foreach($cantidad as $can){
            array_push($nueva_cantidades,$cantidades[$y] - $can);
            $y++;
        }

        try {
            $venta->update([
                "efectivo"     => "devolucion",
                "total_venta"  => $total_venta,
                "cantidad_pro" => implode(",",$nueva_cantidades),
                "id_productos" => implode(",", $id_p),
            ]);
            $x=0;
            foreach($id_p as $id){//restando existencia
               $articulo=articulos::find($id);
                // echo $articulo->descripcion_articulo;
                $articulo->update(["existencia"=>$articulo->existencia+$cantidad[$x]]);
               $x++;

            }

            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function ejecutar_venta(Request $data)
    {
        date_default_timezone_set('america/mexico_city');
        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $tipo_precio = $data->get("tipo_precio");
        $subtotal    = $data->get("subtotal");
        $total       = $data->get("total");
        $efectivo    = $data->get("efectivo");
        $user        = $data->get("user");
        $id_turno    = $data->get("id_turno");
        $nombre_caja        = $data->get("caja");      

        $cambio=$efectivo-$total;

            try {
                \DB::beginTransaction();
                $venta = new ventas();
                $venta->fill([
                    "usuario"      => $user,
                    "id_turno"     => $id_turno,
                    "caja"         => $nombre_caja,
                    "efectivo"     => $efectivo,
                    "total_venta"  => array_sum($subtotal),
                    "cantidad_pro" => implode(",", $cantidad),
                    "id_productos" => implode(",", $id_p),
                    "precio_vendido"=>implode(",",$precio),
                    "tipo_precio_vendido"=>implode(",",$tipo_precio)
                ]);
                $venta->push();
                \DB::commit();
                $idv=$venta->id;
                $created_at=$venta->created_at;
                
                $x=0;
                foreach($id_p as $id){//restando existencia
                   $articulo=articulos::find($id);
                   $articulo->update(["existencia_stock"=>$articulo->existencia_stock-$cantidad[$x]]);
                   $x++;
                }

            ////___________IMPRIMIR TICKET####################################################################
            if($data->get("ticket")=="true"){
               // $this->ticketventa($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal); 
               $this->ticket_for_client_printer($user,$nombre_caja,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal,$created_at);
               $status="  VENTA CORRECTA CON TICKET  #".str_pad($idv, 8, '0', STR_PAD_LEFT);
           }else{
            $status="  VENTA CORRECTA  #".str_pad($idv, 8, '0', STR_PAD_LEFT);
           }
            
        } catch (Throwable $e) {
            \DB::rollback();
            $status = "ERROR al realizar la venta";
        }

        return json_encode($status);
    }

    function ticketventa($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal){
        $nombreImpresora = "XP-58";//POS-58
        $connector = new WindowsPrintConnector($nombreImpresora);
        $im = new Printer($connector);
        $im->setJustification(Printer::JUSTIFY_CENTER);
        // $im->setTextSize(1, 1);
        $idventa=str_pad($idv, 8, '0', STR_PAD_LEFT);
        $im->text("VENTA:#".$idventa."\n Atendio:".Auth::user()->name." Fecha:".date("d-m-Y")."\n");
        $im->text("|DESC |CANT| UNID | $ | TOTAL\n_________________________________");
        for($x=0;$x< count($id_p);$x++){
        $im->text($descripcion[$x]."\n".$cantidad[$x]." ".$unidad[$x]." X ".$precio[$x]."=".$subtotal[$x]);    
        }
        $im->setJustification(Printer::JUSTIFY_RIGHT);
        $total=array_sum($subtotal);
        $im->text("\npago:$".number_format($efectivo,2, '.', '')."\n TOTAL:$".number_format($total,2, '.', '')."\n cambio:$".number_format(($efectivo-$total),2, '.', ''));
        
        $im->feed(5);
        $im->setJustification(Printer::JUSTIFY_LEFT);
        // $im->close();
    }

    function ticket_for_client_printer($user,$caja,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal,$created_at){//guarda la informacion de la venta en el archivo imprimir.json 
        $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $created_at=explode(" ",$created_at);
        $fecha=$created_at[0];
        $hora=$created_at[1];

        $ticket_json=array("proceso"=>$proceso,"tipo"=>"venta","caja"=>$caja,'cajero'=>$user,"fecha"=>$fecha,'hora'=>$hora,'venta'=>$idv,"total"=>array_sum($subtotal),"efectivo"=>$efectivo,
        'id_art'=>$id_p,'cantidad'=>$cantidad,'unidad'=>$unidad,'descripcion'=>$descripcion,'precio'=>$precio,'importe'=>$subtotal);
        $json_venta=json_encode($ticket_json,JSON_PRETTY_PRINT);
        \Storage::disk('files')->put("imprimir.json",$json_venta);
    }

    public function cotizacion(Request $data)
    {

        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $subtotal    = $data->get("subtotal");

        $id_p        = explode(",", $id_p);
        $cantidad    = explode(",", $cantidad);
        $unidad      = explode(",", $unidad);
        $precio      = explode(",", $precio);
        $subtotal    = explode(",", $subtotal);
        $descripcion = explode("=>", $descripcion);

        // return view('plantillas.cotizacion', compact('id_p','cantidad','unidad','descripcion','precio','subtotal'));

        $view = \View::make('plantillas.cotizacion', compact('id_p', 'cantidad', 'unidad', 'descripcion', 'precio', 'subtotal'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Formato salida-' . date('d-m-Y'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
