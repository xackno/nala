<?php

namespace App\Http\Controllers;

use App\Models\salidas;
use App\Models\turnos;
use App\Models\ventas;

class informesController extends Controller
{
    public function salidas()
    {
        $salidas = salidas::select("salidas.id", "name","usuario", "turno","salidas.caja", "cantidad", "concepto", "salidas.created_at")
            ->join("users", "salidas.usuario", "users.id")
            ->orderBy('id', 'DESC')
            ->paginate(25);
        return view("forms.salidas", compact('salidas'));
    }
    public function turnos()
    {
        $turnos = turnos::select("turnos.id","turnos.usuario", "name","turnos.caja", "inicio", "cierre_system","cierre_conteo", "comentario1", "comentario2", "fecha_fin", "turnos.status", "turnos.created_at")
            ->join("users", "turnos.usuario", "users.id")
            ->orderBy("turnos.id", "DESC")
            ->get();

        return view("forms.turnos", compact('turnos'));
    }
    public function ventas()
    {
        $ventas = ventas::orderBy("id", "DESC")->get();
        return view("forms.list_ventas", compact('ventas'));
    }
    public function movimientos()
    {

    }
}
