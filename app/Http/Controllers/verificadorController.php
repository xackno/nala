<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\articulos;
class verificadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sin_busqueda=true;
        return view("verificador",compact('sin_busqueda'));
    }

    public function buscar(Request $data){
        $busqueda=$data->get("buscar");
         $articulo=articulos::select('articulos.id as id_p','clave','descripcion_articulo','unidad','precio_caja','pre_compra','pre_unidad','pre_caja','pre_paquete','pre_mayoreo','pre_membrecia','codigo_sat', 'existencia_stock','mayoreo_apartir','ubicacion_producto','fotos','codigo_barra','caducidad','descripcion_catalogo','articulos.updated_at')
        ->orWhere('descripcion_articulo','LIKE', "%$busqueda%")
        ->orWhere('palabra_clave','LIKE', "%$busqueda%")
        ->orWhere('clave','LIKE', "$busqueda")
        ->orWhere('codigo_barra','LIKE', "$busqueda")
        ->orWhere('caducidad','LIKE', "$busqueda")
        ->orWhere('marca','LIKE', "%$busqueda%")
        ->first();



        return view("verificador",compact('articulo'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
