<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ofertas extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'id_producto',
        'unidad',
        'descripcion_producto',
        'precio_venta',
        'precio_oferta',
        'fecha_valido',
        'foto',
        'url',
        'created_at',
        'updated_at'
    ];
}
