<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class provedores extends Model
{
    use HasFactory;
    protected $fillable = [
    	'id',
    	'nombre',
    	'descripcion',
    	'rfc',
    	'domicilio',
    	'tel',
    	'email'
		];
}
