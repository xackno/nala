<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rutas extends Model
{
    use HasFactory;
     protected $fillable = [
        'id',
        'nombre_ruta',
        'longitud_ruta',
        'tiempo_recorrido_total',
        'origen',
        'destino',
        'id_secciones',
        'nombre_secciones',
        'tiempo_secciones',
        'latitud_seccion',
        'created_at',
        'updated_at'
    ];
}
