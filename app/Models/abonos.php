<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class abonos extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'id_cliente',
        'cantidad_abonado',
        'created_at',
        'updated_at',
    ];
}
