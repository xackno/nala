<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class pedidos extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'id_cliente',
        "cajero",
        "caja",
        "turno",
        "referencia",
        'id_ruta',
        'status',
        'status_recuperacion',
        'historial_status',
        "factura",
        "nota",

        'subtotal_pedido',//precio de todos los productos
        'costo_envio',
        'pago_inicial',
        'descuento',//aplica a un descuento de 20pesos
        'interes',
        'fecha_limite',

        'status_disminucion',
        "id_articulos",
        "cantidad",
        "unidad",
        "descripcion",
        "precio_vendido",
        "tipo_precio_vendido",
        "subtotal_articulos",
        'created_at',
        'updated_at'
    ];

}
