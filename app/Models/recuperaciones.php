<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recuperaciones extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'id_cliente',
        'cajero',
        'caja',
        'turno',
        'tipo',
        'id_pedido',
        'cantidad',
        'created_at',
        'Update_at'
        ];
}
