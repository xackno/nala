<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    use HasFactory;
    protected $fillable = [
            "id",
            'nombre',
            'apellidos',
            'municipio',
            'localidad',
            'calle',
            'telefono',
            'correo',
            'usuarioApp',
            'password',
            'status',
            'foto',
            'nivel',
            'calificacion',
            'monedero',
            'identificacion',
            'id_ruta',
            'limite_credito',
            'created_at',
            'updated_at'
    ];
}
