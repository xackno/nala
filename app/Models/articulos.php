<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class articulos extends Model
{
    use HasFactory;
    
     protected $fillable = [
      'id',
      'clave',
      'descripcion_articulo',
      'unidad',
      'inv_min',
      'inv_max',
      'existencia_bodega',
      'existencia_stock',
      'inv_compra',

      'codigo_barra',
      'codigo_sat',
      'a_granel',
      'en_catalogo',
      'ventas_negativas',
      
      'precio_caja',
      'num_paquete',
      'pza_x_caja',
      'mayoreo_apartir',
      'ganancia_unidad',
      'ganancia_caja',
      'ganancia_mayoreo',
      'ganancia_membrecia',
      'pre_compra',
      'pre_unidad',
      'pre_caja',
      'pre_paquete',
      'pre_mayoreo',
      'pre_membrecia',
      'proveedor',
      'categoria',
      'linea',
      'marca',
      'local',
      'ubicacion_producto',
      'palabra_clave',
      'caducidad',
      'url',
      'fotos',
      'descripcion_catalogo',
      'created_at',
      'updated_at'
    ];
}
