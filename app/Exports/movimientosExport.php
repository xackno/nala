<?php

namespace App\Exports;

use App\Models\clientes;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class movimientosExport implements FromView
{
    //$id,$cliente,$pedidos,$abonos
   protected $id;
    protected $cliente;
    protected $pedidos;
    protected $abonos;
      public function __construct($id=null,$cliente=null,$pedidos=null,$abonos=null)
      {
          $this->id=$id;
          $this->cliente=$cliente;
          $this->pedidos=$pedidos;
          $this->abonos=$abonos;
      }

    public function view():View{
        $id=$this->id;
        $cliente=$this->cliente;
        $pedidos=$this->pedidos;
        $abonos=$this->abonos;
        return view('excel.movimiento',compact('id','cliente','pedidos','abonos'));
    }
}
