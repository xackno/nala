@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: 3px;padding-right: 3px">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" >
            	<div class="card-header text-white" style="background:#28a745">
            		 <h4><i class="fas fa-route"></i> Rutas <span class="float-right">ID {{$ruta->id}}</span></h4>
            	</div>
            	<div class="card-body">
            		<div style="width: 100%" id="msj"></div>
            		<div class="row">
            			<div class="col-xl-3 col-md-6 mb-4">
            				<input type="number" class="d-none"  id="id_ruta" value="{{$ruta->id}}">
						<label >Nombre</label>
		        		<input type="text" id="nombre_ruta" class="form-control" value="{{$ruta->nombre_ruta}}">
		        		<label >Longitud total<span class="text-info">Aproximado</span><b>(metros)</b></label>
		        		<input type="number" id="longitud_ruta" class="form-control" value="{{$ruta->longitud_ruta}}">
						<label>tiempo de recorrido total</label>
						<input type="text" id="tiempo_recorrido_total" class="form-control" value="{{$ruta->tiempo_recorrido_total}}">
		        		
						
				    </div>
				    <div class="col-xl-3 col-md-6 mb-4">
				    	<label >Origen</label>
		        		<input type="text" id="origen" class="form-control" placeholder="Latitud de origen" value="{{$ruta->origen}}" >
		        		<label >Destino</label>
		        		<input type="text" id="destino" class="form-control" value="{{$ruta->destino}}" >
				    </div>
				    <div class="col-xl-6 col-md-6 mb-4">
				    	<h6><b>Agregar secciones</b></h6>
				    	<div style="width: 100%">
				    		<label style="width: 30%" class="text-danger">Nombre </label>
				    	<label style="width: 30%;margin: 0px;padding: 0px" class="text-info">Tiempo desde el origen</label>
				    	<label style="width: 38%" class="text-success">ubicación</label>
				    	</div>
				    	<div class="input-group">
				    		<input type="text" id="nombre_seccion" class="form-control" placeholder="nombre" style="width: 30%">

				    		<input type="text" id="tiempo_origen" class="form-control" placeholder="HH:MM:SS" style="width: 30%">

				    		<input type="text" id="latitud" class="form-control" style="width: 38%">

				    	</div>
				    	<button class="btn btn-info btn-sm" id="agregarsecion">Agregar</button>
				    	<div class="table-responsive">
				    		<table id="tabla_secciones" class="table">
				    		<thead class="table-dark">
				    			<tr>
				    			<th>ID</th>
				    			<th>Nombre</th>
				    			<th>tiempo</th>
				    			<th >ubicación</th>
				    			<th><i class="fa fa-cog"></i></th>
				    		</tr>
				    		</thead>
				    		
				    		<tbody>
				    			<?php

				    		$latitud_seccion=$ruta->latitud_seccion;
							$latitud_seccion=explode("%", $latitud_seccion);
							$cantSeccion=count($latitud_seccion);

							$ultimo_id_seccion=0;
							for($x=1;$x<$cantSeccion;$x++){
							$id_secciones=explode("%", $ruta->id_secciones);
							$nombre_secciones=explode("%",$ruta->nombre_secciones);
							$tiempo_secciones=explode("%",$ruta->tiempo_secciones);
				    			?>
				    			<tr>
				    				<td>{{$id_secciones[$x]}}</td>
				    				<td>{{$nombre_secciones[$x]}}</td>
				    				<td>{{$tiempo_secciones[$x]}}</td>
				    				<td>{{$latitud_seccion[$x]}}</td>
				    				<td><button class="btn btn-danger btn-sm" onclick="eliminarPRO();"><i class="fa fa-trash"></i></button></td>
				    			</tr>
				    		<?php
				    		$ultimo_id_seccion=$id_secciones[$x];
				    		}
				    		?>
				    			
				    		</tbody>
				    	</table>
	
				    	</div>
				   </div>
					
            	</div><!-- fin row -->

            	<button id="btn_guardarRuta" class="btn btn-primary btn-lg float-right"><i class="fas fa-route text-warning"></i> Guardar ruta</button>

            <div id="map" style="width: 100%;height: 500px;background:#afecde">
			</div>

            	</div> <!-- fin body card -->
           	</div>
        </div> 
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyADFLblU07CwFmsRdG7-KQfcBUcJaR7IHk&callback=initMap"></script>

<script type="text/javascript" src="{{asset('/js/gmaps.js')}}"></script>
<script type="text/javascript">
//$#################################################################
	if ($(window).width() <= 360) {
		    $(".btn_toggle").trigger("click");
		}
		$(".btn_toggle").trigger("click");

//##################click e e boton de guardar########################
	$("#btn_guardarRuta").click(function(){

		var id_seccion='';
		var nombre_seccion='';
		var tiempoDesdeOrgen='';
		var ubicacion='';
		$("#tabla_secciones tbody").find("tr td:first-child").each(function(){
                  id_seccion=id_seccion+"%"+($(this).html());
                  nombre_seccion=nombre_seccion+"%"+($(this).siblings("td").eq(0).html());
                  tiempoDesdeOrgen=tiempoDesdeOrgen+"%"+($(this).siblings("td").eq(1).html());
                  ubicacion=ubicacion+"%"+($(this).siblings("td").eq(2).html());
              });//fin each

		$.ajax({
			url:"{{route('actualizar_ruta')}}",
			type:"post",
			dataType:"json",
			data:{
				id_ruta:$("#id_ruta").val(),
				nombre:$("#nombre_ruta").val(),
				longitud_total:$("#longitud_ruta").val(),
				tiempo_total:$("#tiempo_recorrido_total").val(),
				origen:$("#origen").val(),
				destino:$("#destino").val(),
				id_seccion:id_seccion,
				nombre_seccion:nombre_seccion,
				tiempoDesdeOrgen:tiempoDesdeOrgen,
				ubicacion:ubicacion
			},success:function(data){

				if (data=="success") {
					$("#msj").html("<div class='alert alert-primary'>Actuaizado correctamente.</div>");
					setInterval(function(){
						$("#msj").html('');
					},4000);
					dibuando_mapa();
				}else{
					$("#msj").html("<div class='alert alert-danger'>Error al guardar, verifique la información</div>");
					setInterval(function(){
						$("#msj").html('');
					},4000);
				}
			},error:function(){

			}//fin error
		});//fin ajax
	});
//########Agregando secciones a la tabla#############################
	var idSeccion={{$ultimo_id_seccion+1}};
	$("#agregarsecion").click(function(){
		var nombre=$("#nombre_seccion").val();
		var tiempo=$("#tiempo_origen").val();
		var latitud=$("#latitud").val();
		if (latitud=="") {latitud=0;}
		if (tiempo=='') {tiempo="00:00:00";}
		if (nombre=='') {nombre="No especificado";}
		$("#tabla_secciones tbody").append("<tr> <td>"+idSeccion+"</td> <td contenteditable=''>"+nombre+" </td> <td contenteditable=''> "+tiempo+"</td> <td>"+latitud+" </td> <td><button class='btn btn-danger btn-sm' onclick='eliminarPRO();'><i class='fa fa-trash'></i></button></td> </tr>");
	idSeccion++;
	$("#latitud,#tiempo_origen,#nombre_seccion").val('');
	});

///################extrado datos de orgen y destino################
	<?php
		$origen=$ruta->origen;
		$origen=explode(",",$origen);
		$destino=$ruta->destino;
		$destino=explode(",", $destino);
	  ?>


//###################dibujando mapa#####################################
	dibuando_mapa();
	function dibuando_mapa(){
	var map;
	var latOrigin={{$origen[0]}};
	var lngOrigin={{$origen[1]}};
	var latDestino={{$destino[0]}};
	var lngDestino={{$destino[1]}};
      map = new GMaps({
        el: '#map',
        lat:latOrigin,
        lng:latOrigin
      });
      map.setCenter(latOrigin, lngOrigin);

      map.addMarker({ lat: latOrigin,
       lng: lngOrigin,
       title: 'SUCURSAL CENTRAL'
   });

	<?php 
		$latitud_seccion=$ruta->latitud_seccion;
		$latitud_seccion=explode("%", $latitud_seccion);
		$cantSeccion=count($latitud_seccion);

	
		for($x=1;$x<$cantSeccion;$x++){
			$Newposition=explode(",", $latitud_seccion[$x]);
			$nombre_secciones=explode("%",$ruta->nombre_secciones);
			$lastLat=0;$lastLng=0;
			if ($x>1) {
				$lastposition=explode(",", $latitud_seccion[$x-1]);
				$lastLat=$lastposition[0];
				$lastLng=$lastposition[1];
			}
		
		$newLat=$Newposition[0];
		$newLng=$Newposition[1];
		

		?>
		var newlat={{$newLat}};
		var newlng={{$newLng}};
		var lastlat={{$lastLat}};
		var lastlng={{$lastLng}};
		if ({{$x}}==1) {
			map.drawRoute({
          origin: [latOrigin, lngOrigin],  
          destination: [newlat,newlng],
          travelMode: 'driving',
          strokeColor: '#000000',
          strokeOpacity: 0.6,
          strokeWeight: 5
        });
       map.addMarker({
       	lat: newlat,
        lng: newlng,
        title:'{{$nombre_secciones[$x]}}'
       }); 
   }if({{$x}}>1){
   	map.drawRoute({
          origin: [lastlat,lastlng],  
          destination: [newlat,newlng],
          travelMode: 'driving',
          strokeColor: '#000000',
          strokeOpacity: 0.6,
          strokeWeight: 5
        });
       map.addMarker({
       	lat: newlat,
        lng: newlng,
       	title:'{{$nombre_secciones[$x]}}'
       }); 
   }//fin if script	
	<?php
	}//fin for de php	
?>



	map.addListener('click', function(e) {
	          map.drawRoute({
	          origin: [17.3423305, -98.01091079999999],  
	          destination: [e.latLng.lat(), e.latLng.lng()],
	          travelMode: 'driving',
	          strokeColor: '#000000',
	          strokeOpacity: 0.6,
	          strokeWeight: 5
	        });
	          lat = e.latLng.lat();	   
	          lng = e.latLng.lng();
	 		$("#destino").val(lat+","+lng);

	 		$("#latitud").val(lat+","+lng);
	       map.addMarker({lat: lat, lng: lng});  
	        });


}//Fin function

//##############ELIMINAR PRODUCTOS DE LA TABLA#####################
  var eliminarPRO=function(){
    $("#tabla_secciones tbody").on("click","tr td:last-child",function(){
      var a=$(this).parent();
      //alert(a.html());
      $(a).remove();
    });
      
    }


    

</script>

@endsection