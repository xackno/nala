@extends('layouts.app')
@section('content')

<div class="container-fluid" style="margin-top: 20px;">
	<div class="row justify-content-center">
        <div class="col-md-12">
   
            		<div style="width: 100%" id="msj"></div>
            		<div class="row">
            			<div class="col-xl-3 col-md-6 mb-4">
						<label >Nombre</label>
		        		<input type="text" id="nombre_ruta" class="form-control" placeholder="Nombre">
		        		<label >Longitud total<span class="text-info">Aproximado</span><b>(metros)</b></label>
		        		<input type="number" id="longitud_ruta" class="form-control" placeholder="En Metros">
						<label>tiempo de recorrido total</label>
						<input type="text" id="tiempo_recorrido_total" class="form-control" placeholder="HH:MM:SS">
		        		
						
				    </div>
				    <div class="col-xl-3 col-md-6 mb-4">
				    	<label >Origen</label>
		        		<input type="text" id="origen" class="form-control" placeholder="Latitud de origen" readonly="">
		        		<label >Destino</label>
		        		<input type="text" id="destino" class="form-control" placeholder="Latitud de destino" readonly="">
				    </div>
				    <div class="col-xl-6 col-md-6 mb-4">
				    	<h6><b>Agregar secciones</b></h6>
				    	<div style="width: 100%">
				    		<label style="width: 30%" class="text-danger">Nombre </label>
				    	<label style="width: 30%;margin: 0px;padding: 0px" class="text-info">Tiempo desde el origen</label>
				    	<label style="width: 38%" class="text-success">ubicación</label>
				    	</div>
				    	<div class="input-group">
				    		<input type="text" id="nombre_seccion" class="form-control" placeholder="nombre" style="width: 30%">

				    		<input type="text" id="tiempo_origen" class="form-control" placeholder="HH:MM:SS" style="width: 30%">

				    		<input type="text" id="latitud" class="form-control" style="width: 38%">

				    	</div>
				    	<button class="btn btn-info btn-sm" id="agregarsecion">Agregar</button>
				    	<div class="table-responsive">
				    		<table id="tabla_secciones" class="table">
				    		<thead class="table-dark">
				    			<tr>
				    			<th>ID</th>
				    			<th>Nombre</th>
				    			<th>tiempo</th>
				    			<th >ubicación</th>
				    			<td><button class="btn btn-danger btn-sm"><i class="fa fa-cog"></i></button></td>
				    		</tr>
				    		</thead>
				    		
				    		<tbody>
				    			
				    		</tbody>
				    	</table>
	
				    	</div>
				   </div>
					
            	</div><!-- fin row -->

            	<button id="btn_guardarRuta" class="btn btn-primary btn-lg float-right"><i class="fas fa-route text-warning"></i> Guardar ruta</button>
            	<p>Dibuje su ruta en el mapa</p>
            <div id="map" style="width: 100%;height: 500px;background:#afecde">
			</div>

        </div> 
    </div>
    <br><br><br>
    <div class="text-center" style="margin:auto" class="text-secondary">&copy Stehs, {{date('Y')}}</div>
    <br><br>
</div>


@endsection

@section('script')

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyADFLblU07CwFmsRdG7-KQfcBUcJaR7IHk&callback=initMap"></script>

<script type="text/javascript" src="{{asset('/js/gmaps.js')}}"></script>
<script type="text/javascript">

//##################click e e boton de guardar########################
	$("#btn_guardarRuta").click(function(){

		var id_seccion='';
		var nombre_seccion='';
		var tiempoDesdeOrgen='';
		var ubicacion='';
		$("#tabla_secciones tbody").find("tr td:first-child").each(function(){
          id_seccion=id_seccion+"%"+($(this).html());
          nombre_seccion=nombre_seccion+"%"+($(this).siblings("td").eq(0).html());
          tiempoDesdeOrgen=tiempoDesdeOrgen+"%"+($(this).siblings("td").eq(1).html());
          ubicacion=ubicacion+"%"+($(this).siblings("td").eq(2).html());
      });//fin each

				$.ajax({
					url:"{{route('guardar_ruta')}}",
					type:"post",
					dataType:"json",
					data:{
						nombre:$("#nombre_ruta").val(),
						longitud_total:$("#longitud_ruta").val(),
						tiempo_total:$("#tiempo_recorrido_total").val(),
						origen:$("#origen").val(),
						destino:$("#destino").val(),
						id_seccion:id_seccion,
						nombre_seccion:nombre_seccion,
						tiempoDesdeOrgen:tiempoDesdeOrgen,
						ubicacion:ubicacion
					},success:function(data){

						if (data=="success") {
							$("#msj").html("<div class='alert alert-success'>Guardado correctamente.</div>");
							setInterval(function(){
								$("#msj").html('');
							},4000);
							$("#nombre_ruta,#longitud_ruta,#tiempo_recorrido_total,#destino,#latitud,#tiempo_origen,#nombre_seccion").val('');
								$("#tabla_secciones tbody").html('');
								dibuando_mapa();
						}else{
							$("#msj").html("<div class='alert alert-danger'>Error al guardar, verifique la información</div>");
							setInterval(function(){
								$("#msj").html('');
							},4000);
						}
					},error:function(){

					}//fin error
				});//fin ajax
	});
//########Agregando secciones a la tabla#############################
	var idSeccion=1;
	$("#agregarsecion").click(function(){
		var nombre=$("#nombre_seccion").val();
		var tiempo=$("#tiempo_origen").val();
		var latitud=$("#latitud").val();
		if (latitud=="") {latitud=0;}
		if (tiempo=='') {tiempo="00:00:00";}
		if (nombre=='') {nombre="No especificado";}
		$("#tabla_secciones tbody").append("<tr> <td>"+idSeccion+"</td> <td contenteditable=''>"+nombre+" </td> <td contenteditable=''> "+tiempo+"</td> <td>"+latitud+" </td> <td><button class='btn btn-danger btn-sm' onclick='eliminarPRO();'><i class='fa fa-trash'></i></button></td> </tr>");
	idSeccion++;
	$("#latitud,#tiempo_origen,#nombre_seccion").val('');
	});


//###################dibujando mapa#####################################
	dibuando_mapa();
	function dibuando_mapa(){
	var map;
      map = new GMaps({
        el: '#map',
        lat: 17.3423305 ,
        lng: -98.01091079999999
      });
       map.addMarker({ lat: 17.3423305, lng: -98.01091079999999}); 
      //  GMaps.geolocate({
      //   success: function(position){
      //     lat = position.coords.latitude;  
      //     lng = position.coords.longitude;
      //     map.setCenter(lat, lng);
      //     map.addMarker({ lat: lat, lng: lng}); 
          $("#origen").val("17.3423305,-98.01091079999999"); 
      //   },
      //   error: function(error){
      //     alert('Geolocalización fallado: '+error.message);
      //   },
      //   not_supported: function(){
      //     alert("Your browser does not support geolocation");
      //   }
      // });

		map.addListener('click', function(e) {
          map.drawRoute({
          origin: [17.3423305, -98.01091079999999],  
          destination: [e.latLng.lat(), e.latLng.lng()],
          travelMode: 'driving',
          strokeColor: '#000000',
          strokeOpacity: 0.6,
          strokeWeight: 5
        });
          lat = e.latLng.lat();	   
          lng = e.latLng.lng();
 		$("#destino").val(lat+","+lng);

 		$("#latitud").val(lat+","+lng);
       map.addMarker({lat: lat, lng: lng});  
        });

}


//##############ELIMINAR PRODUCTOS DE LA TABLA#####################
  var eliminarPRO=function(){
    $("#tabla_secciones tbody").on("click","tr td:last-child",function(){
      var a=$(this).parent();
      //alert(a.html());
      $(a).remove();
    });
      
    }


	 
    

</script>

@endsection