@extends('layouts.app')
@section('content')
<div class="container-fluid" style="font-size:.8em">
	
@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif

	<div class="input-group">
		<button class="btn btn-success" id="modo_completo">Modo completo</button>
		<button class="btn btn-secondary" id="modo_simple">Modo simple</button>
	</div>
	<br>

<!-- #################################################form modo completo############################################ -->
  <form  action="{{route('store_articulo')}}" method="post" onsubmit="return checkSubmit();" id="form_completo">
  		@csrf
		<div class="row">
	  		<div class="col-3">
	  			<label>Nombre o descripción</label>
	  			<input type="text" name="descripcion_articulo" class="form-control " placeholder="descripción" required="" autofocus>

	  			<div class="row">
	  				<div class="col">
	  					<div class="form-group">
	  						<label>Clave</label>
	  						<input type="text" name="clave" class="form-control" placeholder="Clave" required >
	  					</div>
	  				</div>
	  				<div class="col">
	  					<label>Unidad</label>
	      			<select class="form-control" name="unidad" required="" required>
	      				<option  disabled="">Seleccione uno</option>
	      				<option value="pieza" selected>Pieza</option>
	      				<option value="kilogramo">Kilogramo</option>
	      				<option value="litro">Litro</option>
	      				<option value="gramos">Gramos</option>
	      				<option value="bolsa">bolsa</option>
	      				<option value="caja">Caja</option>
	      				<option value="bulto">Bulto</option>
	      				<option value="paquete">Paquete</option>
	      			</select>
	  				</div>
	  			</div>
	  			<hr margin="3px">
	  			<div class="row">
	  				<label class="text-center" style="margin-left: 33%;">STOCK PIEZAS	</label>
	  			</div>

	  			<div class="row">
	  				<div class="col">
	  					<label>Inv. min.</label>
	  					<input type="number" name="inv_min" class="form-control" value="1" required>
	  				</div>
	  				<div class="col">
	  					<label>Inv. max.</label>
	  					<input type="number" name="inv_max" class="form-control" value="100">
	  				</div>
	  				<div class="col">
	  					<label>Exist. bodega</label>
	  					<input type="number" step="any" name="existencia_bodega" class="form-control">
	  				</div>
	  			</div>
	  			
	  			
	  			
	  			
	  			<div class="row">
	  				<div class="col"><label>Existencia stock</label></div>
	  				<div class="col text-danger"><label>Inv. compra</label></div>
	  			</div>
	  			<div class="row">
	  				<div class="col">
	  					<input type="number" step="any" name="existencia_stock" class="form-control">
	  				</div>
	  				<div class="col">
	  					<input type="number" step="any" name="inv_compra" class="form-control">
	  				</div>
	  			</div>
	  			<!-- <input type="number" name="existencia" class="form-control " style="margin-right: 60%;width: 40%;"> -->
	  			<hr>
	  			<div class="row">
	  				<div class="col">
	  					<label>Código de barra</label>
	  					<input type="number" name="codigo_barra" class="form-control">
	  				</div>
	  				<div class="col">
	  					<label>Código SAT</label>
	  					<input type="number" name="codigo_sat" class="form-control">
	  				</div>
	  			</div>
	  		</div>
	  		<div class="col-5">
	  			<h5 class="text-center"><b>CAJA</b></h5>
	  			<hr>
	  			<div class="row">
	  				<div class="col">
	  					<label>Precio por caja</label>
	  					<input type="number" step="any" name="precio_caja" class="form-control " placeholder="$">
	  				</div>
	  				<div class="col">
	  					<label>No. Paquete/bolsas</label>
	  					<input type="number" step="any" name="num_paquete" class="form-control" placeholder="En la caja" >
	  				</div>
	  				<div class="col">
	  					<label>unidades</label>
	  					<input type="number" step="any" name="pza_x_caja" class="form-control" placeholder="en la caja">
	  				</div>
	  			</div>
	  			<label>Mayoreo a partir de:</label>
	  			<input type="number"  step="any" name="mayoreo_apartir" class="form-control col-3" title="Cantidad de unidades que se consideran como venta a mayoreo">
	  			
	  			<h5 class="text-center text-success"><b>% DE GANANCIAS</b></h5>
	  			<hr>
	  			<div class="row">
	  				<div class="col">
	  					<label>Unidad</label>
	  					<input type="number" step="any" name="ganancia_unidad" min="1" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por venta individual">
	  				</div>
	  				<div class="col">
	  					<label>Caja</label>
	  					<input type="number" step="any" name="ganancia_caja" min="1" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por ventas por caja">
	  				</div>
	  				<div class="col">
	  					<label>Mayoreo</label>
	  					<input type="number" step="any" name="ganancia_mayoreo" min="1" max="100" class="form-control" placeholder="%"  title="Porcentaje de ganancia  por venta de unidades a mayoreo">
	  				</div>
	  				<div class="col">
	  					<label>Membrecía</label> 
	  					<input type="number" step="any" name="ganancia_membrecia" min="1" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por ventas a clientes con membrecía">
	  				</div>
	  			</div>
	  			
	  			<h5 class="text-center text-primary"><b>PRECIOS</b></h5>
	  			<div class="row">
	  				<div class="col d-none">
	  					<label>Compra</label>
	  					<input type="number" step="any" name="pre_compra" class="form-control" placeholder="$" title="Precio a la que se adquiere el articulo al provedor.">
	  				</div>
	  				<div class="col">
	  					<label>Unidad</label>
	  					<input type="number" step="any" name="pre_unidad" class="form-control" placeholder="$" title="Precio de una unidad">
	  				</div>
	  				<div class="col">
	  					<label>Caja</label>
	  					<input type="number" step="any" name="pre_caja" class="form-control" placeholder="$" readonly title="precio de una caja">
	  				</div>
	  				<div class="col">
	  					<label>Paquete</label>
	  					<input type="number" step="any" name="pre_paquete" class="form-control" placeholder="$" title="Precio de paquete o bolsa" readonly>
	  				</div>
	  				<div class="col">
	  					<label>Mayoreo</label>
	  					<input type="number" step="any" name="pre_mayoreo" class="form-control" placeholder="$" readonly title="Precio de unidades a mayoreo">
	  				</div>
	  				<div class="col">
	  					<label>Membrecía</label>
	  					<input type="number" step="any" name="pre_membrecia" class="form-control" placeholder="$" readonly title="Precio a clientes con membrecia">
	  				</div>
	  			</div>
	  			
	  		</div>
	  		<div class="col-1">
	  			<label>Provedor</label>
	  			<select name="proveedor" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($provedores as $r)
	  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Categoria</label>
	  			<select name="categoria" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($categorias as $r)
	  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Linea</label>
	  			<select name="linea" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($lineas as $r)
	  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Marca</label>
	  			<select name="marca" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($marcas as $r)
	  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>
	  		</div>
	  		<div class="col-3">
	  			<label for="ubicacion_producto">Ubicación del producto</label>
					<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%">
					<select class="col-4" id="pasillo">
						<option>Tienda</option>
						<option>Bodega</option>
						<option>Sucursal</option>
						<option>Pasillo 1</option>
						<option>Pasillo 2</option>
					</select>
					<select class="col-4" id="anden">
						<option>CAMARA</option>
						<option>Anden 1</option>
						<option>Anden 2</option>
						<option>Anden 3</option>
						<option>Anden 4</option>
					</select>
					<select class="col-3" id="vitrina">
						<option> 0</option>
						<option>Vitrina 1</option>
						<option>Vitrina 2</option>
						<option>Vitrina 3</option>
						<option>Vitrina 4</option>
					</select>
					<br>


					<label>Palabras clave</label>
					<input type="text" name="palabra_clave" class="form-control">

					<label>Caducidad</label>
					<input type="date" name="caducidad" class="form-control">

					<!-- <label>URL</label>
					<input type="text" name="url" class="form-control">
		 			-->
					<label>Foto</label>
					<input type="text" name="fotos" id="fotos" class="form-control" readonly>

					<label>Descripción para catálogo</label>
					<textarea name="descripcion_catalogo" class="form-control"></textarea>
	  		</div>
		</div>
		<br>
		<div class="row">
			<div class="input-group">
	  			<label>A granel</label>
	  			<input type="checkbox" name="a_granel" style="margin-right: 15px">

	  			<label for="en_catalogo">En catálogo</label>
	  			<input type="checkbox" name="en_catalogo" style="margin-right: 15px">

	  			<label for="ventas_negativas">Venta en -0</label>
	  			<input type="checkbox" name="ventas_negativas" >
			</div>
		</div>
		<br>



	  <button type="submit" class="btn btn-primary float-right"><i class="fas fa-save"></i> Guardar</button>
  </form>
<!-- #################################################form modo simple############################################# -->
	<form  action="{{route('store_articulo')}}" method="post" onsubmit="return checkSubmit();" id="form_simple">
	  		@csrf
			<div class="row">
		  		<div class="col-3">
		  			<label>Nombre o descripción</label>
		  			<input type="text" name="descripcion_articulo" class="form-control " placeholder="descripción" required="" autofocus>

		  			<div class="row">
		  				<div class="col">
		  					<div class="form-group">
		  						<label>Clave</label>
		  						<input type="text" name="clave" class="form-control" placeholder="Clave" required >
		  					</div>
		  				</div>
		  				<div class="col">
		  					<label>Unidad</label>
		      			<select class="form-control" name="unidad" required="" required>
		      				<option  disabled="">Seleccione uno</option>
		      				<option value="pieza" selected>Pieza</option>
		      				<option value="kilogramo">Kilogramo</option>
		      				<option value="litro">Litro</option>
		      				<option value="gramos">Gramos</option>
		      				<option value="bolsa">bolsa</option>
		      				<option value="caja">Caja</option>
		      				<option value="bulto">Bulto</option>
		      				<option value="paquete">Paquete</option>
		      			</select>
		  				</div>
		  			</div>
		  			

		  			<label>Existencia</label>
		  			<input type="number" name="existencia_stock" class="form-control " style="margin-right: 60%;width: 40%;">
		  			<div class="row">
		  				<div class="col">
		  					<label>Código de barra</label>
		  					<input type="number" name="codigo_barra" class="form-control">
		  				</div>
		  			</div>
						<br>
		  			<label>A granel</label>
		  			<input type="checkbox" id="checkbox">
		  			<input type="hidden" name="a_granel" value="off">
		  		</div>
		  		<div class="col-4">
		  			<div class="row">
		  				<div class="col">
		  					<label>Precio de Compra</label>
		  					<input type="number" step="any" name="pre_compra" class="form-control" placeholder="$" title="Precio a la que se adquiere el articulo al provedor.">
		  				</div>
		  				<div class="col">
		  					<label>Mayoreo a partir de:</label>
		  					<input type="number"  step="any" name="mayoreo_apartir" class="form-control" title="Cantidad de unidades que se consideran como venta a mayoreo" placeholder="unidades">
		  				</div>
		  			</div>
		  			<h5 class="text-center text-success"><b>% DE GANANCIAS</b></h5>
		  			<hr>
		  			<div class="row">
		  				<div class="col">
		  					<label>Unidad</label>
		  					<input type="number" step="any" name="ganancia_unidad" min="1" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por venta individual">
		  				</div>
		  				<div class="col">
		  					<label>Mayoreo</label>
		  					<input type="number" step="any" name="ganancia_mayoreo" min="1" max="100" class="form-control" placeholder="%"  title="Porcentaje de ganancia  por venta de unidades a mayoreo">
		  				</div>
		  				<div class="col">
		  					<label>Membrecía</label> 
		  					<input type="number" step="any" name="ganancia_membrecia" min="1" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por ventas a clientes con membrecía">
		  				</div>
		  			</div>
		  			
		  			<h5 class="text-center text-primary"><b>PRECIOS</b></h5>
		  			<div class="row">
		  				<div class="col">
		  					<label>Unidad</label>
		  					<input type="number" step="any" name="pre_unidad" class="form-control" placeholder="$" title="Precio de una unidad">
		  				</div>
		  				<div class="col">
		  					<label>Mayoreo</label>
		  					<input type="number" step="any" name="pre_mayoreo" class="form-control" placeholder="$" readonly title="Precio de unidades a mayoreo">
		  				</div>
		  				<div class="col">
		  					<label>Membrecía</label>
		  					<input type="number" step="any" name="pre_membrecia" class="form-control" placeholder="$" readonly title="Precio a clientes con membrecia">
		  				</div>
		  			</div>
		  			
		  		</div>
		  		<div class="col-2">
		  			<label>Provedor</label>
		  			<select name="proveedor" class="form-control">
		  				<option selected="" disabled="">Seleccione uno</option>
		  				<option value="0">Ninguno</option>
		  				@foreach($provedores as $r)
		  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
		  				@endforeach
		  			</select>

		  			<label>Categoria</label>
		  			<select name="categoria" class="form-control">
		  				<option selected="" disabled="">Seleccione uno</option>
		  				<option value="0">Ninguno</option>
		  				@foreach($categorias as $r)
		  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
		  				@endforeach
		  			</select>

		  			<label>Linea</label>
		  			<select name="linea" class="form-control">
		  				<option selected="" disabled="">Seleccione uno</option>
		  				<option value="0">Ninguno</option>
		  				@foreach($lineas as $r)
		  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
		  				@endforeach
		  			</select>

		  			<label>Marca</label>
		  			<select name="marca" class="form-control">
		  				<option selected="" disabled="">Seleccione uno</option>
		  				<option value="0">Ninguno</option>
		  				@foreach($marcas as $r)
		  					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
		  				@endforeach
		  			</select>
		  		</div>
		  		<div class="col-3">
		  			<label for="ubicacion_producto">Ubicación del producto</label>
					<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%">
					<select class="col-4" id="pasillo">
						<option>Tienda</option>
						<option>Bodega</option>
						<option>Sucursal</option>
						<option>Pasillo 1</option>
						<option>Pasillo 2</option>
					</select>
					<select class="col-4" id="anden">
						<option>CAMARA</option>
						<option>Anden 1</option>
						<option>Anden 2</option>
						<option>Anden 3</option>
						<option>Anden 4</option>
					</select>
					<select class="col-3" id="vitrina">
						<option> 0</option>
						<option>Vitrina 1</option>
						<option>Vitrina 2</option>
						<option>Vitrina 3</option>
						<option>Vitrina 4</option>
					</select>
					<br>


					<label>Palabras clave</label>
					<input type="text" name="palabra_clave" class="form-control">

					<label>Caducidad</label>
					<input type="date" name="caducidad" class="form-control">

					<!-- <label>URL</label>
					<input type="text" name="url" class="form-control">
		 -->
					<label>Foto</label>
					<input type="text" name="fotos" id="fotos" class="form-control" readonly>

					<label>Descripción para catálogo</label>
					<textarea name="descripcion_catalogo" class="form-control"></textarea>
		  		</div>
			</div>
			<br>
		  <button type="submit" class="btn btn-primary float-right"><i class="fas fa-save"></i> Guardar</button>
	  </form>

<!-- ########################################form subir imagenes###################################################### -->
	<br><br>
	<form enctype="multipart/form-data" id="formuploadajax" method="post">
		@csrf
		<label for="imagen"><b>Subir imagen:</b></label>
	  	<input type="file" name="imagen" id="imagen"/>
	  	<input type="hidden" name="nombre" id="nombre_articulo">
	  	<button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
    </form>
</div>

	<style type="text/css">
		label{margin-bottom:0px;}
	</style>
@endsection
@section('script')
<script type="text/javascript">

//################################calculando precio modo simple################

$("#form_simple [name=ganancia_unidad]").keyup(function(){
	$("#form_simple [name=pre_unidad]").val(calcular_precio_simple($(this).val()));	
});
$("#form_simple [name=ganancia_mayoreo]").keyup(function(){
	$("#form_simple [name=pre_mayoreo]").val(calcular_precio_simple($(this).val()));	
});
$("#form_simple [name=ganancia_membrecia]").keyup(function(){
	$("#form_simple [name=pre_membrecia]").val(calcular_precio_simple($(this).val()));	
});



function calcular_precio_simple(porcentaje){
	var compra=$("#form_simple [name=pre_compra]").val();
	compra=parseFloat(compra);
	porcentaje=parseFloat(porcentaje);
	porcentaje=porcentaje/100;
	ganancia=compra*porcentaje;
	var precio=compra+ganancia;
	precio=precio.toFixed(2);
	return precio;
}


//##########################modo simple y modo completo########################	
	$("#modo_completo").click(function(){
		$("#form_completo").show();
		$("#form_simple").hide();
		$(this).removeClass();
		$(this).addClass("btn btn-success");
		$("#modo_simple").removeClass();
		$("#modo_simple").addClass("btn btn-secondary");
		$("#form_completo").show();
	});
	$("#form_simple").hide();
	$("#modo_simple").click(function(){
		$("#form_simple").show();
		$("#form_completo").hide();
		$(this).removeClass();
		$(this).addClass("btn btn-success");
		$("#modo_completo").removeClass();
		$("#modo_completo").addClass("btn btn-secondary");
		$("#form_completo").hide();//ocultando form_completo
	});


	
//###############################para calcular precios modo completo##############

	$("#form_completo [name=ganancia_unidad]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("#form_completo [name=pre_unidad]").val(precio.toFixed(2));
	});

	$("#form_completo [name=ganancia_caja]").keyup(function(){
		var precio_caja=$("[name=precio_caja]").val();
		var ganancia=$(this).val();
		precio_caja=parseFloat(precio_caja);
		ganancia=parseFloat(ganancia);

		var pre=precio_caja * (ganancia/100);
		var precio=precio_caja+pre;
		precio=precio.toFixed(2);
		$("#form_completo [name=pre_caja]").val(precio);
		//----------------------------------
		if($("#form_completo [name=num_paquete]").val()!=''){
			var paquetes=$("#form_completo [name=num_paquete]").val();
			paquetes=parseFloat(paquetes);
			var precio_paquete=precio/paquetes;
			precio_paquete=precio_paquete.toFixed(2);
			$("#form_completo [name=pre_paquete]").val(precio_paquete);
		}

	});
	$("#form_completo [name=ganancia_mayoreo]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("#form_completo [name=pre_mayoreo]").val(precio.toFixed(2));
	});
	$(" #form_completo [name=ganancia_membrecia]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("#form_completo [name=pre_membrecia]").val(precio.toFixed(2));
	});

	$("#form_completo [name=precio_caja]").keyup(function(){
		$("#form_completo [name=pre_compra]").val(calcular_pre_compra());
	});
	$("#form_completo [name=pza_x_caja]").keyup(function(){
		$("#form_completo [name=pre_compra]").val(calcular_pre_compra());
	});

	function calcular_pre_compra(){
		var pre_caja=$("#form_completo [name=precio_caja]").val();
		var unidades=$("#form_completo [name=pza_x_caja]").val();
		if(pre_caja!=null && unidades!=null){
			var precio_compra=parseFloat(pre_caja)/parseFloat(unidades);
		precio_compra=precio_compra.toFixed(2);
		}else{
			precio_compra=0;
		}
	return precio_compra;
	}


	function calcular_precio(ganancia){
		var precio_caja=$("#form_completo [name=precio_caja]").val();
		var unidades=$("#form_completo [name=pza_x_caja]").val();
		var pre=parseFloat(precio_caja) / parseFloat(unidades);
		var sum_ganancia=pre*(parseFloat(ganancia)/100);
		var precio=pre+sum_ganancia;

		return precio;
	}

//----------------------------------------------------------------------------------------



var cont=0;
var imagen="";
//-------------------------al seleccionar ubicacion del articulo---------------------------
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});


	//---------------------modo simple-----------------------------------------------
	$("#form_simple #pasillo,#form_simple #anden,#form_simple #vitrina").on('change',function(){
		var ubicacion=$("#form_simple #pasillo").val()+","+$("#form_simple #anden").val()+","+$("#form_simple #vitrina").val();
		$("#form_simple #ubicacion_producto").val(ubicacion);
	});

//------------------------SUBIR IMAGEN----------------------------
	$("#formuploadajax").on("submit", function(e){
  e.preventDefault();
  var f = $(this);
  var formData = new FormData(document.getElementById("formuploadajax"));
  formData.append("dato", "valor");
  //formData.append(f.attr("name"), $(this)[0].files[0]);
  $.ajax({
      url: "{{url('/uploadImg')}}",
      type: "post",
      dataType: "",
      data: formData,
      cache: false,
      contentType: false,
	 			processData: false
	      }).done(function(e){
		// alert(e.nombre);
		if (e.status=="success") {
			if (cont==0) {
				imagen=e.nombre;
			}else{
				imagen=imagen+","+e.nombre;
			}
			cont++;
			$("#fotos").val(imagen);
			$("#form_simple #fotos").val(imagen);

			$("#subirImg").removeClass("btn btn-primary");
			$("#subirImg").addClass("btn btn-success");
			$("#subirImg").text("Subido");
		}else{
			$("#subirImg").removeClass("btn btn-primary");
			$("#subirImg").addClass("btn btn-danger");
			$("#subirImg").text("ERROR");
		}
	}).fail(function (jqXHR, exception) {
    // Our error logic here
    console.log(exception);
    });
});



//###############validando el formulario para enviarlo solo una ves###########################
    enviando = false; //Obligaremos a entrar el if en el primer submit
    function checkSubmit() {
        if (!enviando) {
            enviando= true;
            return true;
        } else {
            //Si llega hasta aca significa que pulsaron 2 veces el boton submit
            alert("El formulario ya se esta enviando");
            return false;
        }
    }
//#######################################################################






</script>
@endsection