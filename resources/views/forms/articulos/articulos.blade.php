@extends('layouts.app')

	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">

@section('content')

<!-- ###############################card 1 muestra tabla de articulso########################### -->
	<div class="container-fluid" id="card1" style="margin-top:10px">


		@if(session('success'))
					<div class="alert alert-success alert-dismissible fade show">
						<h3>{{session('success')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if(session('error'))
					<div class="alert alert-danger alert-dismissible fade show">
						<h3>{{session('error')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif



		<div class="row">
				<div class="col-12 col-md-5 col-sm-5 col-lg-5">
					<h2 class="text-dark float-left">Articulos <i class="fas fa-box-open"></i></h2>
				</div>
				<div class="col-12 col-md-7 col-sm-7 col-lg-7 col-xl-7">
					<div  style="margin:auto" id="botones_superiores">
						<button style="margin-left: 25px" class=" btn-sm float-right btn btn-secondary" id="btn_a_card2"><i class="fas fa-arrow-right"></i></button>
						<button class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#modal_marcas"> <i class="fas fa-plus"></i>  Marca</button>
						<button class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#modal_lineas"> <i class="fa fa-tags"></i>  Linea</button>
						<button class="btn btn-sm btn-danger float-right" data-toggle="modal" data-target="#modal_categorias"> <i class="fas fa-plus"></i>  Categoria</button>
						<button class="btn btn-sm btn-warning float-right" data-toggle="modal" data-target="#modal_agregar_provedor"> <i class="fas fa-plus"></i>  Provedor</button>

						@if(count($provedores)!=0 )
						@if(Auth::User()->type=='superadmin')
						<button class="btn btn-sm btn-success float-right" id="btn_open_existencia"> <i class="fas fa-angle-double-up"></i> Existencia</button>
						@endif
						<a href="{{url('/registrar_articulo')}}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Articulos</a>
						<!-- <button class="btn btn-sm btn-primary float-right" id="btn_open_modal_producto"> <i class="fas fa-plus"></i> Articulos</button> -->
						@endif

					</div>

				</div>
			</div>

			@if(count($provedores)==0)
				<div class="alert alert-danger">Es necesario registrar un <strong>provedor</strong> para poder registrar un articulo</div>
			@endif

			<div id="alertas">
			</div>
			<div class="row">
				<div class="col-12 col-md-3 col-lg-3 col-xl-3"></div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3"></div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					@if(Request::path()!="articulos")
					<a href="{{url('/articulos')}}" class="btn btn-warning btn-sm float-right"><i class="fas fa-eye"></i> Todos</a>
					<br><br>
				@endif
				</div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<form  action="{{url('/buscar_articulos')}}" method="get">
						<div class="input-group">
							<input type="text" name="busqueda" class="form-control" placeholder="Buscar..." autofocus="true" @if(Request::path()!="articulos") value="" @endif>
							<button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-search"></i></button>
						</div>
					</form>	
				</div>
			</div>
			<br>

			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tabla_articulos">
					<thead class="bg-primary text-white">
					<tr class="text-center">
						<th>Clave</th>
						<th>Unidad</th>
						<th>Descripción</th>
						<th>Mayoreo</th>
						<th>Precios</th>
						<th>Existencia</th>
						<th><i class="fa fa-truck text-warning"></i></th>
		        <th><i class="fa fa-tags text-info"></i></th>
		        <th><i class="fa fa-map-marker text-white"></i></th>
		        <th><i class="fas fa-images text-white"></i></th>
						<th><i class="fas fa-barcode fa-2x"></i></th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
					<tbody>
						@foreach($articulos as $r)
						<tr>
							<td class="text-center">{{$r->clave}}</td>
							<td class="text-center">{{$r->unidad}}</td>
							<td><b class="text-primary">{{$r->descripcion_articulo}}</b></td>
							<td class="text-center"><span class="text-primary">>={{$r->mayoreo_apartir}}</span></td>
							<td width="10%">
								@if($r->precio_caja==0 || $r->precio_caja==null)
									<select class="form-control">
										<option disabled selected>PRECIOS</option>
										@if(Auth::User()->type=='superadmin')
											<option >Compra $ {{number_format($r->pre_compra,2)}}</option>
										@endif
										<option>Venta individual $ {{$r->pre_unidad}}</option>
										<option>Venta x mayoreo $ {{number_format($r->pre_mayoreo,2)}}</option>
										<option>venta x membrecia $ {{number_format($r->pre_membrecia,2)}}</option>
									</select>
								@else
								<select class="form-control">
									<option disabled selected>PRECIOS</option>
									@if(Auth::User()->type=='superadmin')
										<option>Compra X caja $ {{$r->precio_caja}}</option>
										<option >Compra individual $ {{number_format($r->pre_compra,2)}}</option>
									@endif
									<option>Venta individual $ {{$r->pre_unidad}}</option>
									<option>Venta x caja $ {{number_format($r->pre_caja,2)}}</option>
									<option>Venta x paquete $ {{number_format($r->pre_paquete,2)}}</option>
									<option>Venta x mayoreo $ {{number_format($r->pre_mayoreo,2)}}</option>
									<option>venta con membrecia $ {{number_format($r->pre_membrecia,2)}}</option>
								</select>
								@endif
							</td>
							
							<td class="text-right"><b class="text-danger">{{$r->existencia_stock}}</b> </td>
							<td class="text-center">{{$r->provedor}}</td>
							<td class="text-center">{{$r->marca}}</td>
							<td class="text-center" data-toggle="tooltip"   title="{{$r->ubicacion_producto}}">@if(isset($r->ubicacion_producto)) Ubicación @endif</td>
							<td class="td_imagen text-center" style="margin:auto">
								<span class="d-none">{{$r->fotos}}</span>	
								 @if($r->fotos!="ninguno" && $r->fotos!='' || $r->fotos!=null)
                    @php
                      $fotos=explode(",",$r->fotos);
                    @endphp
                    <img src="{{asset('/img/articulos/'.$fotos[0])}}" width="30px" class="card-img-to" alt="{{$r->descripcion_producto}}">
                    @else
                    <img src="{{asset('/img/articulos/no-images1.jpg')}}" width="30px" class="card-img-to">
                    @endif
							</td>
							<td class="text-center">{{$r->codigo_barra}}</td>
							<td>
									@if(Auth::user()->type=='superadmin')
										<a href="{{url('/editar_articulo/'.$r->id)}}" class="btn btn-info btn-sm " ><i class="fas fa-edit "></i></a>
									@endif
								<button class="btn btn-danger btn-sm " onclick="eliminar({{$r->id}});" @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
									<i class="fas fa-trash fa-sm"></i>
								</button>

							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="float-right">
				@if(Request::path()=="articulos")
						{{$articulos->links()}}
				@endif
				</div>
			</div>
			<style type="text/css">
				#botones_superiores button{
					margin-right: 5px;
				}
					.table th{
						padding:0px !important;
						height:25px !important;
					}
					.table td{
						height: 25px !important;
						padding: 3px !important;
					}
					#tabla_articulos_filter{ text-align: right; }
					#tabla_marcas_filter{text-align: right; }
					#tabla_provedores_filter{text-align: right; }
					#tabla_articulos tbody tr:hover{
						background: #D6EAF8 !important;
					}
			</style>
	</div>
<!-- #############CARD 2 LISTAS DE MARCAS Y PROVEDORES########################################## -->
	<div class="container-fluid" id="card2">
			<div class="row">
				<div class="col">
					<h2 class="text-white float-left">Tablas <i class="fas fa-table"></i></h2>
				</div>
				<div class="col">
					<button class="float-right btn btn-secondary" id="btn_a_card1"><i class="fas fa-arrow-left"></i></button>
				</div>
			</div>

			<div id="alertas_card2">
			</div>
			<div class="row">
				<div class="col-12 col-lg-12 col-md-12 col-xl-12">
					<h3 class="text-center text-primary">Tabla provedores</h3>
					<div class="table-responsive">
						<table class="table table-striped table-bordered" id="tabla_provedores">
							<thead class="bg-warning ">
								<tr>
									<th>#</th>
									<th>nombre</th>
									<th>descripción</th>
									<th>Domicicio</th>
									<th>Teléfono</th>
									<th>E-mail</th>
									<th>RFC</th>
									<th><i class="fas fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								@foreach($provedores as $provedor)
									<tr>
										<td>{{$provedor->id}}</td>
										<td>{{$nombre_pro=$provedor->nombre}}</td>
										<td>{{$descri_pro=$provedor->descripcion}}</td>
										<td>{{$domicilio_pro=$provedor->domicilio}}</td>
										<td>{{$tel_pro=$provedor->tel}}</td>
										<td>{{$email_pro=$provedor->email}}</td>
										<td>{{$rfc_pro=$provedor->rfc}}</td>
										<td style="padding: 0px;width:50px;padding: 0 1px !important;">
											<div class="input-group">
											<button class="btn btn-sm btn-success" style="margin-right: 20px" onclick="editar_provedor({{$provedor->id}},'{{$nombre_pro}}','{{$descri_pro}}','{{$domicilio_pro}}','{{$tel_pro}}','{{$email_pro}}','{{$rfc_pro}}' );"  @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
												<i class="fas fa-edit"></i>
											</button>

											<button class="btn btn-sm btn-danger" onclick="eliminar_provedor({{$provedor->id}});"  @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
												<i class="fas fa-trash"></i>
											</button>
										</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
<!-- ############################################################################# -->
<!-- ############################################################################# -->









<!--window modal ######modal busqueda par a existencias################-->
  <div class="modal fullscreen-modal fade" id="modal_existencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Existencia</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="alertasModal"></div>

        	<div class="input-group col-4">
        		<input type="text" id="input_buscar" class="form-control" placeholder="Buscar" autocomplete="off">
        		<button  id="btn_buscar_para_existencia" class="btn  btn-primary"> <i class="fas fa-search"></i> </button>
        	</div>
        	<br>
        	<div class="table-responsive" >
        		<table class="table table-striped table-bordered">
        			<thead class="table-success">
        				<tr>
        					<th>Entradas</th>
        					<th>Existencia</th>
        					<th>Clave</th>
        					<th >Descripción</th>
        					<th>Cod. barra</th>
        				</tr>
        			</thead>
        			<tbody id="tbody_existencia"></tbody>
        		</table>
        	</div>

        	<button class="btn btn-primary float-right" id="guardar_cambios"><i class="fas fa-save"></i> Guardar cambios</button>

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal imagenes################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;padding: 5px !important;" >

      		<b> IMAGENES <i class="app-menu__icon  fa fa-picture-o"></i></b>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body" style="text-align: center">
        	<div class="doc">
        		<button class="" id="btn_galeria_left" disabled="true"><i class="fas fa-angle-left"></i></button>
        		<button class="" id="btn_galeria_right"><i class="fas fa-angle-right"></i></button>
					  <div class="box">
					    <img id="img_principal">
					  </div>
					</div>
					<div class="text-center" id="div_point"></div>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal provedor################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_provedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Provedor</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_provedor">
        		<div class="row">
        			<div class="col">
        				<input type="hidden" name="id_provedor">
        				<label>Nombre</label>
			        	<input type="text" name="nombre" class="form-control" placeholder="Nombre">

			        	<label>Descripción</label>
			        	<input type="text" name="descripcion" class="form-control" placeholder="Descripción">

			        	<label>RFC</label>
			        	<input type="text" name="rfc" class="form-control">
        			</div>
        			<div class="col">
        				<label>Domicilio</label>
			        	<input type="text" name="domicilio" class="form-control">

			        	<label>Telefono</label>
			        	<input type="number" name="tel" class="form-control">

			        	<label>Email</label>
			        	<input type="email" name="email" class="form-control">
        			</div>
        		</div>

	        	<br><br>
	        	<button type="button" id="btn_store_provedor" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
	        	<button type="button" id="btn_update_provedor" style="display: none" class="btn btn-secondary float-right"><i class="fas fa-save"></i> Actualizar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal categoria################-->
  <div class="modal fullscreen-modal fade" id="modal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #dc3545;" >
      		<h1><i class="fa fa-plus"></i> Categoria <i class="app-menu__icon  fa fa-code-fork"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeCategoria')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal linea################-->
  <div class="modal fullscreen-modal fade" id="modal_lineas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #17a2b8;" >
      		<h1><i class="fa fa-plus"></i> Linea <i class="app-menu__icon  fa        fa-tags"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeLinea')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light bg-primary" >
      		<h1><i class="fa fa-plus"></i> Marca <i class="app-menu__icon  fab fa-google-wallet"></i> </h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeMarca')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>

        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>


<style type="text/css">

	.doc{
		  display: flex;
		  flex-flow: column wrap;
		  width: 100%;
		  height: 50vh;
		  justify-content: center;
		  align-items: center;
		  /*background: #333944;*/
		}
		.box{
		  width: 100%;
		  height: 400px;
		  /*background: #CCC;*/
		  overflow: hidden;
		}


		.box img{
		  width: auto;
		  max-width: 100%;
		  height: auto;
		  max-height: 49vh;
		}
		@supports(object-fit: cover){
		    .box img{
		      height: 100%;
		      object-fit: cover;
		      object-position: center center;
		    }
		}


	#btn_galeria_left{
		position: absolute;
		left: 2px;
		top: 40%;
		border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
	}
	#btn_galeria_right{
		position: absolute;
		right: 2px;
		top: 40%;
		border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
	}
</style>



@endsection

@section('script')

<script type="text/javascript">

$('[data-toggle="tooltip"]').tooltip();





//#################################funciones para galeria de imagenes#########################
	var array_imgs="";
	var cont_img=0;

	$("#btn_galeria_left").click(function(){
			click_button_left($(this));
	});

	$("#btn_galeria_right").click(function(){
			click_button_right($(this));
	});

	function click_button_left(btn){
		if(cont_img-1==0){
			btn.attr("disabled","true");
		}
		$("#btn_galeria_right").removeAttr("disabled");
			cont_img--;
			$("#img_principal").removeAttr("src");
			$("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
			
			$("#div_point i").removeClass("text-primary");
			$("#div_point i").addClass("text-secondary");

			$("#div_point i").each(function(i){
				if(i==cont_img){
					$(this).removeClass("text-secondary");
					$(this).addClass("text-primary");
				}
			});
	}
	function click_button_right(btn){
		if(cont_img+2==array_imgs.length){
				btn.attr("disabled","true");
			}
			$("#btn_galeria_left").removeAttr("disabled");
			cont_img++;
			$("#img_principal").removeAttr("src");

			$("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
			$("#img_principal").fadeIn('slow','swing');

			$("#div_point i").removeClass("text-primary");
			$("#div_point i").addClass("text-secondary");

			$("#div_point i").each(function(i){
				if(i==cont_img){
					$(this).removeClass("text-secondary");
					$(this).addClass("text-primary");
				}
		});
	}


	//####################Modal ver foto en grande############
		//_______________________________click a  la imagen____________________
		$("table tbody .td_imagen").click(function(){
			cont_img=0;
			 var imagenes=$(this).find("span").html();
			 $("#div_point").html('');
			 array_imgs="";
			 if (imagenes=='' || imagenes=='ninguno') {
			 	$("#btn_galeria_right,#btn_galeria_left").attr("disabled","true");
			 	$("#img_principal").removeAttr("src");
			 	$("#img_principal").attr("src","{{asset('')}}"+"img/articulos/no-images.jpg");
			 }
			else if(imagenes!='' || imagenes!='ninguno'){

				$("#btn_galeria_right").removeAttr("disabled");
			 	imagenes=imagenes.split(",");
			 	array_imgs=imagenes;
			 	$("#img_principal").removeAttr("src");
			 	$("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+imagenes[0]);
			 	
			 	for(var a=0;a<imagenes.length;a++){
			 		if(a==0){//agregando puntos el primero coloreado
						$("#div_point").append("<i class='fas fa-dot-circle text-primary' style='font-size:10px;margin:3px'></i>");
			 		}else{
			 			$("#div_point").append("<i class='fas fa-dot-circle text-secondary' style='font-size:10px;margin:3px'></i>");
			 		}
			 	}
			}

			if(imagenes.length==1){
				$("#btn_galeria_right,#btn_galeria_left").attr("disabled","true");
			}else if(imagenes.length>1){
				$("#btn_galeria_right").removeAttr("disabled");
				$("#btn_galeria_left").attr("disabled","true");
			}

			$("#modal_imagenes").modal('show');

		});




//######################script para el card 2######################
	$("#btn_update_provedor").click(function(){
		$.ajax({
			url:"{{url('/update_provedor')}}",
			type:"post",
			dataType:"json",
			data:$("#form_provedor").serialize(),
			success:function(e){
				if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Modificado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			},error:function(){
				$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
			}
		});
	});

	function editar_provedor(id,nombre,descripcion,domicilio,tel,email,rfc){
		$("#modal_agregar_provedor").modal("show");
		$("#form_provedor")[0].reset();
		$("#btn_update_provedor").show();
		$("#btn_store_provedor").hide();
		$("#form_provedor [name=id_provedor]").val(id);
		$("#form_provedor [name=nombre]").val(nombre);
		$("#form_provedor [name=descripcion]").val(descripcion);
		$("#form_provedor [name=rfc]").val(rfc);
		$("#form_provedor [name=domicilio]").val(domicilio);
		$("#form_provedor [name=tel]").val(tel);
		$("#form_provedor [name=email]").val(email);
	}
	function eliminar_provedor(id){
		msj=confirm("Desea eliminar este registro?, tenga en cuenta que hay articulos relacionada a este provedor lo cual no se mostrarán la información correcta si se elimina este registro.");
		if (msj) {
			$.ajax({
				url:"{{url('/delete_provedor')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Eliminado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}

				},error:function(){
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			});
		}
	}


	$("#card2").hide();
	$("#btn_a_card1").click(function(){
		$("#card2").hide();
		$("#card1").show();
	});
	$("#btn_a_card2").click(function(){
		$("#card1").hide();
		$("#card2").show();
	});

	// $("#tabla_articulos").DataTable({
	// 		"order": [[ 0, 'DESC' ]],
	// 		"language": {
	//         "decimal": "",
	//         "emptyTable": "No hay información",
	//         "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	//         "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	//         "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	//         "infoPostFix": "",
	//         "thousands": ",",
	//         "lengthMenu": "Mostrar _MENU_",
	//         "loadingRecords": "Cargando...",
	//         "processing": "Procesando...",
	//         "search": "Buscar:",
	//         "zeroRecords": "Sin resultados encontrados",
	//         "paginate": {
	//             "first": "Primero",
	//             "last": "Ultimo",
	//             "next": "Siguiente",
	//             "previous": "Anterior"
	//         }}});





	$("#tabla_provedores").DataTable({
			"order": [[ 0, 'DESC' ]],
			"language": {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Entradas",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }}});

//####################################################


	//######################eliminar articulo#################
		function eliminar(id){
			var msj=confirm("Desea eliminar este articulo?");
			if (msj) {
				$.ajax({
					url:"{{route('eliminarArticulo')}}",
					type:"post",
					data:{
						id:id
					},success:function(){
						$("#alertas").append("<div class='alert alert-warning'>Eliminado corectamente</div>");
						setInterval(function(){
							location.reload();
						},2000);
					},error:function(){
						$("#alertas").append("<div class='alert alert-danger'>Error al eliminar este articulo.</div>");
						setInterval(function(){
							$("#alertas").html('');
						},2000);
					}
				});
			}
		}




	//##########################existencia###########
		$("#btn_open_existencia").click(function(){
			$("#modal_existencia").modal("show");
		});
		$("#input_buscar").on("keyup",function(e){
			if (e.keyCode==13) {
				buscarproductos();
				$(this).val('');
			}
		});
		$("#guardar_cambios").hide();

		function buscarproductos(){
			$("#tbody_existencia").html('');
			$.ajax({
				url:"{{route('buscarExistencia')}}",
				type:"post",
				dataType:"json",
				data:{
					buscar:$("#input_buscar").val()
				},success:function(e){
					for(var x=0;x<e.length;x++){
						$("#tbody_existencia").append("<tr> <td style='width:20%'> <input type='number' class='form-control' style='width: 100%'></td><td>"+e[x].existencia+"</td><td>"+e[x].clave+" </td><td>"+e[x].descripcion_articulo+" </td><td class='d-none'>"+e[x].id+"</td> <td>"+e[x].codigo_barra+"</td></tr>");
					}
					$("#guardar_cambios").show();
				}
			});
		}
		$("#btn_buscar_para_existencia").click(function(){
			buscarproductos();
			$("#input_buscar").val('');
		});
		//---------------------------------------------
		$("#guardar_cambios").click(function(){
			var id=[]; var cantidad=[];
			$("#tbody_existencia").find("tr td:first-child").each(function(){
				// alert($(this).find('input').val());
				if(parseFloat($(this).find('input').val())){
					cantidad.push($(this).find('input').val());
					id.push($(this).siblings("td").eq(3).html());
				}
          });//fin each
			$.ajax({
				url:"{{route('updateExistenca')}}",
				type:"post",
				dataType:"json",
				data:{
					id:id,
					cantidad:cantidad
				},success:function(e){
					$("#tbody_existencia").html('');
					if (e=="success") {
						$("#alertasModal").append("<div class='alert alert-success'>Modificado correctamente.</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);
					}
				},error:function(){

						$("#alertasModal").append("<div class='alert alert-danger'>Error al modificar, verifique la información proporcionada...</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);

				}
			});
		});

	//##################submit provedor###############
		$("#btn_open_modal_provedor").click(function(){
			$("#modal_agregar_provedor").modal('show');
			$("#form_provedor")[0].reset();
			$("#btn_update_provedor").hide();
			$("#btn_store_provedor").show();
		});

		$("#btn_store_provedor").click(function(){
			$.ajax({
				url:"{{route('store_provedor')}}",
				type:"post",
				dataType:"json",
				data:$("#form_provedor").serialize(),
				success:function(e){
					$("#form_provedor")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Provedor agregado correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				}
			});
		});






    //################submit marca#################
		$("#btn_open_modal_marca").click(function(){
			$("#form_marca")[0].reset();
			$("#modal_agregar_marca").modal('show');
			$("#btn_store_marca").show();
			$("#btn_update_marca").hide();

		});



</script>
@endsection
