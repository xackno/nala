@extends('layouts.app')
@section('content')
<div class="container-fluid">
	
@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	<br>
  <form  action="{{url('/update_articulo/'.$articulo->id)}}" method="post" onsubmit="return checkSubmit();">
  	@csrf
		<div class="row">
	  		<div class="col-3">
	  			<label>Nombre o descripción</label>
	  			<input type="text" name="descripcion_articulo" class="form-control " placeholder="descripción" autofocus value="{{$articulo->descripcion_articulo}}">

	  			<div class="row">
	  				<div class="col">
	  					<div class="form-group">
	  						<label>Clave</label>
	  						<input type="text" name="clave" class="form-control" placeholder="Clave"  value="{{$articulo->clave}}">
	  					</div>
	  				</div>
	  				<div class="col">
	  					<label>Unidad</label>
	      			<select class="form-control" name="unidad" >
	      				<option  disabled="">Seleccione uno</option>
	      				<option value="pieza" @if($articulo->unidad=='pieza') selected="true" @endif>Pieza</option>
	      				<option value="kilogramo" @if($articulo->unidad=='kilogramo') selected="true" @endif>Kilogramo</option>
	      				<option value="litro" @if($articulo->unidad=='litro') selected="true" @endif>Litro</option>
	      				<option value="gramos" @if($articulo->unidad=='gramos') selected="true" @endif>Gramos</option>
	      				<option value="bolsa" @if($articulo->unidad=='bolsa') selected="true" @endif>Bolsa</option>
	      				<option value="caja" @if($articulo->unidad=='caja') selected="true" @endif>Caja</option>
	      				<option value="bulto" @if($articulo->unidad=='bulto') selected="true" @endif>Bulto</option>
	      				<option value="paquete" @if($articulo->unidad=='paquete') selected="true" @endif>Paquete</option>
	      			</select>
	  				</div>
	  			</div>
	  			<hr margin="3px">
	  			<div class="row">
	  				<label class="text-center" style="margin-left: 33%;">STOCK PIEZAS	</label>
	  			</div>
	  			
	  			<div class="row">
	  				<div class="col">
	  					<label>Inv. min.</label>
	  					<input type="number" name="inv_min" class="form-control" value="{{$articulo->inv_min}}" >
	  				</div>
	  				<div class="col">
	  					<label>Inv. max.</label>
	  					<input type="number" name="inv_max" class="form-control" value="{{$articulo->inv_max}}">
	  				</div>
	  				<div class="col">
	  					<label>Exist. bodega</label>
	  					<input type="number" step="any" name="existencia_bodega" class="form-control" value="{{$articulo->existencia_bodega}}">
	  				</div>
	  			</div>
	  			
	  			<div class="row">
	  				<div class="col"><label>Existencia stock</label></div>
	  				<div class="col text-danger"><label>Inv. compra</label></div>
	  			</div>
	  			<div class="row">
	  				<div class="col">
	  					<input type="number" step="any" name="existencia_stock" class="form-control" value="{{$articulo->existencia_stock}}">
	  				</div>
	  				<div class="col">
	  					<input type="number" step="any" name="inv_compra" class="form-control" value="{{$articulo->inv_compra}}">
	  				</div>
	  			</div>
	  			<hr>

	  			<div class="row">
	  				<div class="col">
	  					<label>Código de barra</label>
	  					<input type="number" name="codigo_barra" class="form-control" value="{{$articulo->codigo_barra}}" >
	  				</div>
	  				<div class="col">
	  					<label>Código SAT</label>
	  					<input type="number" name="codigo_sat" class="form-control" value="{{$articulo->codigo_sat}}">
	  				</div>
	  			</div>
					

	  		</div>
	  		<div class="col-5">
	  			<h5 class="text-center"><b>CAJA</b></h5>
	  			<hr>
	  			<div class="row">
	  				<div class="col">
	  					<label>Precio por caja</label>
	  					<input type="number" step="any" name="precio_caja" class="form-control " value="{{$articulo->precio_caja}}">
	  				</div>
	  				<div class="col">
	  					<label>No. Paquete/bolsas</label>
	  					<input type="number" step="any" name="num_paquete" class="form-control" value="{{$articulo->num_paquete}}" >
	  				</div>
	  				<div class="col">
	  					<label>unidades</label>
	  					<input type="number" step="any" name="pza_x_caja" class="form-control" value="{{$articulo->pza_x_caja}}">
	  				</div>
	  			</div>
	  			<label>Mayoreo a partir de:</label>
	  			<input type="number"  step="any" name="mayoreo_apartir" class="form-control col-3" title="Cantidad de unidades que se consideran como venta a mayoreo " value="{{$articulo->mayoreo_apartir}}">
	  			
	  			<h5 class="text-center text-success"><b>% DE GANANCIAS</b></h5>
	  			<hr>
	  			<div class="row">
	  				<div class="col">
	  					<label>Unidad</label>
	  					<input type="number" step="any" name="ganancia_unidad" min="0" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por venta individual" value="{{$articulo->ganancia_unidad}}">
	  				</div>
	  				<div class="col">
	  					<label>Caja</label>
	  					<input type="number" step="any" name="ganancia_caja" min="0" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por ventas por caja" value="{{$articulo->ganancia_caja}}">
	  				</div>
	  				<div class="col">
	  					<label>Mayoreo</label>
	  					<input type="number" step="any" name="ganancia_mayoreo" min="0" max="100" class="form-control" placeholder="%"  title="Porcentaje de ganancia  por venta de unidades a mayoreo" value="{{$articulo->ganancia_mayoreo}}">
	  				</div>
	  				<div class="col">
	  					<label>Membrecía</label> 
	  					<input type="number" step="any" name="ganancia_membrecia" min="0" max="100" class="form-control" placeholder="%" title="Porcentaje de ganancia por ventas a clientes con membrecía" value="{{$articulo->ganancia_membrecia}}">
	  				</div>
	  			</div>
	  			
	  			<h5 class="text-center text-primary"><b>PRECIOS</b></h5>
	  			<div class="row">
	  				<div class="col d-none">
	  					<label>Compra</label>
	  					<input type="number" step="any" name="pre_compra" class="form-control" placeholder="$" title="Precio a la que se adquiere el articulo al provedor." value="{{$articulo->pre_compra}}">
	  				</div>
	  				<div class="col">
	  					<label>Unidad</label>
	  					<input type="number" step="any" name="pre_unidad" class="form-control" placeholder="$" title="Precio de una unidad" value="{{$articulo->pre_unidad}}">
	  				</div>
	  				<div class="col">
	  					<label>Caja</label>
	  					<input type="number" step="any" name="pre_caja" class="form-control" placeholder="$" readonly title="precio de una caja" value="{{$articulo->pre_caja}}">
	  				</div>
	  				<div class="col">
	  					<label>Paquete</label>
	  					<input type="number" step="any" name="pre_paquete" class="form-control" placeholder="$" title="Precio de paquete o bolsa" readonly value="{{$articulo->pre_paquete}}">
	  				</div>
	  				<div class="col">
	  					<label>Mayoreo</label>
	  					<input type="number" step="any" name="pre_mayoreo" class="form-control" placeholder="$" readonly title="Precio de unidades a mayoreo" value="{{$articulo->pre_mayoreo}}">
	  				</div>
	  				<div class="col">
	  					<label>Membrecía</label>
	  					<input type="number" step="any" name="pre_membrecia" class="form-control" placeholder="$" readonly title="Precio a clientes con membrecia" value="{{$articulo->pre_membrecia}}">
	  				</div>
	  			</div>
	  			
	  		</div>
	  		<div class="col-1">
	  			<label>Provedor</label>
	  			<select name="proveedor" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($provedores as $r)
	  					<option value="{{$r->id}}" @if($r->id==$articulo->proveedor) selected="true" @endif data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Categoria</label>
	  			<select name="categoria" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($categorias as $r)
	  					<option value="{{$r->id}}"  @if($r->id==$articulo->categoria) selected="true" @endif data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Linea</label>
	  			<select name="linea" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($lineas as $r)
	  					<option value="{{$r->id}}" @if($r->id==$articulo->linea) selected="true" @endif   data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>

	  			<label>Marca</label>
	  			<select name="marca" class="form-control">
	  				<option selected="" disabled="">Seleccione uno</option>
	  				<option value="0">Ninguno</option>
	  				@foreach($marcas as $r)
	  					<option value="{{$r->id}}" @if($r->id==$articulo->marca) selected="true" @endif  data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
	  				@endforeach
	  			</select>
	  		</div>
	  		<div class="col-3">
	  			<label for="ubicacion_producto">Ubicación del producto</label>
				<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%" value="{{$articulo->ubicacion_producto}}">
				<select class="col-4" id="pasillo">
					<option>Tienda</option>
					<option>Bodega</option>
					<option>Sucursal</option>
					<option>Pasillo 1</option>
					<option>Pasillo 2</option>
				</select>
				<select class="col-4" id="anden">
					<option>CAMARA</option>
					<option>Anden 1</option>
					<option>Anden 2</option>
					<option>Anden 3</option>
					<option>Anden 4</option>
				</select>
				<select class="col-3" id="vitrina">
					<option> 0</option>
					<option>Vitrina 1</option>
					<option>Vitrina 2</option>
					<option>Vitrina 3</option>
					<option>Vitrina 4</option>
				</select>
				<br>


				<label>Palabras clave</label>
				<input type="text" name="palabra_clave" class="form-control" value="{{$articulo->palabra_clave}}">

				<label>Caducidad</label>
				<input type="date" name="caducidad" class="form-control" value="{{$articulo->caducidad}}">

				<!-- <label>URL</label>
				<input type="text" name="url" class="form-control">
	 -->
				<label>Foto</label>
				<input type="text" name="fotos" id="fotos" class="form-control" readonly value="{{$articulo->fotos}}">

				<label>Descripción para catálogo</label>
				<textarea name="descripcion_catalogo" class="form-control" >{{$articulo->descripcion_catalogo}}</textarea>
	  		</div>
		</div>

		<br>
		<div class="row">
			<div class="input-group">
	  			<label>A granel</label>
	  			<input type="checkbox" name="a_granel" style="margin-right: 15px">

	  			<label for="en_catalogo">En catálogo</label>
	  			<input type="checkbox" name="en_catalogo" style="margin-right: 15px">

	  			<label for="ventas_negativas">Venta en -0</label>
	  			<input type="checkbox" name="ventas_negativas" >
			</div>
		</div>
		<br>



	  <button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar modificaciones</button>
  </form>

  <br><br>
	<form enctype="multipart/form-data" id="formuploadajax" method="post">
		@csrf
		<label for="imagen"><b>Subir imagen:</b></label>
	  	<input type="file" name="imagen" id="imagen"/>
	  	<input type="hidden" name="nombre" id="nombre_articulo">
	  	<button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
    </form>
</div>

<style type="text/css">
	label{margin-bottom:0px;}
</style>
@endsection
@section('script')
<script type="text/javascript">
	


@if($articulo->a_granel=='on')
	$('[name=a_granel]').prop('checked', true);
@else
	$('[name=a_granel]').prop('checked', false);
@endif
@if($articulo->en_catalogo=='on')
	$('[name=en_catalogo]').prop('checked', true);
@else
	$('[name=en_catalogo]').prop('checked', false);
@endif
@if($articulo->ventas_negativas=='on')
	$('[name=ventas_negativas]').prop('checked', true);
@else
	$('[name=ventas_negativas]').prop('checked', false);
@endif

//###############################para calcular precios################################

	$("[name=ganancia_unidad]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("[name=pre_unidad]").val(precio.toFixed(2));
	});

	$("[name=ganancia_caja]").keyup(function(){
		var precio_caja=$("[name=precio_caja]").val();
		var ganancia=$(this).val();
		precio_caja=parseFloat(precio_caja);
		ganancia=parseFloat(ganancia);

		var pre=precio_caja * (ganancia/100);
		var precio=precio_caja+pre;
		precio=precio.toFixed(2);
		$("[name=pre_caja]").val(precio);
		//----------------------------------
		if($("[name=num_paquete]").val()!=''){
			var paquetes=$("[name=num_paquete]").val();
			paquetes=parseFloat(paquetes);
			var precio_paquete=precio/paquetes;
			precio_paquete=precio_paquete.toFixed(2);
			$("[name=pre_paquete]").val(precio_paquete);
		}

	});
	$("[name=ganancia_mayoreo]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("[name=pre_mayoreo]").val(precio.toFixed(2));
	});
	$("[name=ganancia_membrecia]").keyup(function(){
		var ganancia=$(this).val();
		var precio=calcular_precio(ganancia);
		$("[name=pre_membrecia]").val(precio.toFixed(2));
	});

	$("[name=precio_caja]").keyup(function(){
		$("[name=pre_compra]").val(calcular_pre_compra());
	});
	$("[name=pza_x_caja]").keyup(function(){
		$("[name=pre_compra]").val(calcular_pre_compra());
	});

	function calcular_pre_compra(){
		var pre_caja=$("[name=precio_caja]").val();
		var unidades=$("[name=pza_x_caja]").val();
		if(pre_caja!=null && unidades!=null){
			var precio_compra=parseFloat(pre_caja)/parseFloat(unidades);
		precio_compra=precio_compra.toFixed(2);
		}else{
			precio_compra=0;
		}
	return precio_compra;
	}


	function calcular_precio(ganancia){
		var precio_caja=$("[name=precio_caja]").val();
		var unidades=$("[name=pza_x_caja]").val();
		var pre=parseFloat(precio_caja) / parseFloat(unidades);
		var sum_ganancia=pre*(parseFloat(ganancia)/100);
		var precio=pre+sum_ganancia;

		return precio;
	}

//----------------------------------------------------------------------------------------



var cont=0;
var imagen="";
//-------------------------al seleccionar ubicacion del articulo---------------------------
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});

	$("#guardarProducto").click(function(){
		cont=0;
	});

//------------------------SUBIR IMAGEN----------------------------
	$("#formuploadajax").on("submit", function(e){
  e.preventDefault();
  var f = $(this);
  var formData = new FormData(document.getElementById("formuploadajax"));
  formData.append("dato", "valor");
  //formData.append(f.attr("name"), $(this)[0].files[0]);
  $.ajax({
      url: "{{url('/uploadImg')}}",
      type: "post",
      dataType: "",
      data: formData,
      cache: false,
      contentType: false,
	 			processData: false
	      }).done(function(e){
		// alert(e.nombre);
		if (e.status=="success") {
			if (cont==0) {
				imagen=e.nombre;
			}else{
				imagen=imagen+","+e.nombre;
			}
			cont++;
			$("#fotos").val(imagen);

			$("#subirImg").removeClass("btn btn-primary");
			$("#subirImg").addClass("btn btn-success");
			$("#subirImg").text("Subido");
		}else{
			$("#subirImg").removeClass("btn btn-primary");
			$("#subirImg").addClass("btn btn-danger");
			$("#subirImg").text("ERROR");
		}
	}).fail(function (jqXHR, exception) {
    // Our error logic here
    console.log(exception);
    });
});



//###############validando el formulario para enviarlo solo una ves###########################
    enviando = false; //Obligaremos a entrar el if en el primer submit
    function checkSubmit() {
        if (!enviando) {
            enviando= true;
            return true;
        } else {
            //Si llega hasta aca significa que pulsaron 2 veces el boton submit
            alert("El formulario ya se esta enviando");
            return false;
        }
    }
//#######################################################################






</script>
@endsection