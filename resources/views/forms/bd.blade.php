@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<p>ACTUALIZAR LA TABLA ARTICULOS DESDE LOS DATOS DEL SERVIDOR NALAEXPRESS.COM</p>
		</div>
		<div class="card-body">

			<form method="post" action="{{url('/update_database')}}">
				@csrf
				<button type="submit" class="btn btn-lg btn-primary" id="btn_actualizar">Actualizar Articulos</button>
				<span style="margin-left: 20%;display: none;" id="espere">ESPERE... <i class="fas fa-2x fa-spinner fa-spin"></i></span>
			</form>
			<br><br>
			@if(session("status"))
				<div class="alert @if(session('status')=='success') alert-success @else alert-error @endif" id="alert">
				@if(session('status')=='success')
				<b>Actualizado correctamente.</b>
				@else
				<b>ERROR al intentar actualiza, verifique si hay conexión con el servidor, es posible que no tenga conexión a internet</b>
				@endif
			</div>
			@endif

		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">

	$("#btn_actualizar").click(function(){
		$("#espere").show();
	});

	
	@if(session("status"))
		setInterval(function(){
			$("#alert").hide();
		},60000);
	@endif
	
</script>
@endsection