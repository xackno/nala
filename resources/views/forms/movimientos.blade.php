@extends('layouts.app')
@section('content')
<div class="container-fluid" style="margin-top:5px">

	@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif

	@if(isset($recuperaciones))

		@php $total_abono=0;@endphp
		@foreach($recuperaciones as $ab)
			@php $total_abono+=$ab->cantidad; @endphp
		@endforeach

	@endif

	@php $total_cobrar=0;  $con_intereses=0;  @endphp
	@if(isset($id))
		@foreach($pedidos as $r)
			@php
				$desc=0;
				if($r->descuento=='true'){
					$desc=20;
				}else{ $desc=0;}
				$x_cobrar=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$total_cobrar+=$x_cobrar;

				$por_cobrar3=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$fecha_limite=new DateTime($r->fecha_limite);

				if($r->status_recuperacion=="liquidado"){
					$ahora=explode(" ",$r->fechaliquidacion);
					$ahora=new DateTime($ahora[0]);
				}else{
					$ahora=new DateTime(date('Y-m-d'));
				}

				$dias_transcurrido0=0;
				if($fecha_limite < $ahora){
					$diff = $fecha_limite->diff($ahora);
					$dias_transcurrido0=$diff->days;
				}else{
					$dias_transcurrido0=0;
				}

				$intereses=($r->interes/100)*$por_cobrar3;
				$con_intereses+=number_format(($por_cobrar3)+($intereses*$dias_transcurrido0),2);
			@endphp
		@endforeach
	@endif

	@php
		$permiso_set_efectivo="deshabilitado";
		$por_cobrar=0;
		if(isset($con_intereses) &&isset($total_abono)){
			$por_cobrar=$con_intereses-$total_abono;
		}
		if($turno==0 && $caja==0){
			$permiso_set_efectivo="deshabilitado";
		}else{
			if($por_cobrar==0){//ya esta liquidado
				$permiso_set_efectivo="deshabilitado";
			}else{
				$permiso_set_efectivo="habilitado";
			}
		}

		


	@endphp
	<div class="row">
		<div class="col-3">
			<label>Buscar cliente</label>
			<div class="row" id="div_buscador">
				<div class="col-10" >
					<input type="text" name="buscador" id="buscador" placeholder="Buscar cliente" autofocus class="form-control" autocomplete="off">
					<small id="alert_input" class="form-text"></small>
				</div>
				<div class="col-2">
					<button class="btn btn-primary" id="btn_buscador"><i class="fas fa-search"></i></button>
				</div>
				<select id="select_clientes" class="form-control selectpicker" title="Seleccione..." data-live-search="true" style="display: none;"></select>
			</div>


			
			<div class="input-group" >
				
			</div>
		</div>
		<div class="col-5" id="col_movimientos">
			<label>Movimiento:</label>
			<select class="form-control" id="operaciones">
				<option selected="true" disabled="true">Selecciones...</option>
				<option value="todos">Todos</option>
				<option value="por_cobrar">Pedidos por cobrar</option>
				<option value="liquidacion" @if($permiso_set_efectivo=="deshabilitado") class="bg-secondary" disabled title="Es necesario loguearse con un usuario de caja, y tener un turno iniciado" @endif>Liquidar</option>
				<option value="pagos" @if($permiso_set_efectivo=="deshabilitado") class="bg-secondary" disabled title="Es necesario loguearse con un usuario de caja, y tener un turno iniciado" @endif>Abonar a cuenta</option>
				<option value="facturar" disabled="">Facturación</option>
			</select>
		</div>
		<div class="col-4 " >
			<div style="width:100%" id="botones_descargar">
				@if(isset($id) && isset($movimiento))
				<a  href="{{url('movimiento_pdf/'.$id.'/'.$movimiento)}}" target="_blank" title="Solo un administrador puede descargar el PDF" @if(Auth::user()->caja!=NULL) class="btn btn-danger disabled"  @else class="btn btn-danger" @endif>Descardar PDF<i class="fas fa-file-pdf"></i></a>

				<a href="{{url('/movimiento_excel/'.$id.'/'.$movimiento)}}" title="Solo un administrador puede descargar el archivo Excel" @if(Auth::user()->caja!=NULL)  class="btn btn-success disabled"  @else class="btn btn-success" @endif>Descardar EXCEL<i class="fas fa-file-excel"></i></a>
			@endif
			</div>
			
		</div>
	</div>


	@if(isset($cliente))
	<div class="card" id="datos_cliente" style="margin-top: 0;">
		<div class="card-header">
			<div class="row">
				<div class="col-4">
					<b style="text-transform:uppercase;">{{$cliente->nombre." ".$cliente->apellidos}}</b>
					<p style="margin:0px !important:margin-top:-10px !important;margin-bottom:0px !important"><small class="text-secondary" >{{$cliente->localidad." ".$cliente->municipio}}</small></p>
					
				</div> 
				<div class="col-4">
					<label class="text-danger">
						@if(isset($id))
							@if($con_intereses-$total_abono==0 && $total_abono!=0)
							<h4 class="text-success"><i class="fas fa-check"></i> LIQUIDADO</h4>
							@elseif($con_intereses-$total_abono==0 && $total_abono==0)
							<h4 class="text-success" style="text-decoration:underline;">Sin pedidos</h4>
							@else
							Pendiente:$ <span id="pendiente" style="text-decoration: underline;">{{number_format($con_intereses-$total_abono,2)}}</span> MXN
							@endif
						@endif
					</label>
				</div>
				<div class="col-4">
					<strong class="float-right" id="calificacion">
						@for($x=0;$x<$cliente->calificacion;$x++)
							<i class="fas fa-star text-warning"></i>
						@endfor
					</strong>
					<br>
					<p id="identificacion" class="float-right" style="margin:0"> {{str_pad($cliente->id, 8, '0', STR_PAD_LEFT)}}</p>
				</div>
			</div>
		</div>
		<div class="card-body" style="padding-top:3px !important;margin-bottom:0">
			<div class="row">
				<div class="col-9">
					<label>PEDIDOS</label>
					<div class="table-responsive" style="font-size: 13px;">
						<table class="table table-striped table-bordered">
							<thead class="bg-primary text-center">
								<tr>
									<th>No.Pedido</th>
									<th>fecha/pedido</th>
									<th>Límite</th>
									<th>% interés</th>
									<th>Importe</th>
									<th>Envio</th>
									<th>Descuento</th>
									<th>Pago inicial</th>
									<!-- <th>Por cobrar</th> -->
									<th>Fecha Entrego</th>
									<th>Atendió</th>
									<th class="d-none">Factuación</th>
								</tr>
							</thead>
							<tbody>
							@php  $total_descuento=0; $total_importe=0; $total_pago_inicial=0; $total_por_cobrar=0; $total_costo_envio=0; @endphp
								
								@foreach($pedidos as $r)
									@php
										$descuento=0;
										if($r->descuento=='true'){
											$descuento=20;
										}else{ $descuento=0;}

										$por_cobrar=($r->subtotal_pedido+$r->costo_envio)-($descuento + $r->pago_inicial);
										$total_descuento+=$descuento;
										$total_importe+=$r->subtotal_pedido;
										$total_pago_inicial+=$r->pago_inicial;
										$total_por_cobrar+=$por_cobrar;
										$total_costo_envio+=$r->costo_envio;
									@endphp
									<tr>
										<td class="text-center"> {{ str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
										<td class="text-center">{{$r->created_at}}</td>
										<td @if($r->fecha_limite < date("Y-m-d")) class="table-danger" title="vencido" @endif>{{$r->fecha_limite}}</td>
										<td class="text-center">{{$r->interes}}%</td>
										<td class="text-right">$ {{$r->subtotal_pedido}}</td>
										<td class="text-right">$ {{$r->costo_envio}}</td>
										<td class="text-right">$ {{number_format($descuento,2)}}</td>
										<td class="text-right">$ {{$r->pago_inicial}}</td>
										<!--<td class="text-right text-danger" >$ 
											{{number_format($por_cobrar,2)}}
										</td> -->
										<td class="text-center">
											@php
												$status=explode(",",$r->historial_status);
											@endphp
											@foreach($status as $s)
												@php
													$nom_status=explode("%",$s);
												@endphp
													@if($nom_status[0]=="recibido_x_pagar" || $nom_status[0]=="recibido_c_devolucion" || $nom_status[0]=="recibido_y_pagado")
														{{$nom_status[1];}}
													@endif
											@endforeach
										</td>
										<td class="text-center">{{$r->cajero}}</td>
										<td class="text-center d-none" >
											@if($r->factura=="no" || $r->factura==null)
												{{"NO"}}
											@else
												{{"SI"}}
											@endif
										</td>
									</tr>
								@endforeach
								<tr id="tr_totales">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="text-right table-primary">$ {{number_format($total_importe,2)}}</td>
									<td class="text-right table-primary">$ {{number_format($total_costo_envio,2)}}</td>
									<td class="text-right table-warning">$ {{number_format($total_descuento,2)}}</td>
									<td class="text-right table-success">$ {{number_format($total_pago_inicial,2)}}</td>
									<!-- <td class="text-right table-danger">$ {{number_format($total_por_cobrar-$total_abono,2)}}</td> -->
									<td></td>
									<td></td>
									<td class="d-none"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<hr>
					<!-- ############################intereses################################# -->
			<div class="row">
				<label>Cálculo de interéses</label>
				<div class="table-responsive" style="font-size: 13px;">
					<table class="table table-striped table-bordered">
						<thead class="bg-primary text-center">
							<tr>
								<th>No.Pedido</th>
								<th>fecha/pedido</th>
								<th>Límite</th>
								<th>% interés</th>
								<th>TOTAL</th>
								<th>Dias Transcurrido</th>
								<th>Int. diarios</th>
								<th>Total int.</th>
								<th>TOTAL c/int.</th>
								<th>pendiente</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@foreach($pedidos as $r)

							<tr @if($r->status_recuperacion=="liquidado") class="table-success" @endif>
								<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
								<td class="text-center">{{$r->created_at}}</td>
								<td @if($r->fecha_limite < date("Y-m-d")) class="table-danger" title="vencido" @endif>{{$r->fecha_limite}}</td>
								<td>{{$r->interes}}%</td>
								<td>${{number_format($r->por_cobrar2,2)}}</td>
								<td>@if($r->dias_transcurrido==1){{$r->dias_transcurrido}} Día @else {{$r->dias_transcurrido}} Días @endif</td>
								<td class="text-right">$ {{number_format($r->intereses_diarios,2)}}</td>
								<td class="text-right">$ {{number_format($r->intereses,2)}}</td>
								<td class="text-danger text-right"><b> $ {{number_format($r->total_con_intereses,2)}}</b></td>
								<td class="text-right">${{number_format($r->diferencia,2)}}</td>
								<td class="text-center">{{$r->status_recuperacion}} @if($r->status_recuperacion=="liquidado") <p style="margin: 0"><span class="badge badge-primary">{{$r->fechaliquidacion}}</span></p> @endif </td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
				</div>
				<div class="col-3">
					<label>RECUPERACIÓN</label>
					@if(isset($recuperaciones))
					<div class="table-responsive" style="font-size:13px">
						<table class="table table-striped table-bordered">
							<thead class="bg-success">
								<tr>
									<th>Fecha</th>
									<th>Tipo</th>
									<th>Pedido</th>
									<th>Cantidad</th>
								</tr>
							</thead>
							<tbody>
								@foreach($recuperaciones as $ab)
								<tr> 
									<td>{{$ab->created_at}}</td>
									<td>{{$ab->tipo}}</td>
									<td>{{str_pad($ab->id_pedido, 8, '0', STR_PAD_LEFT)}}</td>
									<td class="text-right">${{$ab->cantidad}}</td>
								</tr>
								@endforeach
								<tr style="border-top:1.5px solid black !important">
									<td colspan="3" class="text-right">TOTAL:</td>
									<td class="table-success text-right">$ {{number_format($total_abono,2)}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					@endif
				</div>
			</div>
			
	</div>
	@endif
</div>


<!-- ############################################################################## -->
<!-- Modal -->
	<div class="modal fade" id="modal_abono" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Abono a cuenta</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	@if(isset($id))
	      	<label class="float-right text-danger">Total por cobrar:$ <span id="totalpendienteModal"></span></label>
	      	@endif
	      	<br>
	        <form action="{{url('/abono')}}" method="post" onsubmit="return checkSubmit();">
	        	@csrf
	        	<input type="hidden" name="id_cliente" value="@if(isset($id)){{$id}}@endif" >
	        	<input type="hidden" name="cajero" value="{{Auth::user()->id}}">
	        	<input type="hidden" name="caja" value="{{$caja}}">
	        	<input type="hidden" name="turno" value="{{$turno}}">
	        	<label>Seleccione el pedido al que desea abonar</label>
	        	<select name="pedido" id="select_pedidos" class="form-control">
	        		@if(isset($pedidos))
	        		@foreach($pedidos as $r)
	        			@if($r->status_recuperacion!="liquidado")
	        			<option selected disabled>Seleccione </option>
	        			<option value="{{$r->id}}" data-pendiente="{{$r->pendiente}}">{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</option>
	        			@endif
	        		@endforeach
	        		@endif
	        	</select>
	        	<label>Cantidad a abonar</label>
	        	<input type="number" step="any" name="cantidad" class="form-control" required="true">
	        	<label>Efectivo:</label>
	        	<input type="number" step="any" id="efectivo" class="form-control">
	        	<br><br>
	        	<label id="cambio" class="text-primary"></label>
	        	<button class="btn btn-primary float-right" id="btn_abonar">Abonar</button>
	        	
	        </form>
	      </div>
	    </div>
	  </div>
	</div>

<!-- ##########################modal liquidar############################################### -->
	<div class="modal fade" id="modal_liquidar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Liquidar cuenta</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	@if(isset($id))
	      	<label class="float-right text-danger">Liquida con:$ <span id="totalpendienteModal_liquidar"></span></label>
	      	@endif
	      	<br>
	        <form action="{{url('/liquidar')}}" name="form_liquidar" method="post" onsubmit="return checkSubmit();" class="text-center">
	        	@csrf
	        	<input type="hidden" name="pendiente" value="@if(isset($id)){{number_format($con_intereses-$total_abono,2)}}@endif">
	        	<input type="hidden" name="id_cliente" value="@if(isset($id)){{$id}}@endif" >
	        	<input type="hidden" name="cajero" value="{{Auth::user()->id}}">
	        	<input type="hidden" name="caja" value="{{$caja}}">
	        	<input type="hidden" name="turno" value="{{$turno}}">
	        	<label>Seleccione el pedido que desea liquidar</label>
	        	<select name="pedido" id="select_pedidos_liquidar" class="form-control">
	        		@if(isset($pedidos))
	        		@foreach($pedidos as $r)
	        			@if($r->status_recuperacion!="liquidado")
	        			<option selected disabled>Seleccione </option>
	        			<option value="{{$r->id}}" data-pendiente="{{$r->pendiente}}">{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</option>
	        			@endif
	        		@endforeach
	        		@endif
	        	</select>
	        	<label class="float-left">Efectivo:</label>
	        	<input type="number" step="any" name="cantidad_liquidar" class="form-control" required="true" placeholder="0.00">
	        	<label id="cambio_liquidacion" class="text-primary"></label>
	        	<br><br>
	        	<button type="button" class="btn btn-success" id="btn_liquidar"  onclick="preguntar()">LIQUIDAR</button>
	        	
	        </form>
	      </div>
	    </div>
	  </div>
	</div>

<style type="text/css">
	td,th{
		padding: 3px !important;

	}
	#tr_totales td{
		border-top: 1.5px solid black !important;
	}
</style>


@endsection
@section('script')
<script type="text/javascript">

$("#select_pedidos").change(function(){
	var pendiente=$(this).find(":selected").data("pendiente");
	pendiente=pendiente.toFixed(2);
	$("#totalpendienteModal").html(pendiente);
});

$("#select_pedidos_liquidar").change(function(){
	var pendiente=$(this).find(":selected").data("pendiente");
	pendiente=pendiente.toFixed(2);
	$("#totalpendienteModal_liquidar").html(pendiente);
});


//####################preguntar antes de enviar el form de liquidacion
function preguntar(){
    if (confirm('¿Estas seguro de ejecutar esta acción?')){
       $("[name=form_liquidar]").submit();
    }
}


//#####################para liquidar###################

$("[name=cantidad_liquidar]").keyup(function(){
	@if(isset($id))
	var total='{{$con_intereses-$total_abono,2}}';
	@else 
	var total=0;
	@endif
	
	total=parseFloat(total);
	var thiss=$(this).val(); thiss=parseFloat(thiss);
	var cambio=thiss-total;
	cambio=cambio.toFixed(2);

	if(thiss>total){
		$("#cambio_liquidacion").html("CAMBIO: $"+cambio);
	}else if(thiss<total){
		$("#cambio_liquidacion").html('');
	}else if(thiss==total){
		$("#cambio_liquidacion").html("CAMBIO: $"+cambio);
	}
});


//#####################para modal abonar###########################################

	// $("[name=cantidad2]").keyup(function(e){
	// 	var cantidad=$(this).val();
	// 	cantidad=parseFloat(cantidad);
	// 	@if(isset($id))
	// 	if(cantidad>'{{$total_por_cobrar-$total_abono}}'){
	// 		alert("No puede abonar mas de la cuenta.");
	// 		$("#btn_abonar").hide();
	// 		if(e.which==13){
	// 			return false;
	// 		}
	// 	}else{
	// 		$("#btn_abonar").show();
	// 	}
	// 	@endif
	// });

	$("#efectivo").keyup(function(){
		var abono=$("[name=cantidad]").val();
		var efectivo=$(this).val();
		abono=parseFloat(abono);
		efectivo=parseFloat(efectivo);
		var cambio=efectivo-abono;
		$("#cambio").html("CAMBIO: $ "+cambio.toFixed(2));
	});



	@if(isset($movimiento))
		$("#operaciones").val("{{$movimiento}}");
	@endif

//########################select operaciones#################################
	$("#operaciones").change(function(){
		if($(this).val()=="por_cobrar"){
			let url = "{{ url('/get_por_cobrar') }}";
			@if(isset($id))
		    	url=url+"/"+"{{$id}}";
		    	document.location.href=url;
	    	@else
	    		alert("Debe seleccionar un cliente.");
	    	@endif

		}else if($(this).val()=="todos"){
			let url = "{{ url('/load_movimientos') }}";
			@if(isset($id))
			    url=url+"/"+"{{$id}}";
			    document.location.href=url;
			@else
	    		alert("Debe seleccionar un cliente.");
	    	@endif
		}else if($(this).val()=="pagos"){
			@if(isset($id))
				$("#modal_abono").modal("show");
			@else
				alert("Debe seleccionar un cliente.");
			@endif
		}else if($(this).val()=="liquidacion"){
			liquidacion();
		}
	});

//#################################################################
	function liquidacion(){
		$("#modal_liquidar").modal("show");
		$('#modal_liquidar').on('shown.bs.modal', function (e) {
              $('[name=cantidad_liquidar]').focus();
            });
	}



//############################buscarcliente para cargar su información##############
	$("#buscador").keyup(function(e){
		var valor=$(this).val();
		if(valor.length>4){
			if(e.which==13){
				var buscar=$(this).val()
				buscar_cliente(buscar,"nombre");
			}
		}else{
			if(e.which==13){
				$("#alert_input").html("Minimo 5 caracteres.");
				$("#alert_input").addClass("text-danger");
				$("#buscador").addClass("is-invalid");
			}	
		}
		
	});	
	//----------------------------------------
	$("#div_buscador button").click(function(){
		var buscar=$("#buscador").val();
		if(buscar.length>4){
			buscar_cliente(buscar,"nombre");
			$("#alert_input").removeClass("text-danger");
			$("#alert_input").html('');
			$("#buscador").removeClass("is-invalid");
		}else{
			$("#alert_input").html("Minimo 5 caracteres.");
			$("#alert_input").addClass("text-danger");
			$("#buscador").addClass("is-invalid");
		}
		
	});
	//-------------------------------------------
	$('.selectpicker').selectpicker('hide');
	//-------------------------------------------
	@if(!isset($id))
		$("#col_movimientos").hide();
	@endif
	function buscar_cliente(buscar,tipo){
		$("#calificacion,#datos_cliente .card-header b,#datos_cliente .card-header small,#identificacion").html('');
		 $("#select_clientes").html('');
		$.ajax({
			url:'{{url("/buscar_info_cliente")}}',
			type:"post",
			dataType:"json",
			data:{
				tipo:tipo,
				buscar:buscar},
			success:function(e){
				if(e.length>1){
					$("#alert_input").html('');

					$("#datos_cliente,#operaciones,#botones_descargar").hide();
					$("#buscador,#btn_buscador").hide();
					$('.selectpicker').selectpicker('show');
					$("#select_clientes").append("<option selected disabled>Seleccione...</option>");
					for(var i=0;i<e.length;i++){
						$("#select_clientes").append("<option style='text-transform:uppercase' value='"+e[i].id+"' data-content=\" <b>"+e[i].nombre+" "+e[i].apellidos+"</b><p class='text-secondary' style='font-size:9px;background:white'>"+e[i].localidad+", "+e[i].municipio+"</p>  <span class='badge badge-success' style='position:absolute;top:5px;right:5px;'>"+e[i].identificacion+"</span>\"></option>");
					}
					
					
				}else if(e.nombre!=null){
					load_info(e.id);
					$("#col_movimientos").show();
					$("#alert_input").html('');
				}else if(e.length==0){
					clear_no_found();
					
				}
				$('.selectpicker').selectpicker('refresh');
				
			},error:function(){
				alert("ERROR del servidor.");
			}
		});
	}
	//--------------------------------------------
	$("#select_clientes").on("change",function(){
		var buscar=$(this).val();
		buscar_cliente(buscar,"id");
	});

	function clear_no_found(){
		$("#alert_input").removeClass("text-danger");
		$("#alert_input").addClass("text-secondary");
		$("#alert_input").html('<i class="fas fa-exclamation-triangle text-warning"></i> No se encontró registro.');
		$("#col_movimientos,#botones_descargar,.card").hide();
	}



	function load_info(id){
    let url = "{{ url('/load_movimientos') }}";
    url=url+"/"+id;
    document.location.href=url;
}




//#############evitar que se envie mas de 2 veces el formulari ode abonos######################
	enviando = false; //Obligaremos a entrar el if en el primer submit
    function checkSubmit() {
        if (!enviando) {
    		enviando= true;
    		return true;
        } else {
            //Si llega hasta aca significa que pulsaron 2 veces el boton submit
            alert("El formulario ya se esta enviando");
            return false;
        }
    }



//###################################################################################
</script>
@endsection