@extends('layouts.app')
@section('content')
<div class="container-fluid" style="padding-top:10px">
	@if(session('success'))
		<div class="alert alert-success alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif

	<div class="card">
		<div class="card-body">
			<div class="col-3 input-group">
				<input class="form-control " type="text" placeholder="Buscar"
	  		aria-label="Search" id="input_search" autofocus="">
	  		<button class="btn btn-primary" id="btn_buscar_prod"><i class="fa fa-search"></i></button>
			</div>
			<br>

			<div class="row">
  			<div class="col-xl-3 col-md-6 mb-4 ">
  				<form action="{{route('crearOferta')}}" method="post" id="form_oferta">
						@csrf
						<input type="number" name="id_producto" id="id_producto" class="d-none" readonly="true">
						<label>Unidad</label>
						<input type="text" name="unidad" id="unidad" class="form-control" readonly="true">

		  			<label for="descripcion">Descripción del producto</label>
		  			<textarea class="form-control" name="descripcion_producto" id="descripcion" readonly="true"></textarea>
		  			
		  			<label>Precio de venta <b class="text-danger">$</b></label>
		  			<input type="number" name="precio_venta" id="precio_venta" class="form-control" readonly="true">

		  			<label >Precio de Oferta</label>
		  			<input type="number" name="precio_oferta" id="precio_oferta" class="form-control border-info">

		  			<label for="validez">Valido hasta</label>
		  			<input type="date" name="fecha_valido" id="validez" class="form-control border-info" >
		  			
		  			<label>Imagen</label>
		  			<input type="text" name="foto" id="foto" class="form-control" readonly="true">

		  			<label>Url de youtube</label>
		  			<input type="text" name="url" id="url" class="form-control border-info">
		  			<br>
		  			<a class="btn btn-success  text-light" id="btn_agregar" style="float: right;"><i class="fa fa-plus text-warning"></i> Agregar</a>
					</form>
  			</div>



  			<div class="col-xl-3 col-md-6 mb-4 contenedor" style="background:#EAECEE;text-align: center;height: 300px;padding-top: 45px" id="div_foto">
    				<img src="img/articulos/no-images.jpg" class="imagen" style='margin-top:auto;margin-bottom:auto;width:80%'>
    		</div>


    		<div class="col-xl-6 col-md-12 mb-4">
    			<hr>
          <b class="badge badge-info" style="font-size: 130%">En oferta  </b> <span><i class="fa fa-shopping-bag text-success fa-2x"></i><h4 class="text-primary"></h4></span>
          <div class="table-responsive">
					  <table class="table table-striped table-bordered" id="tabla_oferta">
					    <thead class="table-dark">
					    	<tr>
					    		<th class="d-none">id</th>
					    		<th>id producto</th>
					    		<th>unidad</th>
					    		<th>Descripción</th>
					    		<th>Precio</th>
					    		<th>Precio de oferta</th>
					    		<th>fecha valido</th>
					    		<th><i class="fa fa-picture-o text-info"></i></th>
					    		<th><i class="fa fa-cog"></i></th>
					    		
					    	</tr>
					    </thead>
					    <tbody id="lista_ofertas">
					    	@foreach($ofertas as $oferta)
					    		<tr>
					    			<td class="d-none">{{$oferta->id}}</td>
					    			<td>{{$oferta->id_producto}}</td>
					    			<td>{{$oferta->unidad}}</td>
					    			<td>{{$oferta->descripcion_producto}}</td>
					    			<td>{{$oferta->precio_venta}}</td>
					    			<td>{{$oferta->precio_oferta}}</td>
					    			<td>{{$oferta->fecha_valido}}</td>
					    			<td>
					    				@if($oferta->foto!=null || $oferta->foto!="ninguno")
					    					<img src="{{asset('/img/articulos/'.$oferta->foto)}}" width="40px">
					    				@else
												<img src="{{asset('/img/articulos/no-images.jpg')}}" width="40px">
					    				@endif
					    			</td>
					    			<td>
					    				<form action="{{ route('ofertas.destroy',$oferta->id) }}" method="post" class="btnAction">
					              			 @csrf
                 							 @method('DELETE')
					              			<button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
					              		</form>
					    			</td>
					    		</tr>
					    	@endforeach
					    </tbody>
					  </table>
					</div>
    		</div>
  		</div><!-- fin row -->
		</div><!-- fin card-body -->
	</div><!-- fin card-->
</div><!-- fin container-fluid -->


<div class="modal" tabindex="-1" id="modal_buscar_prod" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#20c997">
        <h5 class="modal-title">Retail Stehs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p class="text-info">Pulse doble click para seleccionar el producto.</p>
      	<div class="table-responsive" style="overflow-y: scroll;height: 400px">
				  <table class="table table-striped" id="tab_modal_buscado">
				    <thead class="table-dark">
				    	<tr>
				    		<th>id</th>
				    		<th>Clave</th>
				    		<th>Unidad</th>
				    		<th>Descripción</th>
				    		<th>Precio</th>
				    		<th>Existencia</th>
				    		<th>Foto</th>
				    	</tr>
				    </thead>
					  <tbody id="lista_buscado">
					  	
					  </tbody>
				  </table>
			</div>
      </div>

    </div>
  </div>
</div>



<style type="text/css">
	.contenedor:hover .imagen {-webkit-transform:scale(1.3);transform:scale(1.3);}
	.contenedor {overflow:hidden;}
	#tabla_oferta thead tr th{
		font-size: 60%;
		padding: 2px;
		border:1px;
	}
	#tabla_oferta tbody tr td{
		font-size: 60%;
		padding: 1px;
		border:1px solid #eee;
		border-bottom: 2px solid #66bb6a;

	}
</style>



@endsection

@section('script')
<script type="text/javascript">

var eliminaroferta=function(id){
	alert("eliminando"+id);
}

//-------------------------------------------------------------------------------
	$("#btn_agregar").click(function(){
		var id_pro=$("#id_producto").val();
		var cont=0;
		$("#lista_ofertas").find("tr td:first-child").each(function(){
			cont++;
			if ($(this).siblings("td").eq(0).html()==id_pro) {
				alert("YA ESTA AGREGADO");
			}else{
				$("#form_oferta").submit();
			}
		    });
		
		if (cont==0) {
			$("#form_oferta").submit();
		}
	});
//##############################################################################
	$("#btn_buscar_prod").click(function () {

		
		// alert($("#input_search").val());
		$("#lista_buscado").html('');
		$.ajax({
          url:'{{route("buscarProducto")}}',
          type:'POST',
          dataType:'json',
          data:{
          		busqueda:$("#input_search").val()
          		}
            }
            ).done(function(e){
              if (e=="") {
               $("#lista_buscado").append("<tr class='text-danger'> <td  colspan='8'><b>No se encontró registro!!</b></td></tr>");
              }else{
              	$("#modal_buscar_prod").modal("show");
                for(var x=0;x<e.length;x++){
                	if (e[x].fotos=='ninguno' || e[x].fotos==null) {
                		$("#lista_buscado").append(
	                	"<tr tabindex='1' class='move'><td width='1px' >"
	                	+e[x].id_p+"</td><td>"
	                	+e[x].clave+"</td><td class=''>"
	                	+e[x].unidad+"</td><td>"
	                	+e[x].descripcion_articulo+"</td><td>"
	                	+e[x].pre_unidad+"</td><td>"
	                	+e[x].existencia+"</td> <td class='d-non'><img src='{{asset('')}}/img/articulos/no-images.jpg' style='margin-top:auto;margin-bottom:auto;width:80%' class='imagen'> <span class='d-none'>no-images.jpg</span></td></tr>"
	                	);
                	}else{
                		fotos=e[x].fotos;
                		fotos=fotos.split(",");
                		$("#lista_buscado").append(
	                	"<tr tabindex='1' class='move'><td width='1px' >"
	                	+e[x].id_p+"</td><td>"
	                	+e[x].clave+"</td><td class=''>"
	                	+e[x].unidad+"</td><td>"
	                	+e[x].descripcion_articulo+"</td><td>"
	                	+e[x].pre_unidad+"</td><td>"
	                	+e[x].existencia+"</td> <td class='d-non'><img src='{{asset('')}}/img/articulos/"+fotos[0]+"' style='margin-top:auto;margin-bottom:auto;width:80%' class='imagen'> <span class='d-none'>"+fotos[0]+"</span></td></tr>"
	                	);
                	}
                
               }//fin for
              }
              
              });//fin done 
	});
//_________________________________________________________________________________________________

	$("#lista_buscado").on("dblclick","tr",function(){
		// alert($(this).find("td").eq(0).html());
		$("#id_producto").val($(this).find("td").eq(0).html());
		$("#unidad").val($(this).find("td").eq(2).html());
		$("#descripcion").val($(this).find("td").eq(3).html());
		$("#precio_venta").val($(this).find("td").eq(4).html());
		
		// alert($(this).find("td").eq(6).find("img").attr("src"));
		let fot=$(this).find("td").eq(6).find("span").html();
		$("#foto").val(fot);
		$("#div_foto").html($(this).find("td").eq(6).html());
		$("#modal_buscar_prod").modal("hide");
	});
//______________________________________________________________________________________________________
</script>
@endsection