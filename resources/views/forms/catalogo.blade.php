@extends('layouts.app')
@section('content')
<div class="container-fluid" style="background:white !important">


  <div class="row">
    <div class="col col-md-2 col-lg-2 col-xl-2 d-none d-sm-none d-md-block" id="menu_lateral"><!--menu lateral  -->
      <br>
      <h5 class="text-center text-primary"><b>RETAIL 2</b></h5>

      <div class="accordion" id="accordionExample">
        <div class="card">
          <p class="text-right">Total <span class="badge badge-secondary">{{$total}}</span></p>
          <div class="card-header" id="headingOne" >
              <b class="text-secondary text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="font-size:12px">CATEGORIAS</b>
              <span class="badge badge-primary float-right">{{count($categorias)}}</span>
          </div>

          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
              <ul class="nav flex-column">
                @foreach($categorias as $r)
                  <li>
                    <a onclick="buscar_opciones('categorias','{{$r->nombre}}');">{{$r->nombre}}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo">
              <b class="text-secondary text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="font-size:12px">MARCAS</b>
              <span class="badge badge-primary float-right">{{count($marcas)}}</span>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
              <ul class="nav flex-column">
                @foreach($marcas as $r)
                  <li class="">
                    <a class="" onclick="buscar_opciones('marcas','{{$r->nombre}}');">{{$r->nombre}}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingThree">
              <b class="text-secondary text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="font-size:12px">PROVEDORES</b>
              <span class="badge badge-primary float-right">{{count($provedores)}}</span>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
              <ul class="nav flex-column">
                @foreach($provedores as $r)
                  <li class="">
                    <a class="" onclick="buscar_opciones('provedores','{{$r->nombre}}');">{{$r->nombre}}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col col-md-10 col-lg-10 col-xl-10"><!-- articulos -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col col-md-4 col-lg-4 col-xl-4">
              <form action="{{route('buscar_catalogo')}}" method="post" id="form_buscador">
                @csrf
                  <div class="input-group">
                    <input type="hidden" name="tipo" id="tipo_busqueda" value="libre">
                    <input type="hidden" name="nombre" id="nombre_buscar">
                  <input type="text" name="input_buscar" id="input_buscar" class="form-control" placeholder="Buscar en mi catálogo..." autofocus>
                  <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
              </form>
              </div>
            <div class="col col-md-8 col-xl-8 col-lg-8"></div>
          </div>
          <hr>

          <!-- #########################################listando catalogo--############################### -->
          <div class="row">
            @foreach($productos as $r)
            <div class="col col-sm-2 col-lg-3 col-xl-4" style="margin-bottom: 20px;">
              <a title="{{$r->descripcion_articulo}}" onclick="ver_articulo({{$r->id}});" >
                <div class="card-deck">
                  <div class="card text-center">
                    @if($r->fotos!="ninguno" && $r->fotos!='' )
                    @php
                      $fotos=explode(",",$r->fotos);
                    @endphp
                    <div class="doc">
                      <div class="box">
                        <img  src="{{asset('/img/articulos/'.$fotos[0])}}" class="card-img-to" alt="{{$r->descripcion_producto}}">
                      </div>
                    </div>
                    <img >
                    @else
                    <div class="doc">
                      <div class="box">
                        <img src="{{asset('/img/articulos/no-images1.jpg')}}" class="card-img-to">
                      </div>
                    </div>
                    @endif
                    <div class="card-body text-left">
                      <h5 class="card-title texto_corto">{{$r->descripcion_articulo}}</h5>
                      <p class="card-text text-primary"><b>MXN $ {{number_format($r->pre_unidad,2,'.',' ')}}</b></p>
                      <p class="card-text texto_corto"><small class="text-muted">{{$r->descripcion_catalogo}}</small></p>
                      <p class="card-text" style="font-size:10pt"><b>EXISTENCIA EN ESTOCK: <span class="text-success" style="text-decoration:underline">{{number_format($r->existencia_stock)}}</span> <span>{{$r->unidad}}</span></b></p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            @endforeach
            
            <div style="width: 80%;" >
              <br><br><br>
              {{$productos->links() }}
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>


</div><!-- fin container -->

<style type="text/css">
  .doc{
      display: flex;
      flex-flow: column wrap;
      width: 100%;
      height: 30vh;
      justify-content: center;
      align-items: center;
      /*background: #333944;*/
    }
    .box{
      width: 100%;
      height: 400px;
      /*background: #CCC;*/
      overflow: hidden;
    }


    .box img{
      width: auto;
      max-width: 100%;
      height: auto;
      max-height: 49vh;
    }
    @supports(object-fit: cover){
        .box img{
          height: 100%;
          object-fit: cover;
          object-position: center center;
        }
    }
    .doc2{
      display: flex;
      flex-flow: column wrap;
      width: 100%;
      height: 50vh;
      justify-content: center;
      align-items: center;
      /*background: #333944;*/
    }
    .box2{
      width: 100%;
      height: 400px;
      /*background: #CCC;*/
      overflow: hidden;
    }


    .box2 img{
      width: auto;
      max-width: 100%;
      height: auto;
      max-height: 49vh;
    }
    @supports(object-fit: cover){
        .box2 img{
          height: 100%;
          object-fit: cover;
          object-position: center center;
        }
    }

</style>



<!-- Modal -->
<div class="modal fade" id="modal_articulo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-6 text-center" style="border-right: 1px solid #ccc;">

              <div class="doc2">
                <button class="" id="btn_galeria_left" disabled="true"><i class="fas fa-angle-left"></i></button>
                <button class="" id="btn_galeria_right"><i class="fas fa-angle-right"></i></button>
                <div class="box2" id="imagenes">
                  <img src="{{asset('/img/articulos/no-images1.jpg')}}" class="card-img-to" id="img_principal">
                </div>
              </div>
              <div class="text-center" id="div_point"></div>
            </div>
            <div class="col-6" id="datos">
              <h3></h3>
              <p class="text-secondary"></p>
              <b class="text-primary"></b>
              <br>
              <label>Más precios</label>
              <select class="form-control col-6"></select>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <label id="categoria" class="text-secondary col-3"></label>
        <label id="marca" class="text-secondary col-3"></label>
        <label id="provedor" class="text-secondary col-3"></label>
      </div>
    </div>
  </div>
</div>


<style type="text/css">

  #btn_galeria_left{
    position: absolute;
    left: 2px;
    top: 40%;
    border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
  }
  #btn_galeria_right{
    position: absolute;
    right: 2px;
    top: 40%;
    border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
  }

  .modal-footer label{
    font-size: 12px;
  }
  .texto_corto{
    max-width: 100%;
    margin-bottom: .4em;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  #accordionExample .card{
    padding: 3px !important;
    margin: 0px !important;
  }#accordionExample .card .card-header{
    padding: 0px !important;
  }
  .card-header{
    border-bottom: 0px !important;
  }
  .card .nav.flex-column > li{
    border-bottom: 0px !important;
  }
  #menu_lateral {
    padding: 0px;
    height:89.5vh;
    background-image: linear-gradient(15deg, #FBFBFB 0%, #F0F8FF  100%);
    }
</style>

@endsection

@section('script')
<script type="text/javascript">


var array_imgs="";
var cont_img=0;

$("#btn_galeria_left").click(function(){
    click_button_left($(this));
});

$("#btn_galeria_right").click(function(){
    click_button_right($(this));
});
function click_button_left(btn){
  if(cont_img-1==0){
    btn.attr("disabled","true");
  }
  $("#btn_galeria_right").removeAttr("disabled");
    cont_img--;
    $("#img_principal").removeAttr("src");
    $("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
    
    $("#div_point i").removeClass("text-primary");
    $("#div_point i").addClass("text-secondary");

    $("#div_point i").each(function(i){
      if(i==cont_img){
        $(this).removeClass("text-secondary");
        $(this).addClass("text-primary");
      }
    });
}
function click_button_right(btn){
  if(cont_img+2==array_imgs.length){
      btn.attr("disabled","true");
    }
    $("#btn_galeria_left").removeAttr("disabled");
    cont_img++;
    $("#img_principal").removeAttr("src");

    $("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
    $("#img_principal").fadeIn('slow','swing');

    $("#div_point i").removeClass("text-primary");
    $("#div_point i").addClass("text-secondary");

    $("#div_point i").each(function(i){
      if(i==cont_img){
        $(this).removeClass("text-secondary");
        $(this).addClass("text-primary");
      }
  });
}







function ver_articulo(id){
  
  $.ajax({
    url:"{{url('/buscar_articulo')}}",
    type:"post",
    dataType:"json",
    data:{id:id},success:function(e){

      $("#titulo").html(e.descripcion_articulo);
      $("#datos h3").html(e.descripcion_articulo);
      $("#datos p").html(e.descripcion_catalogo);
      var p_venta=parseFloat(e.pre_unidad);
      $("#datos b").html("MXN $"+p_venta.toFixed(2));
      $("#datos select").html('');

      if(e.precio_caja==0 || e.precio_caja==null){
        if(e.pre_compra!=null){e.pre_compra;}else{e.pre_compra=0;}
        if(e.pre_unidad!=null){e.pre_unidad;}else{e.pre_unidad=0;}
        if(e.pre_membrecia!=null){e.pre_membrecia;}else{e.pre_membrecia=0;}
        if(e.pre_mayoreo!=null){e.pre_mayoreo;}else{e.pre_mayoreo=0;}
        @if (!Auth::guest())
          @if(Auth::User()->type=='superadmin')
            $("#datos select").append("<option >Compra individual $ "+e.pre_compra+"</option>");
          @endif
        @endif
        $("#datos select").append("<option>Venta individual $"+e.pre_unidad+"</option>");
        $("#datos select").append("<option>Venta x mayoreo  $"+e.pre_mayoreo+"</option>");
        $("#datos select").append("<option>venta x membrecia $"+e.pre_membrecia+"</option>");
      }else{
        if(e.precio_caja!=null){e.precio_caja;}else{e.precio_caja=0;}
        if(e.pre_compra!=null){e.pre_compra;}else{e.pre_compra=0;}
        if(e.pre_unidad!=null){e.pre_unidad;}else{e.pre_unidad=0;}
        if(e.pre_caja!=null){e.pre_caja;}else{e.pre_caja=0;}
        if(e.pre_paquete!=null){e.pre_paquete;}else{e.pre_paquete=0;}
        if(e.pre_mayoreo!=null){e.pre_mayoreo;}else{e.pre_mayoreo=0;}
        @if (!Auth::guest())
        @if(Auth::User()->type=='superadmin')
          $("#datos select").append("<option>Compra X caja $ "+e.precio_caja+"</option>");
          $("#datos select").append("<option >Compra individual $ "+e.pre_compra+"</option>");
        @endif
        @endif
        $("#datos select").append("<option> individual $"+e.pre_unidad+"</option>");
        $("#datos select").append("<option> X caja $"+e.pre_caja+"</option>");
        $("#datos select").append("<option> X paquete $"+e.pre_paquete+"</option>");
        $("#datos select").append("<option> X mayoreo $"+e.pre_mayoreo+"</option>");
      }

  if(e.categoria!=null && e.categoria!=0){e.categoria;}else{e.categoria='';}
  if(e.marca!=null && e.marca!=0){e.marca;}else{e.marca='';}
  if(e.provedor!=null && e.provedor!=0){e.provedor;}else{e.provedor='';}

      $("#categoria").html("Categoria: "+e.categoria);
      $("#marca").html("Marca: "+e.marca);
      $("#provedor").html("Provedor: "+e.provedor);


      array_imgs="";
      cont_img=0;
      $("#div_point").html('');
      var fotos=e.fotos;
      if(fotos==null){
        fotos="ninguno";
      }
      fotos=fotos.split(',');
      array_imgs=fotos;
      if(fotos!="ninguno" && fotos!=''){
        $("#btn_galeria_right").removeAttr("disabled");
        $("#imagenes img").removeAttr("src");
        $("#imagenes img").attr("src","{{asset('/img/articulos/')}}/"+fotos[0]+"");
          for(var a=0;a<fotos.length;a++){
            if(a==0){//agregando puntos el primero coloreado
              $("#div_point").append("<i class='fas fa-dot-circle text-primary' style='font-size:10px;margin:3px'></i>");
            }else{
              $("#div_point").append("<i class='fas fa-dot-circle text-secondary' style='font-size:10px;margin:3px'></i>");
            }
          }
      }else{
        $("#btn_galeria_right,#btn_galeria_left").attr("disabled","true");
        $("#img_principal").removeAttr("src");
        $("#img_principal").attr("src","{{asset('/img/articulos/no-images1.jpg')}}");
      }


      if(fotos.length==1){
       $("#btn_galeria_right,#btn_galeria_left").attr("disabled","true");
      }else if(fotos.length>1){
        $("#btn_galeria_right").removeAttr("disabled");
        $("#btn_galeria_left").attr("disabled","true");
      }
      
    }
  });
  $("#modal_articulo").modal("show");
}


function buscar_opciones(tipo,nombre){
  $("#tipo_busqueda").val(tipo);
  $("#nombre_buscar").val(nombre);
  $("#form_buscador").submit();
}


</script>
@endsection