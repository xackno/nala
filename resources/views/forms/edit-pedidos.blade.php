@extends('layouts.app')
@section('content')
<div class="container" >

@if(session('success'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong><i class="fas fa-check"></i></strong>{{session('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if(session('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong><i class="fas fa-check"></i></strong>{{session('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif








	
	<div id="div_alert"></div>
		@if(isset($msj))
		<div class="alert alert-primary alert-dismissible fade show">
			<h3>{{$msj}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
	
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	
	            <div class="card" >
	            	<div class="card-header" style="background-color:#20c997">
	            		@php
				    				$status=explode("=>",$pedido->status);
				    				$actual=$status[count($status)-1];
				    				$actual=explode("&",$actual);
				    			@endphp
									<h4 class="text-white" style="width: 100%"><b> Pedido:</b>{{ str_pad($pedido->id, 6, '0', STR_PAD_LEFT)}}
										<span class="float-right text-white">Estado: {{$actual[0]}}</span>
									</h4>
	            		
	            	</div>
	            	<div class="card-body">
	            		<form action="{{route('actualizar-pedido')}}" method="POST">
			        		@csrf
	            		<div class="row">
	            			<div class="col-xl-3 col-md-3 mb-3">
	            				<input type="hidden" name="id"  value="{{$pedido->id}}">

	            				<label class="text-danger">CLIENTE</label>
	            				<input type="datetime" name="fecha_pedido" value="{{strtoupper($pedido->nombre_cliente)}}" class="form-control" readonly="">

	            				<label class="text-success">Tipo de precio</label>	            				 
	            				<input type="datetime" name="fecha_enviado" value="{{$pedido->tipo_precio}}" class="form-control" readonly="">

	            				<label>RUTA</label>
	            				<select class="form-control" name="id_ruta" id="id_ruta" >
	            					@foreach($rutas as $r)
	            						<option value="{{$r->id}}" @if($pedido->id_ruta==$r->id) selected="true" @endif>{{$r->nombre_ruta}}</option>
	            					@endforeach
	            				</select>

	            				<label class="text-info">REFERENCIA</label>
	            				<textarea name="referencia" id="referencia" class="form-control" >{{$pedido->referencia}}</textarea>
	            			</div>
	            			<div class="col-xl-2 col-md-2 mb-2 text-right" id="div_precio">
	            			
	            				<label class="text-primary">Subtotal $</label>
	            				<input type="number" name="subtotal" value="{{$pedido->subtotal}}"  class="form-control" readonly>

	            				<label class="text-primary">Costo de envio $</label>
	            				<input type="number" name="envio" value="{{$pedido->costo_envio}}"  class="form-control">

	            				<label class="text-success">Descuento $</label>
	            				<input type="number" name="descuento" value="{{$pedido->descuento}}"  class="form-control">
	            			</div>
	            			<div class="col-xl-3 col-md-3 mb-3">
	    								<label class="text-success">Pago Inicial $</label>
	            				<input type="number" name="pago_inicial" class="form-control" value="{{$pedido->pago_inicial}}">
	            				@php
							    			$cobrar=($pedido->subtotal + $pedido->costo_envio - $pedido->descuento) - $pedido->pago_inicial;
							    			$cobrar=number_format($cobrar,2);
						    			@endphp
	            				<label class="text-danger"><b>POR COBRAR $</b></label>
	            				<input type="number" class="form-control " value="{{$cobrar}}" readonly>
	            			</div>
	            			<div class="col-xl-4 col-md-4 col-lg-4">
	            				@php
						    				$status=explode("=>",$pedido->status);
						    			@endphp
	            				<label><b>HISTORIAL DE ESTATUS</b></label>
	            				<div class="table-responsive">
	            					<table class="table table-striped table-bordered">
	            						<thead>
	            							<tr>
	            								<th>STATUS</th>
	            								<th>FECHA</th>
	            							</tr>
	            						</thead>
	            						<tbody>
	            							@foreach($status as $r)
			            						@php
								    					$actual=explode("&",$r);
								    				@endphp
								    				<tr @if($actual[0]=="pendiente") class="table-danger" @endif
										    				@if($actual[0]=="surtiendo")  style="background:#8E44AD;color:white" @endif
										    				@if($actual[0]=="enviado") class="table-info" @endif
										    				@if($actual[0]=="entregado") class="table-success" @endif
										    			 	@if($actual[0]=="apartado") class="table-warning" @endif
										    			 	@if($actual[0]=="guardado") class="table-secondary" @endif>
								    					<td>{{$actual[0]}}</td>
								    					<td>
								    						@php
								    						$date= date_create_from_format("Y-m-d H:i:s", $actual[1])->format("d-m-Y H:i:s");
								    						@endphp
								    					{{$date}}</td>
								    				</tr>
			            					@endforeach
	            						</tbody>
	            					</table>
	            				</div>
	            					
	            			</div>
	            		</div>
	            		<button type="submit" class="btn btn-primary float-right">Guardar cambios</button>
	            	</form>
	            	<button class="btn btn-success" id="VerProductos"><i class="fa fa-eye"></i> Ver productos</button>
	            		
						




	           		</div>
	        </div> 
	            <!-- fin card -->
	        </div>

	    </div>
	</div>


<style type="text/css">
	.table th, .table td{padding: 2px;margin: 0px;}
	label{
		margin-bottom: 0px;
		padding-bottom:0px;
		margin-top: 5px;
	}
	#div_precio input{text-align: right;}
	.form-control{margin-top: -3px !important;}

	img:hover{
		width: 50px;
	}
</style>


<div class="modal" tabindex="-1" id="modal_pro_pedido" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info text-white">
        <h5 class="modal-title id_modal">Productos de este pedido #{{ str_pad($pedido->id, 6, '0', STR_PAD_LEFT)}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	      <div class="table-responsive" style="overflow-y: scroll;height: 400px">
				  <table class="table table-striped" id="tab_modal_pedido">
				    <thead class="table-dark">
				    	<tr>
				    		<th class="d-none">id</th>
				    		<th>Cantidad</th>
				    		<th>Unidad</th>
				    		<th>Descripción</th>
				    		<th>Precio</th>
				    		<th>total</th>
				    		<th>Foto</th>
				    	</tr>
				    </thead>
				    <tbody id="tb_modal_pedido">
				  		@foreach($productos as $r)
				  			<tr>
				  				<td class="d-none">{{$r->id}}</td>
				  				<td>{{$r->cantidad}}</td>
				  				<td>{{$r->unidad}}</td>
				  				<td>{{$r->descripcion_articulo}}</td>
				  				<td>{{$r->precio}}</td>
				  				<td>{{$r->importe}}</td>
				  				<td>
				  					@php
				  						$fotos=explode(",",$r->fotos);
				  					@endphp
				  					@if($fotos[0]=="ninguno")
				  						<img src="{{asset('public/img/productos/no-images.jpg')}}" width="30px">
				  					@else
				  					<img src="{{asset('public/img/productos/'.$fotos[1])}}" width="30px">
				  					@endif
				  					</td>
				  			</tr>
				  		@endforeach
				  </tbody>
				  </table>
				</div>
      </div>
      <div class="modal-footer" style="background: #efefef">
      	<button class="btn btn-success" disabled="">Guardar cambios</button>
      </div>

    </div>
  </div>
</div>





@endsection
@section('script')
<script type="text/javascript">




$("#VerProductos").click(function(){
	$("#modal_pro_pedido").modal("show");
	// $.ajax({
	// 	url:'{{route("verProdutosPedido")}}',
	// 	type:'POST',
	// 	dataType:'json',
	// 	data:{
	// 		idPedido:'{{$pedido->id}}'
	// 	},success:function(data){
	// 		for(var x=0;x<data.length;x++){
	// 		// alert(data[x].id);
	// 		var foto=data[x].fotos;
	// 		foto=foto.split(',');
	// 		if(foto[0]=="ninguno"){
	// 			foto="no-images.jpg";
	// 		}
	// 		$("#tb_modal_pedido").append('<tr>  <td class="d-none">'+data[x].id
	// 			+' </td> <td>'+data[x].cantidad
	// 			+'</td> <td>'+data[x].unidad
	// 			+'</td><td>'+data[x].descripcion_articulo
	// 			+'</td><td>$'+data[x].precio
	// 			+'</td> <td>'+data[x].importe
	// 			+'</td> <td><img src="public/img/productos/'+foto[0]+'" style="width:50px"></td></tr>');
	// 		}
	// 	},error:function(){

	// 	}

	// });
});

//-------------------------------------------------------
</script>
@endsection