@extends('layouts.app')
@section('content')
<div class="container-fluid" style="margin-top: 10px;">
	<!-- ################################################################################## -->
			@if(session('success'))
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="position: absolute;width: 40%;left:30%;top:250px;z-index: 100;">
		  <strong><i class="fas fa-check"></i></strong>{{session('success')}}
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
			@endif
			@if(session('error'))
			<div class="alert alert-danger alert-dismissible fade show" role="alert" style="position: absolute;width: 40%;left:30%;top:250px;z-index: 100;">
		  <strong><i class="fas fa-exclamation-circle"></i></strong> {{session('error')}}
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
			@endif
			@if(session('msj'))
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
			  <strong><i class="fas fa-check"></i></strong>{{session('msj')}}
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
			@endif
	<!-- ###################################################################### -->
<div class="row">
	<div class="col-9">
		<span class="badge text-white" style="background:#186A3B"><i class="fas fa-circle"></i>Enviado y pagado</span>
		<span class="badge text-white" style="background:#239B56"><i class="fas fa-circle"></i>Pagado X enviar</span>
		<span class="badge text-white" style="background:#2ECC71"><i class="fas fa-circle"></i>Recibido y pagado</span>

		<span class="badge text-white" style="background:#FF3131"><i class="fas fa-circle"></i>Enviado X cobrar</span>
		<span class="badge text-white" style="background:#DE5454"><i class="fas fa-circle"></i> Recibido X pagar</span>
		<span class="badge text-white" style="background:#E39999"><i class="fas fa-circle"></i>Recibido C/ Devolución</span>
		<span class="badge text-white" style="background:#000000"><i class="fas fa-circle"></i>Devolución Total</span>

		<span class="badge" style="background:#F1C40F"><i class="fas fa-circle"></i>Pendiente</span>
		<span class="badge" style="background:#F39C12"><i class="fas fa-circle"></i>Revisión</span>
		<span class="badge" style="background:#E67E22"><i class="fas fa-circle"></i>Surtiendo</span>

		<span class="badge text-white" style="background:#7D3C98"><i class="fas fa-circle"></i>Apartado</span>
		<span class="badge text-white" style="background:#117A65"><i class="fas fa-circle"></i>Guardado</span>
	</div>
	<div class="col-3 float-right text-right">
		<a href="{{url('/nuevo_pedido')}}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Nuevo</a>
		<!-- <a class="btn btn-warning btn-sm " href="{{route('paraPedidos')}}"><i class="fa fa-plus"></i></a> -->
		<a href="{{url('/movimientos')}}" class="btn btn-success btn-sm ">Movimientos</a>
	</div>
</div>
<hr style="margin:2px">

  <div class="row justify-content-center" style="margin:0">
    <div class="col-md-12">
      <div class="card card-general" >
	      <div class="card-body">
	      	<!-- ################################guardados########################################## -->
		      	@if(!$guardados->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-guardado" >
		      			<p>GUARDADOS</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($guardados as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################APARTADOS######################################### -->
		      	@if(!$apartados->isEmpty())
		      	<div class="card card-invidivual" >
		      		<div class="card-header text-center bg-apartado" >
		      			<p>APARTADOS</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="">
		      				<table class="table table-striped table-bordered" >
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th >Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($apartados as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td >{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################SURTIENDO######################################### -->
		      	@if(!$surtiendo->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-surtiendo" >
		      			<p>SURTIENDO</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($surtiendo as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################REVISION######################################### -->
		      	@if(!$revision->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-revision" >
		      			<p>REVISIÓN</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($revision as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################PENDIENTE######################################### -->
		      	@if(!$pendiente->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-pendiente" >
		      			<p>PENDIENTE</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($pendiente as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################DEVOLUCION TOTAL######################################### -->
		      	@if(!$devolucion_total->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-devolucion-total" >
		      			<p>DEVOLUCIÓN TOTAL</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($devolucion_total as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################recibido_con_devolucion################################# -->
		      	@if(!$recibido_con_devolucion->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-recibido-con-devolucion" >
		      			<p>RECIBIDO CON DEVOLUCIÓN</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($recibido_con_devolucion as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################recibido_x_pagar################################# -->
		      	@if(!$recibido_x_pagar->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-recibido-x-pagar" >
		      			<p>RECIBIDO POR PAGAR</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($recibido_x_pagar as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################enviado_x_cobrar################################# -->
		      	@if(!$enviado_x_cobrar->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-enviado-x-cobrar" >
		      			<p>ENVIADO POR COBRAR</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($enviado_x_cobrar as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################recibido_y_pagado################################# -->
		      	@if(!$recibido_y_pagado->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-recibido-y-pagado" >
		      			<p>RECIBIDO Y PAGADO</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($recibido_y_pagado as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################pagado_x_enviar################################# -->
		      	@if(!$pagado_x_enviar->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-pagado-x-enviar" >
		      			<p>PAGADO POR ENVIAR</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($pagado_x_enviar as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->
	      	<!-- ###############################enviado_y_pagado################################# -->
		      	@if(!$enviado_y_pagado->isEmpty())
		      	<div class="card card-invidivual">
		      		<div class="card-header text-center bg-enviado-y-pagado" >
		      			<p>ENVIADO Y PAGADO</p>
		      		</div>
		      		<div class="card-body">
		      			<div class="table-responsive">
		      				<table class="table table-striped table-bordered">
		      					<thead>
		      						<tr>
		      							<th>#</th>
		      							<th>Cliente</th>
		      							<th width="50%">Referencia</th>
		      							<th><i class="fas fa-cog"></i></th>
		      						</tr>
		      					</thead>
		      					<tbody>
		      						@foreach($enviado_y_pagado as $r)
					      				<tr>
					      					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					      					<td title="{{$r->nombre.' '.$r->apellidos}}">{{$r->nombre}}</td>
					      					<td>{{$r->referencia}}</td>
					      					<td><button class="btn btn-sm btn-primary" onclick="verpedido({{$r}});"><i class="fas fa-eye"></i></button></td>
					      					</tr>
					      			@endforeach
		      					</tbody>
		      				</table>
		      			</div>
		      			
		      		</div>
		      	</div>
		      	@endif
	      	<!-- ############################################################################ -->




	      </div><!-- fn card-body -->
      </div><!-- fin card -->
    </div>
  </div>



</div>


<style type="text/css">
	.card-invidivual{
		width:400px;
		display: inline-block;
		overflow: hidden;
		text-overflow: ellipsis;
		 word-spacing: normal;
		 white-space: normal;
		 letter-spacing: normal;
		 margin-bottom: 0px;
	}	
	.card-invidivual table{
		font-size: 8pt;
	}
	.card-general{
		background: #D6DBDF;
		margin:0;text-transform: uppercase;
    overflow: hidden;
  	white-space: nowrap;
  	padding-top: 15px;
  	height:80vh;
	}
	.container-fluid .card-general .card-body{
		overflow-x:scroll;padding: 0px;overflow-y:none
	}
	.container-fluid .card-invidivual .card-body{
		height: 66vh;width:100% ;margin: 0px;
		overflow-x: hidden;
	}
	.container-fluid .card-invidivual .card-header{padding:2px;}
	.container-fluid .card-invidivual .card-header p{margin: 0;}

	.bg-apartado{color: white;background: #7D3C98;}
	.bg-guardado{color: white;background:#117A65 ;}
	.bg-surtiendo{color: white;background: #E67E22;}
	.bg-revision{color: white;background: #F39C12;}
	.bg-pendiente{color: white;background: #F1C40F;}
	.bg-devolucion-total{color: white;background: #000000;}
	.bg-recibido-con-devolucion{color: white;background: #E39999;}
	.bg-recibido-x-pagar{color: white;background: #DE5454;}
	.bg-enviado-x-cobrar{color: white;background: #FF3131;}
	.bg-recibido-y-pagado{color: white;background: #2ECC71;}
	.bg-enviado-y-pagado{color: white;background: #186A3B;}
	.bg-pagado-x-enviar{color: white;background: #239B56;}

	.table th,.table td{
		padding: 3px !important;
	}
	#card_cliente{
		margin:auto;
		border: .5px solid #999;
		border-radius: 20px;
		width: 8.4cm;
		height: 5.25cm;
		background-image: url(img/card.png);
		background-repeat:no-repeat;
	}
	#modal_ver_pedido .modal-dialog{margin: 20px !important;}
	#modal_ver_pedido .modal-xl{margin: 0px !important;}
	#modal_ver_pedido .modal-dialog{max-width: 100% !important;}
</style>

<!-- #####################MODAL VER PEDIDO###################################### -->
	<div class="modal" tabindex="-1" id="modal_ver_pedido" role="dialog">
	  <div class="modal-dialog  " role="document" style="margin:20px !important;padding: 0 !important;">
	  	<div class="row">
	  		<div class="col-4">
	  			<div class="modal-content">
	  				<div class="modal-header bg-primary text-white" style="padding:3px">
	  					<div class="row">
	  						<div class="col-6">
	  							<h5 class="modal-title id_modal">PEDIDO:#<span name="id_pedido"></span></h5>
	  						</div>
	  						<div class="col-6">
	  							<h5 ><span class="badge badge-warning" id="card_status_pedido" style="text-transform: uppercase;font-size: 8pt;float: right;"></span></h5>
	  						</div>
	  					</div>
			        
			      </div>
	  				<div class="card">
	  					<div class="form-group">
	  						<span class="badge badge-success" id="badge_cajero"></span>
								<span class="badge badge-primary" id="badge_caja"></span>
								<span class="badge badge-danger" id="badge_turno"></span>
	  					</div>
	  					
	  					<div class="card-body" id="card_cliente">
	  						<div class="row">
									<div class="col">
										<img class="float-left" src="img/logo1.png" style="width:40px;margin:0;margin-right:5px;border-right: 2px solid #054385;">
										<p class="text-left"  style="margin: 0;margin-left:10px;color:#044890"><b id="card_nombre"></b></p>
										<p class="text-left"  style="font-size:8pt;color:#0078F8" id="card_apellido"></p>
									</div>
									<div class="col">
										<b id="card_id"></b>
										<div id="card_star">

										</div>
									</div>
								</div>
								<div class="row" style="margin-left:10px">
									<div class="col-10 text-justify" style="font-size:8pt">
										<p style="margin:0;"><b>Domicilio:</b></p>
										<span id="card_calle"></span>
										<span id="card_localidad"></span>
										<span id="card_municipio"></span>
										<p class="text-primary" style="margin:0"><i class="fas fa-phone-square fa-2x"></i> <span> <b id="card_telefono"></b></span> </p>
										<p class="text-success" style="margin:0"><i class="fa-2x fas fa-envelope "></i> <span id="card_email"></span></p>
									</div>
								</div>
								
								<div class="row" style="font-size: 10pt;margin-left: 10px;">
									<div class="col">
										<span><b id="card_nivel"></b></span>
									</div>
									<div class="col"></div>
									<div class="col">
										<span class="text-white"><i class="fas fa-credit-card text-danger "></i> $ <b style="text-decoration: underline;" id="card_monedero"></b></span>
									</div>
								</div>
	  					</div>

	  					<hr style="margin:3px;">
	  					<p style="margin:0;padding: 10px;padding-top: 0;padding-bottom: 0;"><b>REFERENCIA:</b> <span name="referencia" style="text-decoration: underline;"></span></p>

	  					<div class="nota" style="padding: 0;padding-left:10px;padding-right:10px;"></div>
	  					<div class="row" style="font-size:10pt">
	  						<div class="col-4">
	  							<span>SUBTOTAL:</span>
	  							<p><b class="text-success" name="subtotal_pedido"></b></p>
	  						</div>
	  						<div class="col-4">
	  							<span>$Envio:</span>
	  							<p><b class="text-danger" name="card_envio"></b></p>
	  						</div>
	  						<div class="col-4">
	  							<span>$Efectivo:</span>
	  							<p><b class="text-primary" name="card_pago_inicial"></b></p>
	  						</div>
	  						<div class="col-4">
	  							<span>%interés:</span>
	  							<p><b class="text-primary" name="card_interes"></b></p>
	  						</div>
	  						<div class="col-4">
	  							<span>Fecha límite:</span>
	  							<p><b class="text-primary" name="card_fecha_limite"></b></p>
	  						</div>
	  						<div class="col-4 col_descuento">
	  							<span>DESCUENTO:</span>
	  							<p class="text-primary"><b></b></p>
	  						</div>
	  					</div>
	  					<div class="roww">
	  						<p style="text-decoration:underline;">TOTAL:$ <b id="modal_porpagar"></b></p>
	  					</div>
	  					<!-- ################################## -->
	  					<div class="row">
	  						<div class="col-4">
	  							<button class="btn btn-success btn-sm" onclick="editar_informacion();"><i class="fas fa-edit"></i></button>
	  							<button class="btn btn-danger btn-sm" id="btn_eliminar" onclick="open_eliminar_pedido();"><i class="fas fa-trash"></i></button>
	  						</div>
	  						<div class="col-2">
	  							
	  							<a href='' id="btn_imprimir" class="btn btn-primary btn-sm" title="Imprimir ticket"><i class="fas fa-print"></i></a>
	  						</div>
	  						<div class="col-6">
	  							<button class="btn btn-primary btn-sm" onclick="open_modal_changestatus();" title="Cambiar de estado"><i class="fas fa-redo-alt"></i> estado</button>
	  						<a class="btn btn-warning btn-sm" href="" id="btn_retomar" ><i class="fas fa-hand-holding"></i> Retomar</a>
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="col-8" >
	  			<div class="card" style="margin-right:0;width: 100% !important;height: 91vh;">
	  				<div class="card-body">
	  						<div class="table-responsive" style="height: 85vh; overflow-y: scroll;">
									<table class="table table-striped table-bordered" id="tabla_articulos_pedidos" >
										<thead class="bg-success">
											<tr>
												<th class="d-none">ID</th>
												<th>CANT</th>
												<th>UNIDAD</th>
												<th>DESCRIPCIÓN</th>
												<th>PRECIO</th>
												<th>$VENDIDO</th>
												<th>SUBT</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	    
	  </div>
	</div>

<!-- ###########################modal change status############################# -->
	<div class="modal" tabindex="-1" id="modal_change_status" role="dialog">
	  <div class="modal-dialog " role="document">
	    <div class="modal-content">
	      <div class="modal-header bg-primary text-white" style="padding:3px">
	        <h5 class="modal-title id_modal">Cambiar estado de pedido</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body text-center">

	  				<select name="status" id="select_status" class="form-control" style="width: 50%;margin:auto;">
	  					<option value="guardado">Guardado</option>
	  					<option value="apartado">Apartado</option>
	  					<option value="surtiendo">Surtiendo</option>
	  					<option value="revision">Revisión</option>
	  					<option value="pendiente">Pendiente</option>
	  					<option value="devolucion_total">Devolución Total</option>
	  					<option value="recibido_con_devolucion">Recibido C/ Devolución</option>
	  					<option value="recibido_x_pagar">Recibido X pagar</option>
	  					<option value="enviado_x_cobrar">Enviado X cobrar</option>
	  					<option value="recibido_y_pagado">Recibido y pagado</option>
	  					<option value="pagado_x_enviar">Pagado X enviar</option>
	  					<option value="enviado_y_pagado">Enviado y pagado</option>
	  				</select>

	  				<br>
	  				<hr>
	  				<button class="btn btn-primary" onclick="cambiar_status();">Cambiar</button>
	      </div>
	    </div>
	  </div>
	</div>

<!-- ###########################Eliminar pedido############################# -->
	<div class="modal" tabindex="-1" id="modal_eliminar_pedido" role="dialog">
	  <div class="modal-dialog " role="document">
	    <div class="modal-content">
	      <div class="modal-header bg-danger text-white" style="padding:3px">
	        <h5 class="modal-title id_modal">Eliminar pedido</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body text-center">
	      		<i class="fas fa-exclamation-triangle text-warning fa-3x"></i>
	      			<h3>Esta a punto de eliminar este pedido.</h3>
	      			<h4>¿Realmente desea eliminarlo?</h4>
	      		<br>
	  				<hr>
	  				<button class="btn btn-secondary" onclick="submit_delete_pedido();" >Eliminar</button>
	  				<button  class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
	      </div>
	    </div>
	  </div>
	</div>
<!-- ###########################Editar datos############################# -->
	<div class="modal" tabindex="-1" id="modal_editar_pedido" role="dialog">
	  <div class="modal-dialog " role="document">
	    <div class="modal-content">
	      <div class="modal-header bg-success text-white" style="padding:3px">
	        <h5 class="modal-title id_modal">Editar información</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body text-center">
	      		<form action="{{url('/editar_info_pedido')}}" method="post">
	      			@csrf
	      			<input type="hidden" name="id_pedido">
	      			<label>Referencia</label>
	      			<textarea class="form-control" name="referencia"></textarea>
	      			<label>Nota</label>
	      			<textarea name="nota" class="form-control"></textarea>
	      			<hr>
	      			<div class="row">
	      				<div class="col">
	      					<label>envio</label>
	      					<input type="number" step="any" name="costo_envio" class="form-control">
	      				</div>
	      				<div class="col">
	      					<label>Pago inicial</label>
	      					<input type="number" step="any" name="pago_inicial" class="form-control">
	      				</div>
	      			</div>
	      			<br>
	      				<button type="submit" class="float-right btn btn-sm btn-primary"><i class="fas fa-save"></i>Guardar</button>
	      		</form>	
	  			
	      </div>
	    </div>
	  </div>
	</div>


@endsection

@section('script')
<script type="text/javascript">

var id_pedido=0;
var pedido;

function editar_informacion(){
	$("#modal_editar_pedido").modal("show");
	$("#modal_editar_pedido [name=id_pedido]").val(pedido.id);
	$("#modal_editar_pedido [name=referencia]").val(pedido.referencia);
	$("#modal_editar_pedido [name=nota]").val(pedido.nota);
	$("#modal_editar_pedido [name=costo_envio]").val(pedido.costo_envio);
	$("#modal_editar_pedido [name=pago_inicial]").val(pedido.pago_inicial);
	// alert(pedido.nota);
}


function open_eliminar_pedido(){
	$("#modal_eliminar_pedido").modal("show");
	$("#modal_ver_pedido").modal("hide");
}

function submit_delete_pedido(){
	$.ajax({
		url:"{{url('/eliminar_pedido')}}",
		type:"post",
		dataType:"json",
		data:{id:id_pedido},
		success:function(e){
			if (e=="success") {
				alert("Pedido  #: "+("00000000" + id_pedido).substr(-8,8)+" fue elimido correctamente.");
				location.reload();
			}
		},error:function(){
			alert("No fue posible eliminar este pedido.");
		}
	});
}

function open_modal_changestatus(){
	$("#modal_change_status").modal("show");
}
function cambiar_status(){
	var status=$("#select_status").val();
	$.ajax({
		url:"{{url('/change_status')}}",
		type:"post",
		dataType:"json",
		data:{
			id:id_pedido,
			status:status
		},success:function(e){
			$("#modal_change_status").modal("hide");
			$("#modal_ver_pedido").modal("hide");
			alert("Cambio de estado el pedido #:"+("00000000" + id_pedido).substr(-8,8));
			location.reload();
		},error:function(){
			alert("No fue posible cambiar el Estado del pedido.");
		}
	});
}



function verpedido(p){
	id_pedido=p.id;
	pedido=p;


	$(".nota,[name=card_envio],[name=card_pago_inicial],#badge_cajero,#badge_caja,#badge_turno,[name=subtotal_pedido],#card_status_pedido,[name=card_interes],[name=card_fecha_limite]").html('');
	$("#modal_ver_pedido").modal("show");
	$("[name=id_pedido]").html(("00000000" + p.id).substr(-8,8));
	$("#btn_retomar").attr("href","{{url('/retomar')}}/"+p.id);
	$("#btn_imprimir").attr("href","{{url('/imprimir_ticket')}}/"+p.id);
	buscar_cliente(p.id_cliente);//cargando card del cliente
	$("#card_status_pedido").html(p.status);//status en header
	$("[name=referencia]").html(p.referencia);
	if(p.nota!=null){
		$(".nota").html('<div class="alert alert-warning" style="padding:0"><b>NOTA:</b>'+p.nota+'</div>');
	}
	if(p.status=="guardado" || p.status=="apartado" || p.status=="surtiendo" || p.status=="revision" || p.status=="pendiente" || p.status=="devolucion_total"){
		$("#btn_eliminar,#btn_retomar").show();
	}else{
		$("#btn_eliminar,#btn_retomar").hide();
	}

	$("[name=subtotal_pedido]").html("$ "+p.subtotal_pedido);
	$("[name=card_envio]").html("$ "+p.costo_envio);
	$("[name=card_pago_inicial]").html("$ "+p.pago_inicial);
	$("#badge_cajero").html("CAJERO:"+p.cajero);
	$("#badge_caja").html("CAJA:"+p.caja);
	$("#badge_turno").html("TURNO:"+p.turno);
	$("[name=card_interes]").html(p.interes+"%");
	$("[name=card_fecha_limite]").html(p.fecha_limite);
	var descuento=0;
	if(p.descuento=="true"){
		descuento=20;
		$(".col_descuento").show();
		$(".col_descuento b").html("SI ($20.00)");
	}else{
		descuento=0;
		$(".col_descuento").hide();
	}
	var porpagar=(parseFloat(p.subtotal_pedido)+parseFloat(p.costo_envio));
	porpagar=porpagar-parseFloat(p.pago_inicial);
	porpagar=porpagar-descuento;
	$("#modal_porpagar").html(porpagar.toFixed(2));

	cargar_datos_tabla(p);
}

function cargar_datos_tabla(p){
	$("#tabla_articulos_pedidos tbody").html("");
	var id_art=p.id_articulos;           		id_art=id_art.split(",");
	var cantidad=p.cantidad;             		cantidad=cantidad.split(",");
	var unidad=p.unidad;             		unidad=unidad.split(",");
	var descripcion=p.descripcion;             		descripcion=descripcion.split(",");
	var precio_vendido=p.precio_vendido; 		precio_vendido=precio_vendido.split(",");
	var tipo_precio=p.tipo_precio_vendido; 	tipo_precio=tipo_precio.split(",");
	var sub=p.subtotal_articulos;						sub=sub.split(",");
			for(var x=0;x<id_art.length;x++){
				$("#tabla_articulos_pedidos tbody").append('<tr><td class="d-none">'+id_art[x]+'</td><td>'+cantidad[x]+'</td><td>'+unidad[x]+'</td><td>'+descripcion[x]+'</td>'+
				'<td>'+precio_vendido[x]+'</td><td>'+tipo_precio[x]+'</td><td>'+sub[x]+'</td></tr>');
			}

	
	



}

	function buscar_cliente(id){
		$.ajax({
			url:"{{url('/buscar_clientes_x_id')}}",
			type:"post",
			dataType:"json",
			data:{
				buscador:id
			},success:function(e){
				cargar_card(e[0]);
			},error:function(){
				alert("No fue posible buscar el cliente.");
			}
		});
	}


	function cargar_card(e){

		 $("#card_nombre").html(e.nombre);
		$("#card_apellido").html(e.apellidos);
		$("#card_id").html("ID:"+("00000000" + e.id).substr(-8,8));
		// $("#card_nombre").html(e.nombre);
		$("#card_star").html('');
		for(var x=0;x<5;x++){
				if(x<e.calificacion){
					$("#card_star").append('<i class="fas fa-star text-warning"></i>');
				}else{
					$("#card_star").append('<i class="fas fa-star text-secondary"></i>');
				}
			}///fin for
			$("#card_calle").html(e.calle+", ");
			$("#card_localidad").html(e.localidad+", ");
			$("#card_municipio").html(e.municipio);
			$("#card_telefono").html(e.telefono);
			$("#card_email").html(e.correo);
			$("#card_nivel").html(e.nivel);
			// var monedero;
			if(e.monedero!=null){monedero=e.monedero}else{monedero="0.00"}
			$("#card_monedero").html(monedero);
			$("#card").removeClass();

			$("#select_cliente").modal("hide");
			$("[name=busqueda]").focus();
	}



















</script>
@endsection