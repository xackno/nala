@extends('layouts.app')
@section('content')
<div class="container-fluid" style="padding-top:5px;overflow-y:none">
	<div class="row">
		<div class="col col-md-4 col-lg-4 col-xl-4">
				<span class="badge badge-success">CAJERO: {{Auth::user()->name}}</span>
				<span class="badge badge-primary">CAJA: {{$caja}}</span>
				<span class="badge badge-danger">TURNO: {{$turno}}</span>


			<div class="input-group">
				<input type="text" name="buscar_cliente" placeholder="cliente" class="form-control" >
				<button class="btn btn-primary" onclick="buscar_cliente();"><i class="fas fa-search" ></i></button>
				</div>
				<div class="row text-center">
					<div id="card" style="text-transform:uppercase;" class="d-none">
						<div class="row">
							<div class="col">
								<img class="float-left" src="img/logo1.png" style="width:40px;margin:0;margin-right:5px;border-right: 2px solid #054385;">
								<p class="text-left"  style="margin: 0;margin-left:10px;color:#044890"><b id="card_nombre">FELICIANO</b></p>
								<p class="text-left"  style="font-size:8pt;color:#0078F8" id="card_apellido">LORENZO CRUZ</p>
							</div>
							<div class="col">
								<b id="card_id">ID:00000001</b>
								<div id="card_star">

								</div>
							</div>
						</div>
						<div class="row" style="margin-left:10px">
							<div class="col-10 text-justify" style="font-size:8pt">
								<p style="margin:0;"><b>Domicilio:</b></p>
								<span id="card_calle">Conocido, </span>
								<span id="card_localidad"> Santiago Naranjas,</span>
								<span id="card_municipio">Juxtlahuaca.</span>
								<p class="text-primary" style="margin:0"><i class="fas fa-phone-square fa-2x"></i> <span> <b id="card_telefono"> +52 953 275 6220</b></span> </p>
								<p class="text-success" style="margin:0"><i class="fa-2x fas fa-envelope "></i> <span id="card_email">xackno1995@gmail.com</span></p>
							</div>
						</div>
						<br>
						<div class="row" style="font-size: 10pt;margin-left: 10px;">
							<div class="col">
								<span><b id="card_nivel">común</b></span>
							</div>
							<div class="col"></div>
							<div class="col">
								<span class="text-white"><i class="fas fa-credit-card text-danger "></i> $ <b style="text-decoration: underline;" id="card_monedero">10000.00</b></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
						<textarea name="referencia" style="height: 80px;width: 100% !important;" placeholder="Referencia del lugar"></textarea>
					<div class="row" style="width:100%;">
						<div class="col">
						<label>$envio</label>
					</div>
					<div class="col">
						<label>Efectivo</label>
					</div>
					<div class="col">
						<label>%interés</label>
					</div>
					</div>

					<div class="row">
						<div class="col">
							<input type="number" step="any" name="envio" class="form-control" placeholder="0.00">
						</div>
						<div class="col">
							<input type="number"  step="any" name="pago" class="form-control" placeholder="0.00">
						</div>
						<div class="col">
							<input type="number" name="interes" class="form-control" value="2">
						</div>
					</div>
					<div class="row">
						<div class="col">
								<label>fecha limite</label>
								<input type="date" name="fecha_limite" class="form-control" value="{{date('Y-m-d')}}">
						</div>
					</div>

					<textarea name="nota" style="height: 80px;width: 100% !important;margin-top:15px" placeholder="DETALLES, NOTA, COMENTARIOS."></textarea>
					
				</div>

		</div>
		<div class="col col-sm-8 col-lg-8 col-xl-8">

			<div id="alerta">
				
			</div>
			<div class="row" id="div_tabla">
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="tabla_venta">
						<thead class="bg-success">
							<tr>
								<th class="d-none">ID</th>
								<th>CANT</th>
								<th>UNID</th>
								<th>DESCRIP</th>
								<th>PRECIO</th>
								<th>$VENDIDO</th>
								<th>SUBT</th>
								<th><i class="fas fa-cog"></i></th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<div class="row" id="div_footer">
					<div class="col-3">
							<div class="input-group">
								<input type="text" name="busqueda"  class="form-control" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus >
								<button class="btn btn-primary" onclick="buscarPro();"><i class="fas fa-search"></i></button>
							</div>
					</div>
					<div class="col-5">
						<button class="btn btn-warning" onclick="abrir_modal_generar_pedido();">GENERAR PEDIDO (F8)</button>
					</div>
					<div class="col-4">
						<h5 class="text-warning" style="text-shadow:0px 2px 0px red;"><b>TOTAL MXN: $<span id="totalpagar">0.00</span></b></h5>
					</div>

			</div>
			
		</div>
	</div>

</div>

<style type="text/css">
	#alerta{position: absolute;top: 35%;right: 30%;}
	#tabla_venta td{padding: 3px;height: 38px;font-family: 'Arial Black';}
	#card{
		margin:auto;
		border: .5px solid #999;
		border-radius: 20px;
		width: 8.4cm;
		height: 5.25cm;
		background-image: url(img/card.png);
		background-repeat:no-repeat;
	}

	.table td,.table th{padding: 0px !important;}
	#div_tabla{
		overflow-y: scroll;
		height: 80vh;
	}
	#div_footer{
		background: #138D75;
		padding: 4px;
		height: 10.5vh;
	}
</style>

<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-primary" style="padding:3px">
          <h4 id="nivel_cliente">Articulos encontrados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table-responsive" id="table_responsive_busq" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th class="d-none">Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th style="width: 40%">Descripción</th>
                      <th>Precios</th>
                      <th>Precio</th>
                      <th>Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                    <td></td>
                  </tr>
               </tbody>
           </table>
        </div>
        </div>
      </div>
    </div>
  </div>

<!-- ###########################################modal seleccionar cliente####################### -->
<div class="modal fade" id="select_cliente" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top:160px;margin-left:5px">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Clientes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered" id="clientes_coincidencia" style="text-transform:uppercase">
        	<thead class="bg-primary">
        		<tr>
        			<th class="d-none">ID</th>
        			<th>NOMBRE</th>
        			<th>DOMICILIO</th>
        			<th><i class="fas fa-cog"></i></th>
        		</tr>
        	</thead>
        	<tbody>
        	</tbody>
        </table>
      </div>

    </div>
  </div>
</div>
<!-- ###############################modal generar pedido########################### -->
<div class="modal fade" id="modal_general_pedido" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">PEDIDO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table table-striped table-bordered">
      		<tbody>
      			<tr><td>Subtotal de pedido</td><td id="total_pedido"></td></tr>
      			<tr><td>Costo de envio</td><td id="total_envio"></td></tr>
      			<tr><td>Efectivo o pago inicial</td><td id="total_pago"></td></tr>
      			<tr id="tr_aplicar_descuento"><td>Aplicar descuento de $20.00 </td><td class="text-center"><input type="checkbox" onclick="agregar_descuento();" id="check_descuento"></td></tr>
      			<tr><td class="text-primary">TOTAL A PAGAR</td><td><b class="text-danger" id="total_a_pagar"></b></td></tr>
      		</tbody>
      	</table>
      		<br>
      		<div class="row text-center">
      			<div class="col"></div>
      			<div class="col">
      				<button  class="btn btn-primary" onclick="submit_pedido();" >GUARDAR</button>
      			</div>
      			<div class="col"></div>
      			
      		</div>
      		
      </div>

    </div>
  </div>
</div>




@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/for-pedidos.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){
        cargar_tr();
    });
var id_cliente;
var monedero;
var totalpedido;
var envio;
var pago;
var descuento;




function submit_pedido(){
	var id_articulos=[];
  var cantidad=[];
  var unidad=[];
  var descripcion=[];
  var tipo_precio=[];
  var precio_vendido=[];
  var subtotalPro=[];//inporte de productos
  var contEach=0;
  var total=0;
  $("#tabla_venta tbody").find("tr td:first-child").each(function(){
    if($(this).siblings("td").eq(4).html()!=""){
        id_articulos.push($(this).html());
        cantidad.push($(this).siblings("td").eq(0).html());
        unidad.push($(this).siblings("td").eq(1).html());
        descripcion.push($(this).siblings("td").eq(2).html());
        tipo_precio.push($(this).siblings("td").eq(3).find("select option:selected").data('tipo'));
        precio_vendido.push($(this).siblings("td").eq(4).html());
        subtotalPro.push($(this).siblings("td").eq(5).html());
        total+=parseFloat($(this).siblings("td").eq(5).html());
      }
   });
  

  $.ajax({
  	url:"{{url('/generando_pedido')}}",
  	type:"post",
  	dataType:"json",
  	data:{
  		caja:"{{$caja}}",
      turno:"{{$turno}}",
      cajero:"{{Auth::user()->name}}",
      id_cliente:id_cliente,
      referencia:$("[name=referencia]").val(),
      interes:$("[name=interes]").val(),
      fecha_limite:$("[name=fecha_limite]").val(),
      nota:$("[name=nota]").val(),

      subtotal_pedido:total,
      envio:envio,
      pago:pago,
      descuento:descuento,

      id_articulos:id_articulos,
      cantidad:cantidad,
      unidad:unidad,
      descripcion:descripcion,
      tipo_precio:tipo_precio,
      total_articulos:subtotalPro,
      precio_vendido:precio_vendido,
  	},success:function(e){
  		console.log(e[0]+ " " +e[1]);
  		if(e[0]=="success"){
  			$("#alerta").html('<div class="alert alert-success"><i class="fas fa-check-circle"></i>El pedido #'+e[1]+' fue registrado correctamente. </div>');
  			setInterval(function(){
  				$("#alerta").html('');
  				location.reload();
  			},5000);
  			limpiar_pantalla();
  		}else{
  			$("#alerta").html('<div class="alert alert-danger"><i class="fas fa-check-circle"></i> Error al registrar el pedido, verifique la información proporcionada.</div>');
  			setInterval(function(){
  				$("#alerta").html('');
  			},5000);

  		}
  	},error:function(){
  		alert("Error en el servidor, consute con su administrador");
  	}
  });


}

function limpiar_pantalla(){
	$("#modal_general_pedido").modal("hide");
}




function agregar_descuento(){

		if ($("#check_descuento").is(":checked")) {
			totalpedido=totalpedido-20;
			descuento=true;
		}else{
			totalpedido=totalpedido+20;
			descuento=false;
		}
		calcutar_total();
}



function calcutar_total(){
	var total_a_pagar=(totalpedido+envio)-pago;
	total_a_pagar=total_a_pagar.toFixed(2);
	$("#total_a_pagar").html(total_a_pagar);
}

function abrir_modal_generar_pedido(){
	totalpedido,envio,pago=0;descuento=false;
	$("#check_descuento").prop("checked", false);

	totalpedido=$("#totalpagar").html();
	$("#total_pedido").html(totalpedido);
	//------------------------------------
	envio=$("[name=envio]").val();
	envio=parseFloat(envio);
	if (isNaN(envio)) envio = 0;
	$("#total_envio").html(envio.toFixed(2));
	//-------------------------------------
	pago=$("[name=pago]").val();
	pago=parseFloat(pago);
	if (isNaN(pago)) pago = 0;
	$("#total_pago").html(pago.toFixed(2));
	//-------------------------------------

	totalpedido=parseFloat(totalpedido);	
	if (id_cliente!=null) {//id_cliente!=null
		if(totalpedido!=0){//totalpagar!=0
			$("#modal_general_pedido").modal("show");
		}else{
			alert("No hay articulos en la tabla.");
		}
	}else{
		alert("Es necesario seleccionar el cliente.");
	}
	//------------------------------
	if(totalpedido>500 && monedero>20){
		$("#tr_aplicar_descuento").show();
	}else{
		$("#tr_aplicar_descuento").hide();
	}
	calcutar_total();
	
}



 //################FUNCION DE BUSCAR PRODUCTO############################
      function buscarPro(){
        limpiartablabusqueda();
        var producto=$("#findProducto").val();
          if(producto.length>=0){
            $('#modal_busqueda').modal('show');
              $.ajax({
                url:'{{route("buscarExistencia")}}',
                type:'post',
                dataType:'json',
                data:{
                  buscar:producto
                },success:function(e){
                   if (e=="") {
                     $("#tabla_busqueda tbody").append("<tr  class='text-danger'> <td  colspan='6'><b>No se encontró registro!!</b></td></tr>");
                    }else{
                      for(var x=0;x<e.length;x++){
                      var precio=generar_select_precios(e[x].precio_caja,e[x].pre_unidad,e[x].pre_mayoreo,e[x].pre_caja,e[x].pre_paquete,e[x].pre_membrecia,e[x].a_granel,e[x].mayoreo_apartir);
                      var pre_a_vender=e[x].pre_unidad;

                      $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td width='1px' class='d-none'>"+e[x].id+
                        "</td>   <td>"+e[x].clave+
                        "</td>   <td>"+e[x].unidad+
                        "</td>   <td>"+e[x].descripcion_articulo+
                        "</td>   <td>"+precio+
                        "</td>   <td class='text-right'>$"+pre_a_vender+
                        "</td>   <td class='text-right'>"+e[x].existencia_stock+
                        "</td>  </tr>");
                     }//fin for
                    }
                },error:function(){
                  alert("Hubo un problema al buscar este producto");
                }
              });  
          }
                       
      }
 //####################funcion buscar por codigo de barras#############
    function buscar_x_codigo(codigo){

      $.ajax({
        url:"{{url('/buscar_x_codigo')}}",
        type:"post",
        dataType:"json",
        data:{codigo:codigo},
        success:function(e){
          if(e.length==0){
              alert("No se encontro el articulo.");
              $("#findProducto").val('');
              $("#findProducto").focus();
            }else{
              if (ya_existe(e[0].id)==0) {
                var precios=generar_select_precios(e[0].precio_caja,e[0].pre_unidad,e[0].pre_mayoreo,e[0].pre_caja,e[0].pre_paquete,e[0].pre_membrecia,e[0].a_granel,e[0].mayoreo_apartir);

                append_tabla_venta(e[0].id,e[0].unidad,e[0].descripcion_articulo,precios,e[0].pre_unidad,e[0].pre_unidad,e[0].a_granel);
                $("#findProducto").val('');
                $("#findProducto").focus();
              }
            }  
        },error:function(){
          alert("No se encontro el articulo.");
          $("#findProducto").val('');
          $("#findProducto").focus();
        }
      });
    }
//#########################################################################
function cargar_tr(){
        for(var x=0;x<12;x++){
            $("#tabla_venta tbody").append("<tr><td class='d-none'></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    }
function eliminar_ultimo_tr_vacio(){
        var ultimo=$("#tabla_venta").find("tr:last");
        var valor=ultimo.find("td").eq(0).html();
        if (valor=='') {
            $("#tabla_venta").find("tr:last").remove();
        }
    }

function generar_select_precios(precio_caja,pre_unidad,pre_mayoreo,pre_caja,pre_paquete,pre_membrecia,a_granel,mayoreo_apartir){
  var precio_caja_if=precio_caja;

  precio_caja=parseFloat(precio_caja);
    precio_caja = precio_caja ? precio_caja : 0;
  pre_unidad=parseFloat(pre_unidad);
    pre_unidad = pre_unidad ? pre_unidad : 0;
  pre_mayoreo=parseFloat(pre_mayoreo);
    pre_mayoreo = pre_mayoreo ? pre_mayoreo : 0;
  pre_caja=parseFloat(pre_caja);
    pre_caja = pre_caja ? pre_caja : 0;
  pre_paquete=parseFloat(pre_paquete);
    pre_paquete = pre_paquete ? pre_paquete : 0;
  pre_membrecia=parseFloat(pre_membrecia);
    pre_membrecia = pre_membrecia ? pre_membrecia : 0;

  var precio="";


  if(precio_caja_if==0 || precio_caja_if==null){
    precio="<select class='form-control'>"+
    "<option value='"+pre_unidad+"' selected='true' data-tipo='unidad'>Venta</option>"+
    "<option value='"+pre_mayoreo+"' data-tipo='mayoreo'>Mayoreo >="+mayoreo_apartir+"</option>"+
    "<option value='"+pre_membrecia+"' data-tipo='membrecia'>Membrecia</option>"+
    "</select><span class='d-none'>"+a_granel+"</span><p class='d-none'>"+mayoreo_apartir+"</p><small class='d-none'>"+precio_caja+"</small>";
  }else{
    precio="<select class='form-control'>"+
    "<option value='"+pre_unidad+"' selected='true' data-tipo='unidad'>Venta</option>"+
    "<option value='"+pre_caja+"' data-tipo='caja'>Venta x caja</option>"+
    "<option value='"+pre_paquete+"' data-tipo='paquete'>Venta x paquete</option>"+
    "<option value='"+pre_mayoreo+"' data-tipo='mayoreo'>Venta x mayoreo >="+mayoreo_apartir+"</option>"+
    "<option value='"+pre_membrecia+"' data-tipo='membrecia'>venta con membrecia</option>"+
  "</select><span class='d-none'>"+a_granel+"</span><p class='d-none'>"+mayoreo_apartir+"</p><small class='d-none'>"+precio_caja+"</small>";
  }
  return precio;
}






///##################################################buscando clientes################################
$("[name=buscar_cliente]").keyup(function(e){
	if(e.keyCode==13){
		buscar_cliente();
	}
});

	function buscar_cliente(){
		$("#card").addClass("d-none");

		id_cliente=null;
		monedero=null;
		$.ajax({
			url:"{{url('/buscar_cliente')}}",
			type:"post",
			dataType:"json",
			data:{
				buscador:$("[name=buscar_cliente]").val()
			},success:function(e){
				if(e.length==1){
					cargar_card(e[0]);
				}else{
					$("#clientes_coincidencia tbody").html('');
					$("#select_cliente").modal("show");
					for(var x=0;x<e.length;x++){
						var data=JSON.stringify(e[x]);
						$("#clientes_coincidencia tbody").append("<tr>  td class='d-none'>"+e[x].id+"</td><td>"+e[x].nombre+"</td><td>"+e[x].calle+", "+e[x].localidad+", "+e[x].municipio+"</td><td><button class='btn btn-sm btn-success'            onclick='cargar_card("+data+");'  ><i class='fas fa-mouse-pointer'></i></button></td> </tr>");
					}
				}

			},error:function(){
				alert("No fue posible buscar el cliente.");
			}
		});
	}


	function cargar_card(e){
		id_cliente=e.id;
		monedero=e.monedero;
		 $("#card_nombre").html(e.nombre);
		$("#card_apellido").html(e.apellidos);
		$("#card_id").html("ID:"+("00000000" + e.id).substr(-8,8));
		// $("#card_nombre").html(e.nombre);
		$("#card_star").html('');
		for(var x=0;x<5;x++){
				if(x<e.calificacion){
					$("#card_star").append('<i class="fas fa-star text-warning"></i>');
				}else{
					$("#card_star").append('<i class="fas fa-star text-secondary"></i>');
				}
			}///fin for
			$("#card_calle").html(e.calle+", ");
			$("#card_localidad").html(e.localidad+", ");
			$("#card_municipio").html(e.municipio);
			$("#card_telefono").html(e.telefono);
			$("#card_email").html(e.correo);
			$("#card_nivel").html(e.nivel);
			// var monedero;
			if(e.monedero!=null){monedero=e.monedero}else{monedero="0.00"}
			$("#card_monedero").html(monedero);
			$("#card").removeClass();

			$("#select_cliente").modal("hide");
			$("[name=busqueda]").focus();
	}





//############################################################################
//##########################focus para tb-busqueda##########################################  






</script>

@endsection