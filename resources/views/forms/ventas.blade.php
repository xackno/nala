<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RETAIL 2</title>
    <link rel="shortcut icon" href="{{asset('img/logo1.png')}}">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
</head>

<body class="container-fluid">


<div class="row">
    <div class="col-0 col-md-1 col-xl-1 col-lg-1 d-none d-sm-none d-md-block text-center">
        <a  class="btn btn-primary " href="{{url('/home')}}"><i class="fas fa-arrow-left"></i> <i class="fas fa-home"></i> INICIO </a>
        <hr>
        <b>FECHA:</b>
        <p>{{$fecha}} {{date('Y')}}</p>
        <b>HORA:</b>
        <p id="hora_footer"></p>
        <b>CAJERO:</b>
        <p>{{Auth::user()->name}}</p>
        <b>TURNO:</b>
        <p @if($turno==0) class="bg-danger text-white" @endif>
          @if($turno!=0)
          <span>T{{ str_pad($turno, 6, '0', STR_PAD_LEFT)}}</span>
          <input type="hidden" name="turno" id="id_turno" value="{{$turno}}">
          @else
            <span>Iniciar TURNO</span>
          @endif
        </p>
        <b>CAJA:</b>
        <p> @if(isset($caja)){{($caja)}} @else 0 @endif</p>
        <!-- ##################variable para saver si es para pedido##################### -->
        @if(isset($msj))
          <p id="msj" class="d-none">{{$msj}}</p>
      @endif
        <div id="msj_retomando"></div>
      <p>ID cliente: </p><p id="id_cliente_footer" class="d-non">0</p>

      <input type="number" id="mod_cant" class="form-control" placeholder="cant">
    </div>
    <!-- ########################################TABLA VENTA############################################################### -->
    <div class="col-12 col-md-11 col-xl-11 col-lg-11">
        <div class="tabla">
            <div style="width: 97.5%; position:absolute;">
               <table class="table table-bordered" >
                <thead class="bg-primary">
                    <th class="th_1 d-none">#</th>
                    <th class="th_2">CANTIDAD</th>
                    <th class="th_3">UNIDAD</th>
                    <th class="th_4">ARTICULO</th>
                    <th class="th_5">PRECIOS</th>
                    <th class="th_5">PRECIO VENDIDO</th>
                    <th class="th_6">SUBTOTAL</th>
                    <th class="th_7 text-center"><button class="btn btnprimary"><i class="fas fa-cog"></i></button></th>
                </thead>
            </table> 
            </div>
            <div style="overflow-y: scroll;height: 89.9vh; width: 100%" id="div_scroll_tabla_venta">
                <table class="table table-striped table-bordered">
                    <thead class="d-non" style="margin-right:-20px;">
                        <th class="th_1 d-none">#</th>
                        <th class="th_2">CANTIDAD</th>
                        <th class="th_3">UNIDAD</th>
                        <th class="th_4">ARTICULO</th>
                        <th class="th_5">PRECIOS</th>
                        <th class="th_5">PRECIO VENDIDO</th>
                        <th class="th_6">SUBTOTAL</th>
                        <th class="th_7"><button class="btn btnprimary"><i class="fas fa-cog"></i></button></th>
                    </thead>
                    <tbody id="tabla_venta">
                        <tr  class="d-none"><td class="d-none"></td><td id="td_focus"></td><td></td><td>NO ELIMINAR</td><td></td><td></td><td></td></tr>
                    </tbody>
                </table> 
        </div><!-- End -->

        <div id="alertas"></div>

      <div  id="footer" class="@if(Request::path()=='paraPedidos') bg-danger @elseif(Request::path()=='retomar_pedido') bg-secondary @else bg-primary @endif  ">
            <div class="row" style="padding: 5px;">
              <div class="col col-md-4">
                <div class="input-group">
                  <input type="text" name="busqueda"  class="form-control" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus="true" style="color:black;font-family: 'arial black'">
                  <button class="btn btn-sm btn-success" onclick="buscarPro();"><i class="fas fa-search fa-2x" ></i></button>
                </div>
              </div>
              <div class="col col-md-3">
                @if(Request::path()=="ventas")
                <button class="btn btn-sm btn-warning" id="btn_f9"><i class="fas fa-shopping-basket text-dark fa-2x"></i></button>
                @endif
                 <button class="btn btn-sm btn-warning" id="btn_f8"><i class="fas fa-power-off text-dark fa-2x"></i></button>
                 <button class="btn btn-sm btn-info" id="btn_f4"><i class="fas fa-sign-out-alt fa-2x text-dark"></i></button>
                 <button class="btn btn-sm btn-info" id="btn_devolucion"><i class=" fas fa-hand-point-left fa-2x text-dark"></i></button>
                 @if(Request::path()=="paraPedidos" || Request::path()=="retomar_pedido")
                  <button class="btn btn-sm btn-secondary" id="f2">f2</button>
                  <button class="btn btn-sm btn-success" id="f8">F8</button>
                 @endif
              </div>
              <div class="col col-md-5">
                <button class="float-left btn btn-sm btn-danger" data-toggle="modal" data-target="#modal_ultima_venta" >Último venta</button>
                <div class="float-right">
                  <h4 class="text-white" style="margin:0"><span>TOTAL:$</span> <span id="totalpagar">00.00</span> MXN</h4>
                  <b >Último cambio: <span class="text-warning" id="ultimo_cambio"></span></b>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
</body>
<style type="text/css">
    .btn-sm{font-size: 10px;}
    #tabla_venta{overflow-y:scroll;}
    #tabla_venta td{padding: 3px;height: 33px;font-family: 'Arial Black';}
    /*Ancho de las columas de la tabla venta*/
    .th_2{width: 10%;padding: 3px !important;height: 20px;}
    .th_3{width: 10%;padding: 3px !important;height: 20px;}
    .th_4{width: 40%;padding: 3px !important;height: 20px;}
    .th_5{width: 15%;padding: 3px !important;height: 20px;}
    .th_6{width: 10%;padding: 3px !important;height: 20px;}
    .th_7{width: 10%;padding: 3px !important;height: 20px;}

    #alertas{position: absolute;width: 30%; top: 50px;left: 30%}
    
    #footer{height: 10vh;}
    body {
        font-family: 'Comfortaa', serif;
        /*font-size: 16px;*/
    } 
    #tabla_busqueda tbody td,#tabla_busqueda tbody th{
      padding: 0px !important;
    }
    #tabla_busqueda tbody td select{
      padding: 0px !important;
      height: 30px !important;
    }
</style>


<!-- #################################modales###################################3 -->
<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-primary" style="padding:3px">
          <h4 id="nivel_cliente">Articulos encontrados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table-responsive" id="table_responsive_busq" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th class="d-none">Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th style="width: 40%">Descripción</th>
                      <th>Precios</th>
                      <th>Precio</th>
                      <th>Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                    <td></td>
                  </tr>
               </tbody>
           </table>
        </div>
        </div>
      </div>
    </div>
  </div>
<!-- $$$$$$$$$$$$$$$modal cobrar###################### -->
  <div class="modal fade" id="modal_cobrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Realizar venta
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col text-center">
              <h1 class="text-success">TOTAL:$</h1>
              <h1 id="text-total" class="text-danger">00.00</h1>
            </div>
            <div class="col text-center">
              EFECTIVO:<i class="fas fa-dollar-sign"></i>
              <input type="number" id="efectivo" class="form-control border border-primary">
            </div>
          </div><br>
          <div class="text-center">
            <h3 class="text-primary">Cambio:$<span id="cambio" class="text-danger">00.00</span></h3>
          </div>

        </div>
        <div class="card-footer">
          <!-- <button class="btn btn-success btn-sm float-left" id="btn_a_credito" style="margin-right: 4px"><i class="fas fa-credit-card"></i> Credito</button> -->

          <form  action="{{url('/credito')}}" class="float-left" id="form_credito" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="credito_id_p">
            <input type="hidden" name="credito_cantidad">
            <input type="hidden" name="credito_unidad">
            <input type="hidden" name="credito_descripcion">
            <input type="hidden" name="credito_precio">
            <input type="hidden" name="credito_subtotal">
            <input type="hidden" name="turno" value="{{$turno}}">
            <button class="btn btn-success btn-sm float-left " disabled="true"  id="btn_credito"><i class="fas fa-credit-card"></i> Crédito</button>
          </form>

          <form  action="{{url('/cotizacion')}}" class="float-left" id="form_cotizacion" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="id_p">
            <input type="hidden" name="cantidad">
            <input type="hidden" name="unidad">
            <input type="hidden" name="descripcion">
            <input type="hidden" name="precio">
            <input type="hidden" name="subtotal">
            <button class="btn btn-secondary btn-sm float-left" id="btn_cotizacion"><i class="fas fa-table"></i> Cotización</button>
          </form>

          <button class="btn btn-primary btn-sm float-right" onclick="ejecutar_venta();">Realizar <i class=" fas fa-angle-right"></i></button>

        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->
<!--window modal ######modal turno################-->
  <div class="modal fullscreen-modal fade" id="modal_turno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header @if($turno==0) bg-primary @else bg-info @endif" style="width: 100%;">
          <h5 class="text-white" style="margin:0px" id="header_turno">Turno</h5> 
          <span style="margin-left: 40%" class="text-warning">@if($turno!=0) T{{ str_pad($turno, 6, '0', STR_PAD_LEFT)}} @endif</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <!-- #############################from para abrir turno -->
          <form  id="form-store-turno">
            @csrf
            <p>Usuario:<b style="text-decoration: underline;">{{Auth::user()->name}}</b> <span class="float-right">{{$fecha}}</span></p>
                
            <input type="hidden" name="caja" value="{{$caja}}">
            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="abierto">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="inicio" class="form-control border-secondary" required="" placeholder="0.00" readonly>
            <input type="hidden" name="conteo_inicio">
            <hr>
            <div id="conteo_inicio">
              <label>Billetes</label>
              <div class="row">
                <div class="col">
                    <i>$1000</i>
                    <input type="number" name="b1000" class="form-control">
                </div>
                <div class="col">
                    <i>$500</i>
                    <input type="number" name="b500" class="form-control">
                </div>
                <div class="col">
                    <i>$200</i>
                    <input type="number" name="b200" class="form-control">
                </div>
                <div class="col">
                    <i>$100</i>
                    <input type="number" name="b100" class="form-control">
                </div>
                <div class="col">
                    <i>$50</i>
                    <input type="number" name="b50" class="form-control">
                </div>
                <div class="col">
                    <i>$20</i>
                    <input type="number" name="b20" class="form-control">
                </div>
              </div>
              <label>Monedas</label>
              <div class="row">
                <div class="col">
                  <i>$20</i>
                  <input type="number" name="m20" class="form-control">
                </div>
                <div class="col">
                  <i>$10</i>
                  <input type="number" name="m10" class="form-control">
                </div>
                <div class="col">
                  <i>$5</i>
                  <input type="number" name="m5" class="form-control">
                </div>
                <div class="col">
                  <i>$2</i>
                  <input type="number" name="m2" class="form-control">
                </div>
                <div class="col">
                  <i>$1</i>
                  <input type="number" name="m1" class="form-control">
                </div>
                <div class="col">
                  <i>$.50</i>
                  <input type="number" name="m_50" class="form-control">
                </div>
                <div class="col">
                  <i>Dolar</i>
                  <input type="number" name="dollar" class="form-control">
                </div>
                <div class="col">
                  <i>Otros</i>
                  <input type="number" name="otros" class="form-control">
                </div>
              </div>
            </div>
            <hr>
            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario1" ></textarea>
            <br><br>


            <button id="btn_submit_store_turno" type="button"  class="btn btn-primary btn-sm float-right">Iniciar <i class="fas fa-power-off"></i></button>
          </form>
          <!-- #####################from para cerrar turno ###################################-->
            <form action="{{url('/cerrarturno')}}" method="post" id="form-cerrar-turno">
              @csrf
              <div class="row">
                <div class="col">
                  <p>Usuario:<b style="text-decoration: underline;">{{Auth::user()->name}}</b></p>
                </div>
                <div class="col">
                  @if(isset($info_turno[0]))
                  @php
                  $inicio=$info_turno[0]->created_at;
                  $only_hours=explode(" ",$inicio);
                  @endphp
                  inicio:{{$only_hours[1]}}
                  @endif
                </div>
                <div class="col">
                  <p id="hora_modal_cerrar_turno"></p>
                </div>
              </div>
              <hr>

              <input type="hidden" name="id" @if(isset($turno)) value="{{$turno}}" @endif >
              <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
              <input type="hidden" name="status" value="cerrado">
              <div class="row">
                <div class="col">
                  <label><i class="fas fa-dollar-sign"></i>En caja según el sistema</label>
              <input type="number" name="cierre_system" class="form-control border-secondary" step="any" required="" placeholder="0.00" readonly>

                </div>
                <div class="col">
                  <label><i class="fas fa-dollar-sign"></i>En caja después del conteo </label>
                  <input type="number" step="any" name="cierre_conteo" class="form-control" placeholder="0.00" readonly>
                </div>
              </div>
              <!-- ####################conteo_final############ -->
              <input type="hidden" name="conteo_final" >
              <hr>
              <div id="conteo_fin">
                <label>Billetes</label>
                <div class="row">
                  <div class="col">
                      <i>$1000</i>
                      <input type="number" name="b1000" class="form-control">
                  </div>
                  <div class="col">
                      <i>$500</i>
                      <input type="number" name="b500" class="form-control">
                  </div>
                  <div class="col">
                      <i>$200</i>
                      <input type="number" name="b200" class="form-control">
                  </div>
                  <div class="col">
                      <i>$100</i>
                      <input type="number" name="b100" class="form-control">
                  </div>
                  <div class="col">
                      <i>$50</i>
                      <input type="number" name="b50" class="form-control">
                  </div>
                  <div class="col">
                      <i>$20</i>
                      <input type="number" name="b20" class="form-control">
                  </div>
                </div>
                <label>Monedas</label>
                <div class="row">
                  <div class="col">
                    <i>$20</i>
                    <input type="number" name="m20" class="form-control">
                  </div>
                  <div class="col">
                    <i>$10</i>
                    <input type="number" name="m10" class="form-control">
                  </div>
                  <div class="col">
                    <i>$5</i>
                    <input type="number" name="m5" class="form-control">
                  </div>
                  <div class="col">
                    <i>$2</i>
                    <input type="number" name="m2" class="form-control">
                  </div>
                  <div class="col">
                    <i>$1</i>
                    <input type="number" name="m1" class="form-control">
                  </div>
                  <div class="col">
                    <i>$.50</i>
                    <input type="number" name="m_50" class="form-control">
                  </div>
                  <div class="col">
                    <i>Dolar</i>
                    <input type="number" name="dollar" class="form-control">
                  </div>
                  <div class="col">
                    <i>Otros</i>
                    <input type="number" name="otros" class="form-control">
                  </div>
                </div>
              </div>
              
              

              <label><i class="fas fa-comment-dots"></i> Comentario</label>
              <textarea class="form-control border-secondary" name="comentario2" ></textarea>



              <br><br>
              <button id="btn_submit_cerrar_turno" type="submit" class="btn btn-danger btn-sm float-right">Cerrar <i class="fas fa-power-off"></i></button>
            </form>
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal salidas################-->
  <div class="modal fullscreen-modal fade" id="modal_salidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px">SALIDAS</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="padding-top: 1px">
          <div id="alertasmodal_salidas"></div>
          <div class="input-group">
            <button class="btn btn-primary btn-sm" id="toggle-registrar">Registrar</button>
            <button class="btn btn-primary btn-sm" id="toggle-ver">VER</button>
          </div>
          <div id="registrar_salidas">
              <form id="form_save_salidas">
                <div class="row">
                  <div class="col-5">
                    <span>Usuario: {{Auth::user()->name}}</span>
                  </div>

                  <div class="col-5">
                    @if(isset($turno))
                      <span class="text-danger" style="font-family: 'Arial Black';padding: 0">Turno: {{$turno}} Caja: {{$caja}}</span>
                      @else
                      <span class="text-warning" style="font-family: 'Arial Black'">Iniciar TURNO</span>
                    @endif
                  </div>

                  <div class="col-2">
                    <p><span class="float-right">{{$fecha}}</span></p>
                  </div>
                </div>

                <input type="hidden" name="turno" @if(isset($turno)) value="{{$turno}}" @endif >
                <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
                <input type="hidden" name="caja" value="{{$caja}}">
                <label>Concepto</label>
                <input type="text" name="concepto" class="form-control border-secondary" required="">

                <label><i class="fas fa-dollar-sign"></i> Cantidad</label>
                <input type="number" name="cantidad" id="cantidad_registro_salida" class="form-control border-secondary" placeholder="0.00">
                </form>
          </div>
          <div id="listar_salidas">
            <div class="table_responsive">
              <table class="table table-bordered table-striped" id="tabla_registros_salidas">
                <thead class="table-dark text-center">
                  <tr>
                    <th class="d-none">#</th>
                    <th>concepto</th>
                    <th>cantidad</th>
                    <th><i class="fas fa-cogs"></i></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
              <form class="input-group" id="form_edit_salida">
                <input type="hidden" name="edit_id">
                <input type="text" name="edit_concepto" class="form-control">
                <input type="number" name="edit_cantidad" class="form-control">
                <button type="button" onclick="guardar_edit_salida();" class="btn btn-primary">Guardar <i class="fas fa-save"></i></button>

              </form>
            </div>
          </div>



        </div>
        <div class="modal-footer">
          <button onclick="registrar_salidas();" id="btn-registrar-salida" class="btn btn-primary btn-sm float-right">Registrar</button>
        </div>

      </div>
    </div>
  </div>
<!-- $$$$$$$$$$$$$$$modal devolucion###################### -->
  <div class="modal fade" id="modal_devolucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Devolución para la  venta <span id="id_venta"></span>
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="alert_in_devoluciones"></div>

          <div id="div_info" style="width: 100%">

          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tabla_prod_devolucion">
            <thead class="table-success">
              <tr>
                <th class="d-none">id</th>
                <th>cantidad a devolver</th>
                <th>unidad</th>
                <th>descripcion</th>
                <th>precio</th>
                <th>subtotal</th>
                <th>a devolver</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          </div>
          <h3 class="float-right text-primary">Total de venta: $<span id="total_venta"></span></h3>
          <br><br>
          <h3 class="float-right text-danger">Total a devolver: $<span id="total_devolver"></span></h3>
          <br><br><br><br>
          <button class="btn btn-primary float-right" onclick="guardar_devolucion();">Guardar devolución</button>
          <br>
          <br>
          <div class="table bg-info rounded">
            <p class="text-white"><span class="text-warning">NOTA:</span> Verificar que la mercancia no este dañada antes de aceptar la devolución.</p>
          </div>
        </div>

      </div>
    </div>
  </div>
<!-- fin modal devolucion -->



<!--window modal ######modal realizar pedidos################-->
  <div class="modal fullscreen-modal fade" id="modal_pedidos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <h5 class="text-white"><i class="fa fa-clipboard"></i> Realizar pedido</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="div_alert_excede"></div>
          <div class="alert alert-warning d-none" style="padding:0px" >
            <p>SUGERENCIA:<br> Despúes de cada $500.00 de compra aplica descuento del <b>Monedero.</b></p>
          </div> 
          <div id="div_alert_pedidos" style="width: 100%;margin:5px"></div>

          <div class="row">
              <div class="col-xl-3 col-md-6 mb-4">
                <label><b>Pedido para el cliente con:</b></label>
                <label >Nombre</label>
                <input type="text" id="nombreParaPedido" class="form-control" readonly="">
                <label>Localidad</label>
                <input type="text" id="localidadParaPedido" class="form-control" readonly="">

                <label>Monedero <b class="text-danger">$</b></label>
                <input type="number" id="monedero" class="form-control text-success" readonly="">
                <label>Limite<b class="text-danger">$</b></label>
                <input type="number" id="limite" class="form-control text-success" readonly="">
              </div>

              <div class="col-xl-5 col-md-6 mb-4">
                  <label>Referencia del lugar</label>
                <textarea id="descripcion_lugar" style="height: 80px;width:100%" required="" >Sin referencia</textarea>
                <br>
                <div class="row">
                  <div class="col">
                    <label>Costo de envio</label>
                    <input type="number" id="costo_envio" class="form-control text-right" required="" value="0">
                  </div>
                  <div class="col">
                    <label>PAGO o pago inicial</label>
                    <input type="number" id="pago_inicial" class="form-control text-right" required="" value="0">
                  </div>
                </div>
                <!-- <label>RUTA <i class="text-success fas fa-route"></i> </label> -->
                @if(isset($rutas))
                    <!-- <select name="id_ruta" id="id_ruta" class="form-control" required=""> -->
                <!--             <option selected="" disabled="">Seleccione una opción</option> -->
                        @foreach($rutas as $ruta)
                        <!-- <option value="{{$ruta->id}}">{{$ruta->nombre_ruta}}</option> -->
                        @endforeach
                      <!-- </select> -->
                @endif
              </div>

              <div class="col-xl-3 col-md-6 mb-4">

                <h6 class="float-right" style="margin:0px"> <span class="text-success">IMPORTE:$</span> <span id="subtotal" class="text-danger"></span></h6><br>

                <h6 class="float-right" style="margin:0px"> <span class="text-info">Envio:$</span> <span id="span_envio" class="text-danger"></span></h6>
                <br>
                <h6 class="float-right" style="margin:0px"> <span class="text-primary">PAGO o pago inicial:$</span> <span id="span_pago_inicial" class="text-danger"></span></h6>

                <h6 class="float-right" style="margin: 0px"> <span class="text-success">SUBTOTAL:$</span> <span id="totalPedido" class="text-danger"></span></h6>


                <!-- ////aqui esta el checkbox -->
                <div class="form-check" id="div_check">
                  <input type="checkbox" class="form-check-input " id="exampleCheck1" value="20">
                  <label class="form-check-label" for="exampleCheck1" style=" font-size: 70%">
                    <b>Aplicar descuento 
                      <span class="text-primary">$20.00</span>
                    </b>
                  </label>
                </div>
                <hr><br>
                <div class="text-center">
                    <h4 class="text-danger">POR  PAGAR:$ <span class="text-danger" style="font-size:40px" id="total_a_pagar">0.00</span> </h4>
                </div>
                
              </div>
          </div>
       
        </div><!-- fin modal-body -->
        <div class="modal-footer" style="background:#efefef">
          <button class="btn btn-danger" onclick="cancelar_pedido();" >Cancelar</button>
          <button class="btn btn-info" id="btn_apartar" onclick="guardarPedido(3);" >apartar</button>
          <button class="btn btn-primary" id="btn_solo_guardar" onclick="guardarPedido(1);">Sólo guardar</button>
          <button class="btn btn-success float-right" id="btn_enviar_pedido" onclick="guardarPedido(2);">Enviar</button>
          
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal ver imgs################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
        <div class="modal-header text-dark" style="background-color: #ffc107;" >
          <h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal mensaje modalselectCliente################-->
  <div class="modal fullscreen-modal fade" id="modalSelectCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info text-white">
          <h4>Seleccione un cliente</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div id="div_para_alert" style="width: 100%;margin:5px"></div>
         <label><i class="fa fa-user"></i> Buscar cliente</label>
         <div class="input-group col-xl-4 col-md-6 mb-4">  
          <input type="text" class="form-control bg-light border-1 small" placeholder="Buscar..." autocomplete="off" id="findClient" autofocus="true"
            aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
          <button class="btn btn-primary" id="btn_findClient" onclick="buscarClientes(1);">
              <i class="fas fa-search fa-sm"></i>
            </button>
          </div><!-- fin nput-group -->

          <div class="row">
              <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Selecciones uno</label>
              <select class="form-control" id="econtrados"  >
              </select>
              <input type="number" id="input_id_cliente" class="d-none">
              <input type="text" id="input_nivel_cliente" class="d-none">
            </div>
            <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Nombre</label>
              <input type="text" id="nombreClinte" class="form-control" readonly="">

              <label>Localidad</label>
              <input type="text" id="localidad2" class="form-control" readonly="">

              <label>Calificación</label>
              <p id="stars"></p>
            </div>

            <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Teléfono <i class="fas fa-phone-square text-danger"></i></label>
              <input type="text" id="telefono" class="form-control " readonly="">

              <label>En monedero <i class="fas fa-money-check-alt text-success"></i></label>
              <input type="text" id="monedero_al_buscar_cliente" class="form-control text-danger" readonly="">

              <label>Nivel</label>
              <input type="text" id="nivel" class="form-control" readonly="">
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
              <div style="width:100%;margin:20px">
                <img src="{{asset('/img/clientes/'.'no-images.jpg')}}" id="fotoCliente" style="width: 70%;border:2px solid #aeaeff;border-radius: 40px" >
              </div>
            </div>
    
          </div>
          
        </div><!-- fin modal-body -->
        <div class="modal-footer">
          <button class="btn btn-success float-right" id="btn_selectionClient">Selecionar</button>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal ultima venta################-->
  <div class="modal fullscreen-modal fade" id="modal_ultima_venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
        <div class="modal-header text-dark" style="background-color: #ffc107;padding: 5px;" >
          <p style="margin: 0;"> Ultima venta #<span id="id_ultima_venta"></span></p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col text-primary"><span id="turno"></span></div>
            <div class="col text-success"><span id="usuario"></span></div>
            <div class="col text-danger"> <span id="fecha"></span></div>
          </div>
          <div class="table-responsive">
            <table class="table table-striped table-bordered text-center" id="table_articulos">
              <thead class="bg-primary text-center text-white">
                <tr>
                  <th>cantidad</th>
                  <th>unidad</th>
                  <th>descripción</th>
                  <th>Tipo precio</th>
                  <th>precio</th>
                  <th>subtotal</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
          <div>
            <button class="btn btn-success float-left" id="btn_reimprimir">Reimprimir ticket</button>
            <h2 id="total" class="float-right text-primary"></h2>
          </div>
        </div>
      </div>
    </div>
  </div>


</html>

<script type="text/javascript" src="{{asset('js/jquery.3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/for-ventas.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/for-creditos.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

    $(document).ready(function(){
        cargar_tr();
    });

    last_venta();

  ///##################################obteniendo datos del ultimo venta######################
    var id_ultimo_venta=0;
    function last_venta(){
      $.ajax({
        url:"{{url('/last_venta')}}",
        type:"post",
        dataType:"json",
        data:{caja:'{{$caja}}'},
        success:function(e){
          var efectivo=e.efectivo;
          efectivo=parseFloat(efectivo);
          var total_venta=e.total_venta;
          total_venta=parseFloat(total_venta);
          var cambio=efectivo-total_venta;
          cambio=cambio.toFixed(2);
          $("#ultimo_cambio").html(cambio);
          $("#id_ultima_venta").html(e.idcero);
          verarticulos(e.id);
          id_ultimo_venta=e.id;
          // console.log(e);
        }
      });
    }

    function verarticulos(id){
      $("#id_venta_header").html("VENTA: "+  ('00000000' + id).slice(-8));
      $("#table_articulos tbody").html('');
      $.ajax({
        url:"{{url('/buscar_venta')}}",
        type:"post",
        dataType:"json",
        data:{id_venta:id},
        success:function(e){
          $("#turno").html("Turno: T"+ ('000000' + e["venta"].id_turno).slice(-6));
          $("#usuario").html("Atendió: "+e["venta"].usuario);
          $("#fecha").html("Fecha: "+e["fecha"]);
          $("#total").html("Total: $"+e["venta"].total_venta);
          $("#modal_ver_articulos").modal("show");

          for(var x=0;x<e["nombres"].length;x++){
            var cantidad=e['cantidad'][x];
            var precio=e['precio'][x];
            precio=parseFloat(precio);
            precio=precio.toFixed(2);
            var total=parseFloat(cantidad)*parseFloat(precio);
            total=total.toFixed(2);

            $("#table_articulos tbody").append("<tr> <td>"+cantidad+"</td>   <td>"+e['unidad'][x]+"</td>   <td>"+e["nombres"][x]+"</td>  <td>"+e["tipo_precio"][x]+"</td><td>$ "+precio+"</td>  <td>$ "+total+"</td>   </tr>");
          }

        },error:function(){

        }
      });
    }
    //###############################reimpresion#############################
    $("#btn_reimprimir").click(function(){
      reimprimir(id_ultimo_venta);
    });
    function reimprimir(id){
        $.ajax({
          url:"{{url('/reimprimir')}}",
          type:"post",
          dataType:"json",
          data:{id:id},
          success:function(e){
            if(e==true){
              alert("Se reimprimió correctamente.");
              $("#modal_ultima_venta").modal("hide");
            }
          }
        });
    }



//##############################conteo al cerrar turno################

  $("#btn_f8").click(function() {
      @if($turno!=0)
        calcular_total_venta({{$turno}});
      @endif
    });
  $("#conteo_inicio input").keyup(function(){
    conteo_caja_inicio();
  });
  $("#conteo_fin input").keyup(function(){
    conteo_caja_cierre();
  });

  function conteo_caja_inicio(){
    var total_conteo=0;
    if($("#conteo_inicio [name=b1000]").val()!=''){
      total_conteo+=1000*parseInt($("#conteo_inicio [name=b1000]").val());
    }
    if($("#conteo_inicio [name=b500]").val()!=''){
      total_conteo+=500*parseInt($("#conteo_inicio [name=b500]").val());
    }
    if($("#conteo_inicio [name=b200]").val()!=''){
      total_conteo+=200*parseInt($("#conteo_inicio [name=b200]").val());
    }
    if($("#conteo_inicio [name=b100]").val()!=''){
      total_conteo+=100*parseInt($("#conteo_inicio [name=b100]").val());
    }
    if($("#conteo_inicio [name=b50]").val()!=''){
      total_conteo+=50*parseInt($("#conteo_inicio [name=b50]").val());
    }
    if($("#conteo_inicio [name=b20]").val()!=''){
      total_conteo+=20*parseInt($("#conteo_inicio [name=b20]").val());
    }
    //####################monedas#################################
    if($("#conteo_inicio [name=m20]").val()!=''){
      total_conteo+=20*parseInt($("#conteo_inicio [name=m20]").val());
    }
    if($("#conteo_inicio [name=m10]").val()!=''){
      total_conteo+=10*parseInt($("#conteo_inicio [name=m10]").val());
    }
    if($("#conteo_inicio [name=m5]").val()!=''){
      total_conteo+=5*parseInt($("#conteo_inicio [name=m5]").val());
    }
    if($("#conteo_inicio [name=m2]").val()!=''){
      total_conteo+=2*parseInt($("#conteo_inicio [name=m2]").val());
    }
    if($("#conteo_inicio [name=m1]").val()!=''){
      total_conteo+=1*parseInt($("#conteo_inicio [name=m1]").val());
    }
    if($("#conteo_inicio [name=m_50]").val()!=''){
      total_conteo+=.50*parseInt($("#conteo_inicio [name=m_50]").val());
    }
    total_conteo=parseFloat(total_conteo);
    total_conteo=total_conteo.toFixed(2);
    $("[name=inicio]").val(total_conteo);
    conteo_inicio="";
    conteo_inicio=$("#conteo_inicio [name=b1000]").val()+","+$("#conteo_inicio [name=b500]").val()+
    ","+$("#conteo_inicio [name=b200]").val()+","+$("#conteo_inicio [name=b100]").val()+","+$("#conteo_inicio [name=b50]").val()+","+$("#conteo_inicio [name=b20]").val()+"-"+

    $("#conteo_inicio [name=m20]").val()+","+$("#conteo_inicio [name=m10]").val()+","+$("#conteo_inicio [name=m5]").val()+","+$("#conteo_inicio [name=m2]").val()+","+
    $("#conteo_inicio [name=m1]").val()+","+$("#conteo_inicio [name=m_50]").val()+","+
    $("#conteo_inicio [name=dollar]").val()+","+$("#conteo_inicio [name=otros]").val();
    ;
    $("[name=conteo_inicio]").val(conteo_inicio);
  }
  function conteo_caja_cierre(){
    var total_conteo=0;
    if($("#conteo_fin [name=b1000]").val()!=''){
      total_conteo+=1000*parseInt($("#conteo_fin [name=b1000]").val());
    }
    if($("#conteo_fin [name=b500]").val()!=''){
      total_conteo+=500*parseInt($("#conteo_fin [name=b500]").val());
    }
    if($("#conteo_fin [name=b200]").val()!=''){
      total_conteo+=200*parseInt($("#conteo_fin [name=b200]").val());
    }
    if($("#conteo_fin [name=b100]").val()!=''){
      total_conteo+=100*parseInt($("#conteo_fin [name=b100]").val());
    }
    if($("#conteo_fin [name=b50]").val()!=''){
      total_conteo+=50*parseInt($("#conteo_fin [name=b50]").val());
    }
    if($("#conteo_fin [name=b20]").val()!=''){
      total_conteo+=20*parseInt($("#conteo_fin [name=b20]").val());
    }
    //####################monedas#################################
    if($("#conteo_fin [name=m20]").val()!=''){
      total_conteo+=20*parseInt($("#conteo_fin [name=m20]").val());
    }
    if($("#conteo_fin [name=m10]").val()!=''){
      total_conteo+=10*parseInt($("#conteo_fin [name=m10]").val());
    }
    if($("#conteo_fin [name=m5]").val()!=''){
      total_conteo+=5*parseInt($("#conteo_fin [name=m5]").val());
    }
    if($("#conteo_fin [name=m2]").val()!=''){
      total_conteo+=2*parseInt($("#conteo_fin [name=m2]").val());
    }
    if($("#conteo_fin [name=m1]").val()!=''){
      total_conteo+=1*parseInt($("#conteo_fin [name=m1]").val());
    }
    if($("#conteo_fin [name=m_50]").val()!=''){
      total_conteo+=.50*parseInt($("#conteo_fin [name=m_50]").val());
    }
    total_conteo=parseFloat(total_conteo);
    total_conteo=total_conteo.toFixed(2);
    $("[name=cierre_conteo]").val(total_conteo);

    conteo_final="";
    conteo_final=$("#conteo_fin [name=b1000]").val()+","+$("#conteo_fin [name=b500]").val()+
    ","+$("#conteo_fin [name=b200]").val()+","+$("#conteo_fin [name=b100]").val()+","+$("#conteo_fin [name=b50]").val()+","+$("#conteo_fin [name=b20]").val()+"-"+

    $("#conteo_fin [name=m20]").val()+","+$("#conteo_fin [name=m10]").val()+","+$("#conteo_fin [name=m5]").val()+","+$("#conteo_fin [name=m2]").val()+","+
    $("#conteo_fin [name=m1]").val()+","+$("#conteo_fin [name=m_50]").val()+","+
    $("#conteo_fin [name=dollar]").val()+","+$("#conteo_fin [name=otros]").val();
    ;
    $("[name=conteo_final]").val(conteo_final);

  }


//###########################PARA EDITAR CANTIDAD CON CONTROL+C##############################
  $("#findProducto").keydown(function(e){//###############para ctrl+c  editar cantidad
    if( e.ctrlKey && e.keyCode ==67){ //ctrl=17, c=67
      $("#mod_cant").focus();
      $("#mod_cant").val('');
    }
  });

  $("#mod_cant").keyup(function(e){
    var nueva_cantidad=$(this).val();
    nueva_cantidad=parseFloat(nueva_cantidad);
    if( e.keyCode==13){ 
      $("#findProducto").focus();
      var p_en_tabla=0;
      $("#tabla_venta").find("tr td:first-child").each(function(e){
        if ($(this).siblings("td").eq(4).html()!="") {
          p_en_tabla++;
          }
      });
      if(p_en_tabla>0){
        $("#tabla_venta").find("tr td:first-child").each(function(x){
            if(x==0){
                $(this).siblings("td").eq(0).html(nueva_cantidad);
                var precio=$(this).siblings("td").eq(4).html();
                precio=parseFloat(precio);
                var subtotal=nueva_cantidad*precio;
                subtotal=subtotal.toFixed(2);
                $(this).siblings("td").eq(5).html(subtotal);//subtotal
                // alert(precio);
                calcularTotal();
              }
           });
      }else{
        alert("¡¡¡NO HAY PRODUCTOS EN LA TABLA!!!");
      }

      
    }
  });
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PARA PEDIDOS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    //-------------------teclas de funciones-------------
        $("body").keyup(function(e) {
            if (e.keyCode == 120) { //F9
               @if(Request::path()=="ventas")
                if (validar_turno() != 0) {
                    var cont2=0;
                    $("#tabla_venta").find("tr td:first-child").each(function(){
                      if ($(this).siblings("td").eq(4).html()!="") {
                          cont2++;//comprobando si hay productos en la tabla
                        }
                      });
                    if (cont2>0) {//abrir modal solo si hay productos en la tabla de venta
                        $("#modal_cobrar").modal("show");
                        $("#text-total").html($("#totalpagar").html());
                    }else{
                        alert("¡¡¡NO HAY PRODUCTOS EN LA TABLA!!!");
                    }
                } else {
                    alert("INICIAR TURNO");
                }
                @endif
            }

            if (e.keyCode == 119) {      //F8  pedidos
              @if(Request::path()=="paraPedidos" || Request::path()=="retomar_pedido")
                mostrarModal_pedidos();  
              @endif
              }
            if(e.keyCode == 113) {      //F2  pedidos
              $("#modalSelectCliente").modal("show"); 
              limpiarIformacionCliente(); 
            }//fin if keycode 113

        });








     $("#f8").hide(); //mantener oculto siempre el f8.
    //###################variables e id clientes###############
        var idCliente=0;
        var nivelCliente="a_publico";

      @if(isset($id_pedido))
          retomar_pedido();
          idCliente={{$pedido->id_cliente}};
          nivelCliente='{{$pedido->tipo_precio}}';
          $("#msj_retomando").html('<div class="alert alert-success" style="padding:2px">retomando<br>PEDIDO<br>#{{ str_pad($id_pedido, 8, '0', STR_PAD_LEFT)}}</div>');
        @endif
      function retomar_pedido(){
        @if(isset($prod_pedidos))
        @foreach($prod_pedidos as $r)
          //p1,p2,p3,p4,p5
          var select=mostrar_4_precio({{$r->precio_venta}},{{$r->mayoreo_1}},{{$r->mayoreo_2}},{{$r->mayoreo_3}},{{$r->mayoreo_4}});

            $("#tabla_venta").append("<tr tabindex='0' class='move'><td class='d-none'>"+{{$r->id_producto}}+"</td><td contenteditable='true' class='text-center'>"+{{$r->cantidad}}+"</td>  <td class=''>"+'{{$r->unidad}}'+"</td> <td><b class='text-primary'>"+"{{$r->descripcion}}"+"</b> </td> <td>"+select+"</td> <td>"+{{$r->importe}}+"</td><td class='text-center'><button class='btn btn-sm btn-danger' onclick='eliminarPRO();' ><i class='fas fa-trash' ></i></button></td></tr>");
            eliminar_ultimo_tr_vacio();
          @endforeach

          $("#f8").show();
          $("#btn_f9").hide();
          // $("#f2").hide();
          calcularTotal();

          $("#id_cliente_footer").html({{$pedido->id_cliente}});

          $("#costo_envio").val({{$pedido->costo_envio}});
          $("#pago_inicial").val({{$pedido->pago_inicial}});
          $("#span_envio").html({{$pedido->costo_envio}});
          $("#span_pago_inicial").html({{$pedido->pago_inicial}});
          $("#descripcion_lugar").text('{{$pedido->referencia}}');
        @endif
      }
    //#################para click en el checkbox de aplicar descuento
        $("#exampleCheck1").click(function(){
          var condiciones = $("#exampleCheck1").is(":checked");

          var importe=parseFloat($("#totalpagar").html());
          var pago_inicial=parseFloat($("#pago_inicial").val());
          var subtotal=(parseFloat($("#costo_envio").val())+importe)-pago_inicial;

          if (condiciones==true) {
            var condescuento=subtotal-20;
            $("#total_a_pagar").html(condescuento.toFixed(2));
          }else{
            $("#total_a_pagar").html(subtotal.toFixed(2));
          }
        });
    //-------------funcion solo guardar pedido...............
        function guardarPedido(accion){
          var descuento=0;
          var condiciones = $("#exampleCheck1").is(":checked");//si esta sellecionado el checkbox
          if (condiciones==true) {
            descuento=$("#exampleCheck1").val();
          }else{
            descuento=0;
          }

          var id_pedido=0;
          @if(isset($id_pedido))
            id_pedido={{$id_pedido}};
          @else
            id_pedido=0;
          @endif

          var contEach=0;
          //comproband si hay productos en la tabla de venta
          $("#tabla_venta").find("tr td:first-child").each(function(){
                contEach++; 
              });//fin each

            if (contEach>1) {//si si hay productos en la tabla
              if(idCliente!=0){//si sí existe el cliente
                //guardando datos de la tabla de los productos

                  var id=[];
                  var cantidad=[];
                  var unidad=[];
                  var descripcion=[];
                  var precio=[];
                  var tipo_precios=[];
                  var precio_vendido=[];
                  var subtotalPro=[];//inporte de productos
                  var contEach=0;
                  var total=0;
              $("#tabla_venta").find("tr td:first-child").each(function(){
                if($(this).siblings("td").eq(4).html()!=""){
                    id.push($(this).html());
                    cantidad.push($(this).siblings("td").eq(0).html());
                    unidad.push($(this).siblings("td").eq(1).html());
                    descripcion.push($(this).siblings("td").eq(2).html());
                    precio.push($(this).siblings("td").eq(3).find("select").val());
                    subtotalPro.push($(this).siblings("td").eq(5).html());
                    precio_vendido.push($(this).siblings("td").eq(4).html());
                    total+=parseFloat($(this).siblings("td").eq(5).html());
                  }
               });
         
              var fecha=fecha_actual()+" - "+hora();
                $.ajax({
                  url:"{{route('crearpedido')}}",
                  type:'post',
                  dataType:'json',
                  data:{
                    caja:"{{$caja}}",
                    turno:"{{$turno}}",
                    cajero:"{{Auth::user()->name}}",
                    id_pedido:id_pedido,
                    accion:accion,
                    idCliente:idCliente,
                    descripcion_lugar:$("#descripcion_lugar").val(),
                    id_ruta:$("#id_ruta").val(),
                    importe:$("#subtotal").html(),
                    costo_envio:$("#costo_envio").val(),
                    pago_inicial:$("#pago_inicial").val(),
                    subtotal:$("#totalPedido").html(),
                    descuento:descuento,
                    por_pagar:$("#total_a_pagar").html(),
                    idP:id,
                    cantidad:cantidad,
                    unidad:unidad,
                    descripcion:descripcion,
                    precio:precio,
                    precio_vendido:precio_vendido,
                    subtotalProducto:subtotalPro,
                    cajero:'{{Auth::user()->name}}',
                    fecha:fecha
                  },success:function(data){
                    if (data.status=="success") {
                      $("#descripcion_lugar,#costo_envio").val('');
                      $("#subtotal,#totalPedido").html('0.00');
                      $("#modal_pedidos").modal("hide");
                      $("#totalpagar").html("0.00");
                      $("#Total_en_moda_Venta").html("0.00");

                      $("#alertas").html("<div class='bg-success text-white rounded'>Pedido #"+data.id_pedido+" creado correctamente.</div>");
                      setInterval(function(){
                        $("#alertas").html('');
                      },5000);

                      limpiartablaventa();
                      bloqueador=0;
                      $("#findProducto").focus();

                      idCliente=0;
                      nivelCliente="a_publico";
                      $("#id_cliente_footer").html(0);
                    }else{
                      $("#AlertasHome").html("<div class='alert alert-danger' role='alert'>error al realizar el pedido</div>");
                      setInterval(function(){ $("#AlertasHome").html('');}, 5000);
                    }

                  },error:function(){
                    alert("Error en el servidor, consute con su administrador");
                  }
                });
              }else{//sin aun no se ha seleccionado el cliente
                 $("#div_alert_pedidos").html('<div class="alert alert-danger">No se ha seleccionado un cliente.</div>');
              setInterval(function(){$("#div_alert_pedidos").html('');},4000);
              }
            }else{//si no hay productos en la tabla
              $("#div_alert_pedidos").html('<div class="alert alert-danger">No hay productos en la tabla.</div>');
              setInterval(function(){$("#div_alert_pedidos").html('');},4000);
            }
        }
    //-----------funcion cancelar----pedidos--------------------
        function cancelar_pedido(){ 
          location.reload();
        }


      if ($("#msj").html()=='F8') {//si se recibe que es un pedido mostrar el modal del msj
        // $("#msjmodal").modal("show");
        $("#div_para_alert").html('<div class="alert alert-warning">Esta en la pestaña de ventas pero con opciones de realizar pedidos, es necesario elegir primero un cliente para poder ajustar los precios. <b class="text-dark">Al finalizar preciona F8 para realizar el pedido.</b> </div>');
        setInterval(function(){
          $("#div_para_alert").html("");
        },10000);
        $("#modalSelectCliente").modal("show"); 
        $("#f9").hide();
        $("#f8").show();
        $(".p_nav_parapedidos").html("Para pedidos");
      }
    //------click en el boton f8 en footer--------------
          $("#f8").click(function(){
              mostrarModal_pedidos();
            });
    //---------foncus en input de costo de envio al abrir el modal----
          $('#modal_pedidos').on('shown.bs.modal', function (e) {
              $('#costo_envio').focus();
            });
    //------calcular total de pedido--------------------
          $('#costo_envio').keyup(function(){
              calcular_total_pedido();
          });
    function calcular_total_pedido(){
      var envio=parseFloat($("#costo_envio").val());
              envio=envio.toFixed(2);
              $("#span_envio").html(envio);

              var pago_inicial=parseFloat($("#pago_inicial").val());

              var importe=parseFloat($("#totalpagar").html());
              var subtotal=(parseFloat($("#costo_envio").val())+importe)-pago_inicial;

            $("#totalPedido").html(subtotal.toFixed(2));
            

            var condiciones = $("#exampleCheck1").is(":checked");
            if (condiciones==true) {
                var condescuento=subtotal-20;
                $("#total_a_pagar").html(condescuento.toFixed(2));
            }else{
                $("#total_a_pagar").html(subtotal.toFixed(2));
            }
    }
    //-------------calcular total pedido cuando keyup en pago inicial
          $('#pago_inicial').keyup(function(){

            var envio=$('#costo_envio').val();
            var envio=parseFloat($("#costo_envio").val());

            var pago_inicial=parseFloat($(this).val());
            pago_inicial=pago_inicial.toFixed(2);

            $("#span_pago_inicial").html(pago_inicial);

              var importe=parseFloat($("#totalpagar").html());
              var subtotal=parseFloat((envio+importe)-parseFloat($(this).val()));

            // $("#totalPedido").html(subtotal.toFixed(2));
            

            var condiciones = $("#exampleCheck1").is(":checked");
            if (condiciones==true) {
                var condescuento=subtotal-20;
                $("#total_a_pagar").html(condescuento.toFixed(2));
            }else{
                $("#total_a_pagar").html(subtotal.toFixed(2));
            }
            var limite=parseFloat($("#limite").val());
            probar_limite_credito(limite,subtotal);


          });
    //####################funcion mostrarModalPedidos############################
          function mostrarModal_pedidos(){
            $("#modal_pedidos").modal("show");
            $("#costo_envio").val(0);
            $("#pago_inicial").val(0);
            $("#subtotal").html($("#totalpagar").html());//importe
            $("#totalPedido").html($("#totalpagar").html());//subtotal
            $("#total_a_pagar").html($("#totalpagar").html());//total

            calcular_total_pedido();

           if (idCliente!=0) {
              $.ajax({
                url:'{{route("buscarClientes")}}',
                type:'post',
                dataType:'json',
                data:{
                  accion:4,
                  busqueda:idCliente,
                },
                success:function(data){
                  $("#nombreParaPedido").val(data.nombre+" "+data.apellidos);
                  $("#localidadParaPedido").val(data.localidad+" "+data.municipio);
                  $("#identificacionParaPedido").val(data.identificacion);
                  $("#monedero").val(data.monedero);
                  $("#limite").val(data.limite_credito);
                  var monedero=parseFloat(data.monedero);

                  //si el total es mayor al limite permitido del cliente
                  var total_pedido=parseFloat($("#totalpagar").html());
                    probar_limite_credito(data.limite_credito,total_pedido);
                  //--------------si el monedero es mayor a 20 mostrar aplicar descuento
                   if (monedero>=20 && parseFloat($("#totalpagar").html())>500 ) {
                      $("#div_check").show();
                    }else{

                      $("#div_check").hide();
                    }
                    @if(isset($pedido->descuento))
                        var descuento='{{$pedido->descuento}}';
                          descuento=parseFloat(descuento);
                        if(descuento==20){
                          $("#div_check").show();
                          $("#exampleCheck1").trigger('click');
                        }
                    @endif
                },
                error:function(){
                }
              });
           }else{
             $("#btn_apartar,#btn_solo_guardar,#btn_enviar_pedido").hide();
            $("#div_check").hide();
           }
          }
      function probar_limite_credito(limite,total){
        if( total>parseFloat(limite)){
          $("#div_alert_excede").html("<div class='alert alert-danger'>Excede el limite permitido de su credito.</div>");
          $("#btn_apartar,#btn_solo_guardar,#btn_enviar_pedido").hide();
        }else{
          $("#div_alert_excede").html('');
          $("#btn_apartar,#btn_solo_guardar,#btn_enviar_pedido").show();
        }
      }



    //##############funcion buscarClientes###########################
        function buscarClientes(tipo){
          limpiarIformacionCliente();

          var campoBusqueda=$("#findClient").val();
          $.ajax({
            url:'{{route("buscarClientes")}}',
            type:'post',
            dataType:'json',
            data:{
              accion:tipo,
              busqueda:campoBusqueda
            },
            success:function(data){
               // alert(data[0].nombre+data.length);

               if (data.length==1) {
                $("#econtrados").html('');
                idCliente=data[0].id;
                nivelCliente=data[0].nivel;
                $("#econtrados").append("<option value='"+data[0].id+"'>"+data[0].nombre+" "+data[0].apellidos+"</option>");
                $("#nombreClinte").val(data[0].nombre+" "+data[0].apellidos);
                $("#localidad2").val(data[0].localidad+" "+data[0].municipio);
                $("#telefono").val(data[0].telefono);
                var monedero=0;
                if(data[0].monedero!=null){
                  monedero=data[0].monedero;
                }else{monedero=0;}
                $("#monedero_al_buscar_cliente").val("$"+monedero);
                $("#nivel").val(data[0].nivel);
                $("#fotoCliente").attr("src","img/clientes/"+data[0].foto+"");
                var calificacion=data[0].calificacion;
                $("#input_id_cliente").val(data[0].id);
                $("#input_nivel_cliente").val(data[0].nivel);

                for(var y=0;y<5;y++){
                  if(y<parseInt(calificacion)){
                    $("#stars").append("<i class='fas fa-star text-warning'></i>");
                  }else{
                    $("#stars").append("<i class='fas fa-star text-secondary'></i>");
                  }
                }
               }else{
                //para limpiar los campos primero
                 $("#fotoCliente").attr("src","img/clientes/no-images.jpg");
                $("#nombreClinte,#localidad,#telefono,#monedero,#nivel").val("");
                $("#econtrados,#stars").html("");
                $("#econtrados").append("<option selected='' disabled=''>Selecione uno.</option>");
              for(var x=0;x<data.length;x++){
                 $("#econtrados").append("<option value='"+data[x].id+"'>"+data[x].nombre+" "+data[x].apellidos+"</option>");
              }//fin for
            }//fin else
             
             
            },
            error:function(){

            }
          });
        }

    //##################al seleccionar clientes listados en el select
        $("#econtrados").change(function(){
          limpiarIformacionCliente();
          $.ajax({
            url:'{{route("buscarClientes")}}',
            type:'post',
            dataType:'json',
            data:{
              accion:2,
              busqueda:$("#econtrados").val()
            },success:function(data){

              idCliente=data[0].id;
              nivelCliente=data[0].nivel;
              $("#nombreClinte").val(data[0].nombre+" "+data[0].apellidos);
                $("#localidad2").val(data[0].localidad+" "+data[0].municipio);
                $("#telefono").val(data[0].telefono);
                var monedero=0;
                if(data[0].monedero!=null){
                  monedero=data[0].monedero;
                }else{monedero=0;}
                $("#monedero_al_buscar_cliente").val("$ "+monedero);
                $("#nivel").val(data[0].nivel);
                $("#input_id_cliente").val(data[0].id);
                $("#input_nivel_cliente").val(data[0].nivel);
                $("#fotoCliente").attr("src","img/clientes/"+data[0].foto+"");
                var calificacion=data[0].calificacion;
                for(var y=0;y<5;y++){
                  if(y<parseInt(calificacion)){
                    $("#stars").append("<i class='fas fa-star text-warning'></i>");
                  }else{
                    $("#stars").append("<i class='fas fa-star text-secondary'></i>");
                  }
                }
            }//fin success
          });//fin ajax
        }); //fin on change
//#######################################################################################
//#######################################################################################
//#######################################################################################


    var bloqueador=0;//variable public
    //-------------------------------------------------------------------------------------------
    function cargar_tr(){
        for(var x=0;x<16;x++){
            $("#tabla_venta").append("<tr><td class='d-none'></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    }

    function eliminar_ultimo_tr_vacio(){
        var ultimo=$("#tabla_venta").find("tr:last");
        var valor=ultimo.find("td").eq(0).html();
        if (valor=='') {
            $("#tabla_venta").find("tr:last").remove();
        }
    }

    function guardar_devolucion(){
      var id_venta=$("#id_venta").html();
      var total_venta=parseFloat($("#total_venta").html())-parseFloat($("#total_devolver").html());
      var id=[];
      var cantidad_devolver=[];
      $("#tabla_prod_devolucion tbody").find("tr td:first-child").each(function() {
          id.push($(this).html());
          cantidad_devolver.push(parseFloat($(this).siblings("td").eq(0).find("input").val()));
        });
      // alert(cantidad_devolver);
      $.ajax({
        url:"{{url('/devolucion')}}",
        type:"post",
        dataType:"json",
        data:{
          id_venta:id_venta,
          total_venta:total_venta,
          ip_p:id,
          cantidad:cantidad_devolver
        },
        success:function(e){
          buscar_venta(id_venta);
          $("#alert_in_devoluciones").html("<div class='alert alert-success'>Devolución correctamente</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
        },error:function(){
          $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se pudo guardar la información</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
        }
      });
    }
    function limpiar_modal_devolucion(){
      $("#tabla_prod_devolucion tbody").html('');
      $("#total_venta,#total_devolver,#id_venta").html('');
    }
    function buscar_venta(id_venta){
      $("#div_info,#total_venta,#total_devolver").html("");
      $("#tabla_prod_devolucion tbody").html("")
      $.ajax({
        url:"{{url('/buscar_venta')}}",
        type:"post",
        dataType:"json",
        data:{
          id_venta:id_venta
        },success:function(e){
          console.log(e);
          if (e !=null) {//si existe la venta mostrarla
            $("#div_info").html(
              "<span class='text-primary'>Turno: "+e['venta'].id_turno+"</span>"+
              "  <span>Atendio: "+e['venta'].usuario+"</span>"+
              "<span class='float-right text-danger'>Fecha: "+e['fecha']+"</span>");
              var total_venta=0;
            for(var x=0;x<e['id_pro'].length;x++){
              var cantidad=e['cantidad'][x];
              var subtotal=parseFloat(cantidad)* parseFloat(e['precio'][x]);
              subtotal=subtotal.toFixed(2);
              $("#tabla_prod_devolucion tbody").append("<tr><td class='d-none'>"+e['id_pro'][x]+"</td> <td > <input type='number' style='width:60px' value='0'>     de <i class='d-non float-right'>"+cantidad+"</i></td> <td>"+e['unidad'][x]+"</td> <td>"+e['nombres'][x]+"</td> <td>"+e['precio'][x]+"</td> <td>"+subtotal+"</td> <td>0</td>  </tr>");
              total_venta+=parseFloat(cantidad)* parseFloat(e['precio'][x]);
            }


            $("#total_venta").html(total_venta.toFixed(2));
            $("#total_devolver").html("0.00");
          }else{
            $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
          }
        },error:function(){
          $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
        }
      });
    }
    //##################calculando cantidad a devolver###########
    $("#tabla_prod_devolucion tbody").on("keyup || change","tr td:nth-child(2)",function(){
      var cantidad_original=parseFloat($(this).find("i").html());
      var modificado=parseFloat($(this).find("input").val());
      // alert(cantidad_original+" " + modificado);
      if (modificado>cantidad_original || modificado<0) {//si es mayor a la cantidad original mandar alerta
        $(this).find("input").val(cantidad_original);
        $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No puede ser mayor ni menor a 0</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},2000);
          //calculando e lsubtotal
          var precio=parseFloat($(this).siblings("td").eq(3).html());
          var subtotal=cantidad_original*precio;
          $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
      }else{
        var precio=parseFloat($(this).siblings("td").eq(3).html());
        var subtotal=modificado*precio;
       $(this).siblings("td").eq(5).html(subtotal.toFixed(2));
      }
      //calculando total de $ a devolver
      total=0;
      $("#tabla_prod_devolucion tbody").find("tr td:last-child").each(function() {
          total+=parseFloat($(this).html());
        });
      $("#total_devolver").html(total.toFixed(2));
    });




    function storeturno(){
      if (bloqueador==0) {
        bloqueador=1;
        $.ajax({
          url:"{{url('/storeturno')}}",
          type:"post",
          data:$("#form-store-turno").serialize(),
          beforeSend:function(){
            bloqueador=1;
          },
          success:function(e){
            $("#form-store-turno")[0].reset();
              $("#modal_turno").modal("hide");
              location.reload();
              setInterval(bloqueador=0,1000);
            bloqueador=0;
          },error:function(e){

          }
        });
      }
    }

    function calcular_total_venta(id){

      $.ajax({
        url:"{{url('/total_venta')}}",
        type:"post",
        dataType:"json",
        data:{id:id},
        success:function(e){
          e=parseFloat(e);
          e=e.toFixed(2);
          $("[name=cierre_system]").val(e);
        },error:function(){
          alert("No fue posible obtener los datos de las ventas.");
        }
      });
    }



    $("#form_edit_salida").hide();
    function editarsalida(id,concepto,cantidad){
      $("#form_edit_salida").show();
      $("[name=edit_id]").val(id);
      $("[name=edit_concepto]").val(concepto);
      $("[name=edit_cantidad]").val(cantidad);
    }
    function guardar_edit_salida(){
      $.ajax({
        url:"{{url('/editar_salida')}}",
        type:"post",
        dataType:"json",
        data:{
          id:$("[name=edit_id]").val(),
          concepto:$("[name=edit_concepto]").val(),
          cantidad:$("[name=edit_cantidad]").val(),
        },success:function(e){
          buscarsalidas();
          $("#form_edit_salida")[0].reset();
          $("#form_edit_salida").hide();
          $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Modificado corectamente.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        },error:function(){
          $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>Error al modificar.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        }
      });
    }
    function buscarsalidas(){
      $("#tabla_registros_salidas tbody").html('');
      $.ajax({
        url:"{{url('/buscarsalidas')}}",
        type:"post",
        dataType:"json",
        data:{
          user:{{Auth::user()->id}},
          turno:$("#id_turno").val()
        },
        success:function(e){
          for(var x=0;x<e.length;x++){
            var concepto=e[x].concepto;
            $("#tabla_registros_salidas tbody").append("<tr><td class='d-none'>"+e[x].id+"</td><td>"+e[x].concepto+"</td> <td class='text-right'>"+e[x].cantidad+"</td><td><button class='btn btn-danger btn-sm' onclick='eliminarsalida("+e[x].id+");'> <i class='fas fa-trash'></i></button> <button class='btn btn-sm btn-success' onclick='editarsalida("+e[x].id+",\""+e[x].concepto+"\",\""+e[x].cantidad+"\");'><i class='fas fa-edit'></i></button></td> </tr>");
          }
        },error:function(){
          alert("Error al buscar registros");
        }
      });
    }
    function eliminarsalida(id){
      // alert(id);
      var msj=confirm("Desea eliminar este registro?");
      if (msj) {
        $.ajax({
        url:"{{url('/eliminarsalida')}}",
        type:"post",
        dataType:"json",
        data:{id:id},
        success:function(e){
          buscarsalidas();
          $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Se eliminó corectamente.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        },error:function(){
          $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>No se pudo eliminar</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        }
      });
      }
    }

    //###############registrar salidas##########
        function registrar_salidas(){

          if ($("[name=concepto]").val()!="" && $("#cantidad_registro_salida").val()!="" && bloqueador==0) {
            bloqueador=1;
            $.ajax({
            url:"{{url('/storesalida')}}",
            type:"post",
            dataType:"json",
            data:$("#form_save_salidas").serialize(),
            success:function(e){
              $("#form_save_salidas")[0].reset();
              $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>"+e+"</div>");
              setInterval(function(){
                $("#alertasmodal_salidas").html('');
              },4000);
              bloqueador=0;
            },error:function(){
              $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>ERROR AL GUARDAR ESTE REGISTRO</div>");
              setInterval(function(){
                $("#alertasmodal_salidas").html('');
              },4000);
            }
          });
          }else{
            alert("Necesita llenar todos los campos.");
          }
        }
//###########################ejecutar venta##########################
        function ejecutar_venta(){
          var id_p=[];
          var cantidad=[];
          var unidad=[];
          var descripcion=[];
          var precio=[];
          var tipo_precio=[];
          var subtotal=[];
          var cont=0;
          var total=0;
          $("#tabla_venta").find("tr td:first-child").each(function(){
            if($(this).siblings("td").eq(4).html()!=""){
                id_p.push($(this).html());
                cantidad.push($(this).siblings("td").eq(0).html());
                unidad.push($(this).siblings("td").eq(1).html());
                descripcion.push($(this).siblings("td").eq(2).html());
                precio.push($(this).siblings("td").eq(3).find("select").val());
                tipo_precio.push($(this).siblings("td").eq(3).find("select option:selected").data("tipo"));
                subtotal.push($(this).siblings("td").eq(5).html());
                total+=parseFloat($(this).siblings("td").eq(5).html());
              }
           });


          var efectivo=parseFloat($("#efectivo").val());
          var ticket=confirm("¿Desea imprimir TICKET?");
          if(id_p.length>0){
            if (bloqueador==0) {
              if(efectivo-total >=0){
                bloqueador=1;
                $.ajax({
                        url:"{{url('ejecutar_venta')}}",
                        type:"post",
                        dataType:"json",
                        data:{
                          id_p:id_p,
                          cantidad:cantidad,
                          unidad:unidad,
                          descripcion:descripcion,
                          precio:precio,
                          tipo_precio:tipo_precio,
                          subtotal:subtotal,
                          total:total,
                          efectivo:$("#efectivo").val(),
                          user:"{{Auth::user()->name}}",
                          caja:"{{$caja}}",
                          id_turno:$("#id_turno").val(),
                          ticket:ticket
                        },success:function(e){
                          // alert(e);
                          $("#alertas").html("<div class='bg-success text-white rounded'>"+e+"</div>");
                          setInterval(function(){
                            $("#alertas").html('');
                          },5000);
                          $("#modal_cobrar").modal("hide");
                          $("#efectivo").val('');
                          limpiartablaventa();
                          bloqueador=0;
                          $("#findProducto").focus();
                          last_venta();
                        },error:function(){
                          alert("No se pudo realizar la venta.");
                        }
                });
              }else{
                alert("ERROR en el cambio, no debe de ser negativo");
              }
            }else{
              alert("No es posible realizar la venta, hay otra tarea en proceso...");
            }
          }else{
            alert("No hay articulos en la tabla");
          }
        }

  //####################funcion buscar por codigo de barras#############
    function buscar_x_codigo(codigo){

      $.ajax({
        url:"{{url('/buscar_x_codigo')}}",
        type:"post",
        dataType:"json",
        data:{codigo:codigo},
        success:function(e){
          if(e.length==0){
              alert("No se encontro el articulo.");
              $("#findProducto").val('');
              $("#findProducto").focus();
            }else{
              if (ya_existe(e[0].id)==0) {
                var precios=generar_select_precios(e[0].precio_caja,e[0].pre_unidad,e[0].pre_mayoreo,e[0].pre_caja,e[0].pre_paquete,e[0].pre_membrecia,e[0].a_granel,e[0].mayoreo_apartir);

                append_tabla_venta(e[0].id,e[0].unidad,e[0].descripcion_articulo,precios,e[0].pre_unidad,e[0].pre_unidad,e[0].a_granel);
                $("#findProducto").val('');
                $("#findProducto").focus();
              }
            }  
        },error:function(){
          alert("No se encontro el articulo.");
          $("#findProducto").val('');
          $("#findProducto").focus();
        }
      });
    }
  //################FUNCION DE BUSCAR PRODUCTO############################
      function buscarPro(){
        limpiartablabusqueda();
        var producto=$("#findProducto").val();
          if(producto.length>=0){
            $('#modal_busqueda').modal('show');
              $.ajax({
                url:'{{route("buscarExistencia")}}',
                type:'post',
                dataType:'json',
                data:{
                  buscar:producto
                },success:function(e){
                   if (e=="") {
                     $("#tabla_busqueda tbody").append("<tr  class='text-danger'> <td  colspan='6'><b>No se encontró registro!!</b></td></tr>");
                    }else{
                      for(var x=0;x<e.length;x++){
                      var precio=generar_select_precios(e[x].precio_caja,e[x].pre_unidad,e[x].pre_mayoreo,e[x].pre_caja,e[x].pre_paquete,e[x].pre_membrecia,e[x].a_granel,e[x].mayoreo_apartir);
                      var pre_a_vender=e[x].pre_unidad;

                      $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td width='1px' class='d-none'>"+e[x].id+
                        "</td>   <td>"+e[x].clave+
                        "</td>   <td>"+e[x].unidad+
                        "</td>   <td>"+e[x].descripcion_articulo+
                        "</td>   <td>"+precio+
                        "</td>   <td class='text-right'>$"+pre_a_vender+
                        "</td>   <td class='text-right'>"+e[x].existencia+
                        "</td>  </tr>");
                     }//fin for
                    }
                },error:function(){
                  alert("Hubo un problema al buscar este producto");
                }
              });  
          }
                       
      }

function generar_select_precios(precio_caja,pre_unidad,pre_mayoreo,pre_caja,pre_paquete,pre_membrecia,a_granel,mayoreo_apartir){
  var precio_caja_if=precio_caja;

  precio_caja=parseFloat(precio_caja);
    precio_caja = precio_caja ? precio_caja : 0;
  pre_unidad=parseFloat(pre_unidad);
    pre_unidad = pre_unidad ? pre_unidad : 0;
  pre_mayoreo=parseFloat(pre_mayoreo);
    pre_mayoreo = pre_mayoreo ? pre_mayoreo : 0;
  pre_caja=parseFloat(pre_caja);
    pre_caja = pre_caja ? pre_caja : 0;
  pre_paquete=parseFloat(pre_paquete);
    pre_paquete = pre_paquete ? pre_paquete : 0;
  pre_membrecia=parseFloat(pre_membrecia);
    pre_membrecia = pre_membrecia ? pre_membrecia : 0;

  var precio="";


  if(precio_caja_if==0 || precio_caja_if==null){
    precio="<select class='form-control'>"+
    "<option value='"+pre_unidad+"' selected='true' data-tipo='unidad'>Venta</option>"+
    "<option value='"+pre_mayoreo+"' data-tipo='mayoreo'>Mayoreo >="+mayoreo_apartir+"</option>"+
    "<option value='"+pre_membrecia+"' data-tipo='membrecia'>Membrecia</option>"+
    "</select><span class='d-none'>"+a_granel+"</span><p class='d-none'>"+mayoreo_apartir+"</p><small class='d-none'>"+precio_caja+"</small>";
  }else{
    precio="<select class='form-control'>"+
    "<option value='"+pre_unidad+"' selected='true' data-tipo='unidad'>Venta</option>"+
    "<option value='"+pre_caja+"' data-tipo='caja'>Venta x caja</option>"+
    "<option value='"+pre_paquete+"' data-tipo='paquete'>Venta x paquete</option>"+
    "<option value='"+pre_mayoreo+"' data-tipo='mayoreo'>Venta x mayoreo >="+mayoreo_apartir+"</option>"+
    "<option value='"+pre_membrecia+"' data-tipo='membrecia'>venta con membrecia</option>"+
  "</select><span class='d-none'>"+a_granel+"</span><p class='d-none'>"+mayoreo_apartir+"</p><small class='d-none'>"+precio_caja+"</small>";
  }
  return precio;
}

setInterval(function(){$("#hora_modal_cerrar_turno").html(hora());},1000);




  function fecha_actual(){
    var fecha = new Date();
    var fechahoy=(fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear());
    return fechahoy;
  }
  function hora(){
    var fecha = new Date();
    var horahoy=(fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds());
    return horahoy;
  }


    
</script>
























