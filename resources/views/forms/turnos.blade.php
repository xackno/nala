@extends('layouts.app')

@section('content')

@if(session('success'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong><i class="fas fa-check"></i> </strong> {{session('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
<div class="card">

	<div class="card-body">
		<h2 class="text-center text-primary"> <i class="fas fa-power-off "></i> TURNOS</h2>
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="table_turnos">
				<thead class="bg-primary text-white">
					<tr>
						<th>#</th>
						<th>usuario</th>
						<th>caja</th>
						<th>$ inicio</th>
						<th>calculo por el sistema</th>
						<th>conteo en caja</th>
						<th>comentario inicio</th>
						<th>comentario final</th>
						<th>finalizó</th>
						<th>status</th>
						<th>inició</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody>
					@foreach($turnos as $turno)
					@if(Auth::user()->caja!=null)
						@if(Auth::user()->caja==$turno->caja && Auth::user()->id==$turno->usuario)
							<tr @if($turno->status=='abierto') style="background:#F55E32;color:white" @endif>
							<td>T{{ str_pad($turno->id, 6, '0', STR_PAD_LEFT)}}</td>
							<td>{{$turno->name}}</td>
							<td>{{$turno->caja}}</td>
							<td>{{$turno->inicio}}</td>
							<td>{{$turno->cierre_system}}</td>
							<td>{{$turno->cierre_conteo}}</td>
							<td>{{$turno->comentario1}}</td>
							<td>{{$turno->comentario2}}</td>
							<td>{{$turno->fecha_fin}}</td>
							<td>{{$turno->status}}</td>
							<td>{{$turno->created_at}}</td>
							<td><a href="{{url('/imprimir_corte_turno',$turno->id)}}" class="btn btn-sm btn-dark"><i class="fas fa-print"></i></a> </td>
						</tr>
						@endif
					@else
						<tr @if($turno->status=='abierto') style="background:#F55E32;color:white" @endif>
							<td>{{ str_pad($turno->id, 8, '0', STR_PAD_LEFT)}}</td>
							<td>{{$turno->name}}</td>
							<td>{{$turno->caja}}</td>
							<td>{{$turno->inicio}}</td>
							<td>{{$turno->cierre_system}}</td>
							<td>{{$turno->cierre_conteo}}</td>
							<td>{{$turno->comentario1}}</td>
							<td>{{$turno->comentario2}}</td>
							<td>{{$turno->fecha_fin}}</td>
							<td>{{$turno->status}}</td>
							<td>{{$turno->created_at}}</td>
							<td><a href="{{url('/imprimir_corte_turno',$turno->id)}}" class="btn btn-sm btn-dark"><i class="fas fa-print"></i></a> </td>
						</tr>
					@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>
<style type="text/css">
	#table_turnos_filter label{float: right;}
	.table tbody tr:hover{
		background:#3CA567!important;
		color:white;
	}
	.table td, .table th{
		padding: 0px;
		height: 30px
	}
</style>

@endsection
@section('script')
<script type="text/javascript">
	$("#table_turnos").DataTable({
		"order": [[ 0, 'desc' ]],
		"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
});
	$("#table_turnos_filter input").focus();

</script>
@endsection
