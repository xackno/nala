@extends('layouts.app')
@section('content')

<div class="container-fluid" style="padding-top: 10px">

	<div class="col-12 col-md-3 col-lg-3 col-xl-3 float-left">
		<select class="form-control">
			<option>Compras</option>
			<option>Stock</option>
		</select>
	</div>

	<div class="col-12 col-md-3 col-lg-3 col-xl-3 float-right">
		@if(Request::path()=="buscar_en_inventario")
		<a href="{{url('/inventarios')}}"><i class="fas fa-eye"></i> Ver todos</a>
		@endif
		<form  action="{{url('/buscar_en_inventario')}}" method="post">
			@csrf
			<div class="input-group">
				<input type="text" name="busqueda" class="form-control" placeholder="Buscar..."  @if(Request::path()!="articulos") value="" @endif autofocus="true">
				<button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-search"></i></button>
			</div>
		</form>	
	</div>



	<div class="table-responsive">
			<hr>
		<table class="table table-bordered table-striped" id="tabla_articulos" style="font-size: 10pt">
			<thead class="bg-primary">
				<tr>
					<th>CLAVE</th>
					<th>DESCRIPCIÓN</th>
					<th><i class="fas fa-barcode"></i></th>
					<th>$ COMPRA</th>
					<th>$ VENTA</th>
					<th>INV. COMPRA</th>
					<th>EXIS. STOCK</th>
					<th>EXIS. BODEGA</th>
					<th>COMPRA RESERVA</th>
					<th>INV.MIN. COMPRA</th>
					<th>INV.MAX. COMPRA</th>
				</tr>
			</thead>
			<tbody>
				@foreach($articulos as $r)
				<tr>
					<td>{{$r->clave}}</td>
					<td>{{$r->descripcion_articulo}}</td>
					<td class="text-right">{{$r->codigo_barra}}</td>
					<td class="text-right">$ {{$r->pre_compra}}</td>
					<td class="text-right">$ {{$r->pre_unidad}}</td>
					<td>{{number_format($r->inv_compra)}}</td>
					<td>{{number_format($r->existencia_stock)." ".$r->unidad}}</td>
					<td>{{number_format($r->existencia_bodega)}}</td>
					<td></td>
					<td>{{number_format($r->inv_min)}}</td>
					<td>{{number_format($r->inv_max)}}</td>

				</tr>
				@endforeach
				
			</tbody>
		</table>
		<div >
			<div class="float-right">
				@if(Request::path()=="inventarios")
					{{$articulos->links()}}
				@endif
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.table td,.table th{
		padding: 3px !important;
	}

</style>
@endsection
@section('script')
<script type="text/javascript">


</script>
@endsection