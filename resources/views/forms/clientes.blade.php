@extends('layouts.app')

@section('content')


<div class="container-fluid" style="margin-top:10px">
	<div id="div_alert"></div>
		@if(session('success'))
			<div class="alert alert-success alert-dismissible fade show">
				<h3>{{session('success')}}</h3>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
			</div>
				@endif
				@if(session('error'))
			<div class="alert alert-danger alert-dismissible fade show">
				<h3>{{session('error')}}</h3>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif
	
	<div class="row">
		<div class="col-10"><h4 id="titulo" style="margin-left:20px" class="text-primary"><i class="fas fa-users"></i> Clientes</h4></div>
		<div class="col-2">
			<div class="input-group float-right">
					<button class="btn btn-sm btn-warning float-right" id="btn_a_modal_cliente">
					<i class="fas fa-plus"></i> Cliente</button>	
					<a  href="{{url('/rutas')}}" class="btn btn-sm btn-success float-right" style="margin-left:10px"><i class="fas fa-plus"></i> Rutas</a>
			</div>
		</div>
	</div>

<div class="alertas"></div>
<button class="btn btn-primary" id="btn_ver_rutas"><i class="fa fa-eye"></i>Ver rutas</button>
<button class="btn btn-primary" id="btn_ver_clientes" style="display: none"><i class="fa fa-eye"></i>Ver clientes</button>


<div class="card" id="view_clientes" >
	<div class="card-body" style="height: 82vh;">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="tabla_clientes">
				<thead class="table-dark ">
		      <tr> 
		        <th style="width: 8%"><i class="fas fa-cogs"></i></th>  
		        <th class="text-center"><i class="fas fa-image text-warning"></i></th>
			      <th>Nombre</th>
			      <!-- <th>Apellidos</th> -->
			      <!-- <th>Municipio</th> -->
			      <th>Localidad</th>
			      <th>Calle</th>
			      <th class="text-center"><i class="fas fa-phone-square text-danger"></i></th>
			      <th class="text-center"><i class="fas fa-mail-bulk text-primary"></i></th>
			      <th>Usuario</th>
			      <th>Status</th>
			      <th>nivel</th>
			      <th><i class="fas fa-money-check-alt text-success"></i></th>
			      <th><i class="fas fa-barcode text-primary"></i></th>
			      <th><i class="fa fa-star text-warning"></i></th>
			      <th><i class="fas fa-route text-success"></i></th>   
		      </tr>
		    </thead>
				<tbody>
					@foreach($clientes as $cliente)
		      	<tr scope="row">
		      		<td>
		      			<button class="btn btn-danger btn-sm" onclick="eliminar({{$cliente->id}});"><i class="fas fa-trash"></i></button>
		      			<a href="{{url('/edit_clientes',$cliente->id)}}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
		         	</td>

		        	<td><img src="{{asset('img/clientes/'.$cliente->foto)}}" class="imgRedonda"></td>
		        	<td class="text-primary"><b>{{$cliente->nombre}}</b>  {{$cliente->apellidos}}</td>
		        	<td class="text-info minimizar">{{$cliente->localidad}}<p class="text-primary">{{$cliente->municipio}}</p></td>
		        	<td>{{$cliente->calle}}</td>
		        	<td>{{$cliente->telefono}}</td>
		        	<td class="minimizar text-info">{{$cliente->correo}}</td>
		        	<td>{{$cliente->usuarioApp}}</td>
		        	<td>
		        		@if($cliente->status=="enable")
		        		<p class="text-primary">{{$cliente->status}}</p>
		        		@else
		        		<p class="text-secondary">{{$cliente->status}}</p>
		        		@endif
		        	</td>
		        	<td>{{$cliente->nivel}}</td>
		        	<td><b>${{$cliente->monedero}}</b></td>
		        	<td>{{$cliente->identificacion}}</td>
		        	<td>
		        		<span id="span_star" class="float-right">
		      			@for($x=0;$x<5;$x++)
		      				@if($x<$cliente->calificacion)
		      					<i class="fa fa-star text-warning"></i>
		      				
		      				@endif
		      			@endfor
		            	</span>
		        	</td>
		        	<td>{{$cliente->ruta}}</td>
		        	
		      	</tr>
		      	@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- ###############################RUTAS########################## -->
<div id="view_rutas" style="display: none"  class="card">
	<div class="card-body">
		<table class="table table-striped table-bordered" id="tabla_rutas">
	   	<thead class="table-dark ">
        <tr>   
	       <th>ID</th>
	       <th>Nombre</th>
	       <th>Longitud (m)</th>
	       <th>Tiempo de recorrido</th>
	       <th>Origen</th>
	       <th>Destino</th>
	       <th></th>
	       <th><i class="fa fa-cog"></i></th>
	       <th></th>		       
            </tr>
          </thead>
          <tbody>
          	@foreach($rutas as $ruta)
          	<tr scope="row">
            	<td>{{$ruta->id}}</td>
            	<td>{{$ruta->nombre_ruta}}</td>
            	<td>{{$ruta->longitud_ruta}}</td>
            	<td>{{$ruta->tiempo_recorrido_total}}</td>
            	<td>{{$ruta->origen}}</td>
            	<td>{{$ruta->destino}}</td>
            	<td>
            		<form action="{{route('see_ruta')}}" method="post" >
            			@csrf
            			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
            			<button class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
            		</form>
            		
            		
            	</td>
            	<td>
            		<form action="{{route('edit_ruta')}}" method="post" >
            			@csrf
            			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
            			<button class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
            		</form>
            	</td>
            	<td>@if($ruta->id!=1)
            		<button class="btn btn-danger btn-sm" onclick="
            		var confm=confirm('Desea eliminar esta ruta?');
          			if(confm==true){
          			$('#Form_eliminar_ruta{{$ruta->id}}').submit();}
            		"><i class="fa fa-trash"></i></button>

            		<form action="{{route('destroy_ruta')}}" method="post" id="Form_eliminar_ruta{{$ruta->id}}" class="d-none">
            			@csrf
            			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
            		</form>
            		@endif
            	</td>
          	</tr>
          	@endforeach
          </tbody>
	  </table>
	</div>
</div>



</div>
<style type="text/css">
	#tabla_clientes_filter label{float: right;}
	#tabla_rutas_filter label{float: right;}
	.minimizar{font-size: 70%;}
	.minimizar:hover{font-size: 100%}
	#tabla_clientes  tr td,#tabla_rutas tr td{ 
		padding: 2px;margin:0px;
		border: 0.2px solid #eee;
		border-bottom: 2px solid #66bb6a;
		 }
	#tabla_clientes tr th,#tabla_rutas tr th{ 
		padding:4px;
		padding-left: 3px;
		padding-right: 3px }

	.btnAction{width: 48%;display: inline-block;}
	.imgRedonda {
		    width:40px;
		    height:42px;
		    border-radius:140px;
		    border:2px solid #666;
		}
		.imgPerfil{
			width:60px;
		    height:62px;
		    border-radius:140px;
		    border:2px solid #666;
		}
</style>



<!-- ############################################################## -->
<!--window modal ######modal agregar clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus" id="icon_header"></i><i class="fas fa-user"></i> cliente</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="div_alert2"></div>
        	<div id="alertasModal"></div>

        	<form id="form_cliente">
        		<div class="row">
					    <div class="col-xl-4 col-md-6 mb-4">
									<label for="nombre">Nombre</label>
			        		<input type="text" name="nombre" class="form-control" placeholder="Nombre o nombres ">
			        		<label for="apellidos">Apellidos</label>
			        		<input type="text" name="apellidos" class="form-control" placeholder="apellidos">
									<label for="municipio">Municipio</label>
									<input type="text" name="municipio" class="form-control" placeholder="municipio o C.P.">
			        		<label for="localidad">Localidad</label>
			        		<input type="text" name="localidad" class="form-control" placeholder="Localidad">
					    </div>

					    <div class="col-xl-4 col-md-6 mb-4">
					    	<label for="calle">Calle</label>
			        		<input type="text" name="calle" class="form-control" placeholder="Nombre y número">

			        		<label for="telefono">teléfono</label>
			        		<input type="tel" name="telefono" class="form-control" >

			        		<label for="correo">Correo</label>
			        		<input type="email" name="correo" class="form-control">

			        		<label for="foto">Foto</label>
			        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="no-images.jpg">

			        		<label>Limite de crédito</label>
			        		<input type="number" name="limite_credito" class="form-control" id="limite_credito" value="10000.00">
					    </div>

					    <div class="col-xl-4 col-md-6 mb-4">

									<label for="usuarioApp">Usuario Para la App</label>
			        		<input type="text" name="usuarioApp" class="form-control">

									<label for="password">Contraseña</label>
			        		<input type="password" name="password"  class="form-control">
			        		<label>Tipo de cliente</label>
				        		@auth
		                  @if(Auth::user()->type=='superadmin' || Auth::user()->type=='admin')
				        			<select class="form-control" name="nivel">
					        			<option value="comun">común</option>
					        			<option value="Mayoreo1">Mayoreo 1</option>
					        			<option value="Mayoreo2">Mayoreo 2</option>
					        			<option value="Mayoreo3">Mayoreo 3</option>
					        			<option value="Mayoreo4">Mayoreo 4</option>
				        			</select>
				        		@else
				        			<select class="form-control" name="nivel">
				        				<option value="comun">básico</option>
				        			</select>
				        		@endif
	                @endauth
                	<label class="">Número de identificación</label>
                	<input type="number" name="identificacion" class="form-control">

                	<input type="number" name="calificacion" class="d-none" value="1" readonly="">
                	<label>Rutas</label>
                	<select name="id_ruta" class="form-control" required="">
                		<option selected="" disabled="">Seleccione una opción</option>
                		@foreach($rutas as $r)
                		<option value="{{$r->id}}">{{$r->nombre_ruta}}</option>
                		@endforeach
                	</select>
					 		</div>
						</div>
        		<br><br>
        		<button type="button" class="btn btn-warning float-right d-none" id="updateUser">Guardar modificación</button>
        		<button type="button" class="btn  btn-success float-right" id="btn_submit_cliente"><i class="fas fa-save"></i> Guardar</button>
        	</form>
        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
        		@csrf
        		<label for="imagen"><b>Subir foto:</b></label>
			  		<input type="file" name="imagen" id="imagen" />
			  		<button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>

        </div>
      </div>
    </div>
  </div>

<!-- ############################################################## -->
<!--window modal ######modal ver card del clientes################-->
  <div class="modal fullscreen-modal fade" id="modal-card-cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white header-card-cliente" style="font-size: 160%" >
        		
        	</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div class="text-center">
        		<i class="fas fa-user fa-3x"></i>
        		<hr>
        		<label>Nombre: <b class="card-nombre" style="text-transform: uppercase;"></b></label>
        		<label>Dirección: <b class="card-direccion text-success"></b></label>
        		<label>Teléfono: <b class="card-tel text-danger"></b></label>
        		<label>E-mail: <a href="" class="card-email"></a></label><br>
        		<label>RFC:<b class="card-rfc"></b> </label>
        	</div>

        </div>
      </div>
    </div>
  </div>


@endsection
@section('script')
<script type="text/javascript">




/////-----------------------para subir foto
	$("#formsubirFoto").on("submit", function(e){
      e.preventDefault();
      var f = $(this);
      var formData = new FormData(document.getElementById("formsubirFoto"));
      formData.append("dato", "valor");
      //formData.append(f.attr("name"), $(this)[0].files[0]);
      $.ajax({
          url: "{{url('/uploadimgCliente')}}",
          type: "post",
          dataType: "",
          data: formData,
          cache: false,
          contentType: false,
 					processData: false
      	}).done(function(e){
					if (e.status=="success") {
						$("#foto").val(e.nombre);
						$("#subirImg").removeClass();
						$("#subirImg").addClass("btn btn-success");
						$("#subirImg").text("Subido");
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
				}
			}).fail(function (jqXHR, exception) {
          // Our error logic here
          console.log(exception);
      });
	});



	function eliminar(id){
		var msj=confirm("Desea eliminar este cliente?");
		if (msj) {
			$.ajax({
				url:"{{route('destroyCliente')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					$("#alertas").append('<div class="alert alert-success">Cliente eliminado correctamente.</div>');
					setInterval(function(){
						$("#alertas").html('');
						location.reload();
					},2000);

				},error:function(){
					$("#alertas").append('<div class="alert alert-danger">Error al eliminar este cliente.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
				}	
			});
		}

	}
	function vercard(id){
		$("#modal-card-cliente").modal("show");
		$(".header-card-cliente").html("<i class='fas fa-address-card'></i>"+ " "+ id);

		$.ajax({
			url:"{{route('showCliente')}}",
			type:"post",
			dataType:"json",
			data:{id:id},
			success:function(e){
				$(".card-nombre").html(e[0].nombre_cliente);
				$(".card-direccion").html("<br>Calle:"+e[0].calle+"<br> Colonia: "
					+e[0].colonia+"<br> Población: "+e[0].poblacion+"<br> Municipio: "
					+e[0].municipio	);
				$(".card-tel").html(e[0].tel);
				$(".card-email").html(e[0].email);
				$(".card-rfc").html(e[0].rfc);

			},error:function(){

			}
		});
	}
	function mov(id){

	}

//#######################agregar cliente##########################################
	$("#btn_a_modal_cliente").click(function(){
		$("#modal_cliente").modal("show");
		$("#icon_header").removeClass();
		$("#icon_header").addClass("fas fa-plus");

		$("#updateUser").addClass("d-none");
		$("#btn_submit_cliente").removeClass("d-none");

		$("#form_cliente")[0].reset();
	});

	$("#btn_submit_cliente").click(function(){
		$.ajax({
			url:"{{route('storeCliente')}}",
			type:"post",
			dataType:"json",
			data:$("#form_cliente").serialize(),
			beforeSend:function(){
				$("#form_cliente")[0].reset();
			}
			,success:function(e){
				$("#alertas").append('<div class="alert alert-success">Cliente agregado correctamente.</div>');
				setInterval(function(){
					$("#alertas").html('');
					location.reload();
				},3000);
				$("#modal_cliente").modal('hide');
			},error:function(){
				alert("Error al guardar los datos de este cliente, verifique la información proporcionada.");
			}
		});
	});











	$("#tabla_clientes").DataTable({
		"order": [[ 1, 'ASC' ]],
		"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }}});

$("#tabla_clientes_filter input").focus();



	$("#tabla_rutas").DataTable({
		"order": [[ 1, 'ASC' ]],
		"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }}});

// $("#tabla_clientes_filter input").focus();

//##########ocultar tabla de rutas#########################

	$("#btn_ver_rutas").click(function(){
		$("#titulo").html('<b> <i class="fa fa-route"></i> Rutas</b>');
		$("#view_clientes,#btn_ver_rutas").hide();
		$("#view_rutas,#btn_ver_clientes").show();
		$("#tabla_rutas_filter input").focus();
	});
	$("#btn_ver_clientes").click(function(){
		$("#titulo").html('<b><i class="fa fa-users"></i> Clientes </b>');
		$("#view_clientes,#btn_ver_rutas").show();
		$("#view_rutas,#btn_ver_clientes").hide();
		$("#tabla_clientes_filter input").focus();
	});

</script>
@endsection