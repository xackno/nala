<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{strtoupper($cliente->nombre. " ". $cliente->apellidos)}}</title>
</head>
<body>

	@if(isset($recuperaciones))

		@php $total_abono=0;@endphp
		@foreach($recuperaciones as $ab)
			@php $total_abono+=$ab->cantidad; @endphp
		@endforeach

	@endif

	@php $total_cobrar=0;  $con_intereses=0;  @endphp
	@if(isset($id))
		@foreach($pedidos as $r)
			@php
				$desc=0;
				if($r->descuento=='true'){
					$desc=20;
				}else{ $desc=0;}
				$x_cobrar=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$total_cobrar+=$x_cobrar;

				$por_cobrar3=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$fecha_limite=new DateTime($r->fecha_limite);

				if($r->status_recuperacion=="liquidado"){
					$ahora=explode(" ",$r->fechaliquidacion);
					$ahora=new DateTime($ahora[0]);
				}else{
					$ahora=new DateTime(date('Y-m-d'));
				}

				$dias_transcurrido0=0;
				if($fecha_limite < $ahora){
					$diff = $fecha_limite->diff($ahora);
					$dias_transcurrido0=$diff->days;
				}else{
					$dias_transcurrido0=0;
				}

				$intereses=($r->interes/100)*$por_cobrar3;
				$con_intereses+=number_format(($por_cobrar3)+($intereses*$dias_transcurrido0),2);
			@endphp
		@endforeach
	@endif

	@php
		$permiso_set_efectivo="deshabilitado";
		$por_cobrar=0;
		if(isset($con_intereses) &&isset($total_abono)){
			$por_cobrar=$con_intereses-$total_abono;
		}

		


	@endphp




	<table style="width:100%;text-transform: uppercase;">
		<tbody>
			<tr>
				<td class="td"><span style="text-decoration:underline;">{{$cliente->nombre. " ". $cliente->apellidos}}</span></td>
				<td class="td">
						@if(isset($id))
							@if($con_intereses-$total_abono==0 && $total_abono!=0)
							<h4>LIQUIDADO</h4>
							@elseif($con_intereses-$total_abono==0 && $total_abono==0)
							<h4 class="text-success" style="text-decoration:underline;">Sin pedidos</h4>
							@else
							Pendiente:$ <span id="pendiente" style="text-decoration: underline;">{{number_format($con_intereses-$total_abono,2)}}</span> MXN
							@endif
						@endif
					</td>
				<td class="td"><strong style="float:right;text-align:center">
					<div>@if($cliente->identificacion!='' || $cliente->identificacion!=null)
						{!! DNS1D::getBarcodeHTML(str_pad($cliente->id, 8, '0', STR_PAD_LEFT), 'EAN13',3,50) !!}
					@endif</div>
					{{str_pad($cliente->id, 8, '0', STR_PAD_LEFT)}}
				</strong></td>
			</tr>
			<tr>
				<td class="text-secondary td" style="font-size:20px">{{$cliente->localidad.", ".$cliente->municipio}}</td>
				<td class="td"></td>
				<td class="td"></td>
			</tr>
		</tbody>
	</table>

<hr>
<!-- ####################################table-#pedidos############################## -->
	<label>PEDIDOS</label>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="tabla_pedidos">
			<thead class="bg-primary text-center">
				<tr>
					<th>No.Pedido</th>
					<th>fecha/pedido</th>
					<th>Importe</th>
					<th>Envio</th>
					<th>Descuento</th>
					<th>Pago inicial</th>
					<th>Total</th>
					<th>Fecha Entrego</th>
					<th>Atendió</th>
					<th>Factuación</th>
				</tr>
			</thead>
			<tbody>
			@php  $total_descuento=0; $total_importe=0; $total_pago_inicial=0; $total_por_cobrar=0; $total_costo_envio=0; @endphp
				
				@foreach($pedidos as $r)
					@php
					$desc=0;
					if($r->descuento=='true'){
						$desc=20;
					}else{ $desc=0;}

						$por_cobrar=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
						$total_descuento+=$desc;
						$total_importe+=$r->subtotal_pedido;
						$total_pago_inicial+=$r->pago_inicial;
						$total_por_cobrar+=$por_cobrar;
						$total_costo_envio+=$r->costo_envio;
					@endphp
					<tr>
						<td class="text-center"> {{ str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
						<td class="text-center">{{$r->created_at}}</td>
						<td class="text-right">$ {{$r->subtotal_pedido}}</td>
						<td class="text-right">$ {{$r->costo_envio}}</td>
						<td class="text-right">$ {{number_format($desc,2)}}</td>
						<td class="text-right">$ {{$r->pago_inicial}}</td>
						<td class="text-right text-danger" >$ 
							{{number_format($por_cobrar,2)}}
						</td>
						<td class="text-center">
							@php
								$status=explode("=>",$r->status);
							@endphp
							@foreach($status as $s)
								@php
									$nom_status=explode("&",$s);
								@endphp
									@if($nom_status[0]=="recibido_x_pagar" || $nom_status[0]=="recibido_c_devolucion" || $nom_status[0]=="recibido_y_pagado")
										{{$nom_status[1];}}
									@endif
							@endforeach
						</td>
						<td class="text-center">{{$r->cajero}}</td>
						<td class="text-center">
							@if($r->factura=="no" || $r->factura==null)
								{{"NO"}}
							@else
								{{"SI"}}
							@endif
						</td>
					</tr>
				@endforeach
				<tr id="tr_totales">
					<td></td>
					<td></td>
					<td class="text-right table-primary"><b>$ {{number_format($total_importe,2)}}</b></td>
					<td class="text-right table-primary"><b>$ {{number_format($total_costo_envio,2)}}</b></td>
					<td class="text-right table-warning"><b>$ {{number_format($total_descuento,2)}}</b></td>
					<td class="text-right table-success"><b>$ {{number_format($total_pago_inicial,2)}}</b></td>
					<td class="text-right table-danger"><b>$ {{number_format($total_por_cobrar,2)}}</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<br>
			
	</div>
<br>
<hr>
<label>Cálculo de interéses</label>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="tabla_intereses">
			<thead class="bg-primary text-center">
				<tr>
					<th>No.Pedido</th>
					<th>fecha/pedido</th>
					<th>Límite</th>
					<th>%</th>
					<th>TOTAL</th>
					<th>Dias/T</th>
					<th>Int. diarios</th>
					<th>Total int.</th>
					<th>TOTAL c/int.</th>
					<th>pendiente</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach($pedidos as $r)

				<tr >
					<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
					<td class="text-center">{{$r->created_at}}</td>
					<td @if($r->fecha_limite < date("Y-m-d")) class="table-danger" title="vencido" @endif>{{$r->fecha_limite}}</td>
					<td>{{$r->interes}}%</td>
					<td>${{number_format($r->por_cobrar2,2)}}</td>
					<td>@if($r->dias_transcurrido==1){{$r->dias_transcurrido}} Día @else {{$r->dias_transcurrido}} Días @endif</td>
					<td class="text-right">{{number_format($r->intereses_diarios,2)}}</td>
					<td class="text-right">${{number_format($r->intereses,2)}}</td>
					<td class="text-danger text-right"><b> $ {{number_format($r->total_con_intereses,2)}}</b></td>
					<td class="text-right">${{$r->diferencia}}</td>
					<td class="text-center">{{$r->status_recuperacion}} @if($r->status_recuperacion=="liquidado") <p style="margin: 0"><span class="badge badge-primary">{{$r->fechaliquidacion}}</span></p> @endif </td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
<hr>
<!-- ##############################tabla abonos######################################### -->
	<label>ABONOS</label>
	<div class="table-responsive" style="font-size:25px">
		<table class="table table-striped table-bordered" id="tabla_abonos">
			<thead class="bg-success">
				<tr>
					<th>Fecha</th>
					<th>Tipo</th>
					<th>Pedido</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>
				@foreach($recuperaciones as $ab)
				<tr> 
					<td>{{$ab->created_at}}</td>
					<td>{{$ab->tipo}}</td>
					<td>{{str_pad($ab->id_pedido, 8, '0', STR_PAD_LEFT)}}</td>
					<td class="text-right">${{$ab->cantidad}}</td>
				</tr>
				@endforeach
				<tr style="border-top:1.5px solid black !important">
					<td  colspan="3" class="text-right">TOTAL:</td>
					<td class="table-danger text-right">$ {{number_format($total_abono,2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>

<script type="text/php">
    if (isset($pdf)) {
        $x = 490;
        $y = 760;
        $text = "Página {PAGE_NUM}";
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>


<!-- ####################################################################################### -->
	

	<style type="text/css">

		#tabla_abonos{border-collapse: collapse; width: 40%;}
		#tabla_abonos th{background: #73DAA2;}
		#tabla_abonos th,#tabla_abonos td{
			border: 1px solid #666;
		}#tabla_abonos td{
			padding-top: 8px;
			padding-bottom: 8px;
		}

		.text-center{
			text-align: center;
		}
		.text-right{
			text-align: right;
		}
		#tabla_pedidos tr:nth-child(even),#tabla_intereses tr:nth-child(even) { background: #eee }
		#tabla_pedidos,#tabla_intereses {width: 100%;font-size: 20px;}
		html {
		  font-family: 'helvetica neue', helvetica, arial, sans-serif;
		}#tabla_pedidos th,#tabla_intereses th{
			background: #84D4FC;
			padding: 15px !important;
		}
		#tabla_pedidos td,#tabla_intereses td{
			border: .1px solid #aaa;padding-top: 8px;
			padding-bottom: 8px;
		}
		#tabla_pedidos ,#tabla_intereses{
			border-collapse: collapse;
		}
		.td{
			width: 32%;
			/*border: 1px solid black;*/
		}
		.text-secondary{
			color: #666;
		}
		.table-primary{background: #71C1FF;}
		.table-warning{background: #FFD271;}
		.table-success{background: #6DCC8D;}
		.table-danger{background: #FF7A7A;}
	</style>

</body>
</html>
