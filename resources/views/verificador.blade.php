<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VERIFICADOR</title>
    <link rel="shortcut icon" href="{{asset('img/logo1.png')}}">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    body {
        font-family: 'Comfortaa', serif;
        /*font-size: 16px;*/
    } 
    .bg-gradient {
        background-image: linear-gradient(15deg, #1E90FF 0%, #00BFFF  100%);
    }
</style>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-gradient shadow-sm">
        <div class="container">
            <a class="navbar-brand" @guest href="{{ url('/') }} @else href="{{ url('/home') }} @endguest">
               <img src="{{asset('img/logo1.png')}}" style="width:20px">SUPERMERCADO NALA EXPRESS</a>
            </div>
        </div>
    </nav>

<div class="container-fluid">
   <div class="card" style="height:88vh">
        <div class="card-body">
            <form action="{{route('buscar')}}" method="post">
                <div class="input-group col-4">
                    @csrf
                   <input type="text" name="buscar" class="form-control" autofocus autocomplete="off" placeholder="Buscar productos">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button> 
                </div>
            </form>
            @if(!isset($sin_busqueda))
                @if($articulo===null)
                    <div class="alert alert-warning">No se encontró el articulo</div>
                @endif
            @endif
            <!-- ######################################################## -->
            @if(isset($articulo))

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">

                                <p class='text-danger' style="font-size:50pt;"><b>${{$articulo->pre_unidad}}</b></p>

                                @php
                                    $fotos=$articulo->fotos;
                                    $fotos=explode(",",$fotos);
                                    if($fotos[0]==null || $fotos[0]=='ninguno'){
                                        $fotos[0]="no-images.jpg";
                                    }
                                @endphp
                                <div class="doc">
                                    <button class="" id="btn_galeria_left" disabled="true"><i class="fas fa-angle-left"></i></button>
                                    <button class="" id="btn_galeria_right" ><i class="fas fa-angle-right"></i></button>
                                    <div class="box">
                                        <img id="img_principal" src="{{asset('')}}/img/articulos/{{$fotos[0]}}"/>
                                    </div>

                                </div>
                                <div class="text-center" id="div_point"></div>
                            </div>
                            <div class="col">
                                <h1 style="text-transform:uppercase"><b>{{$articulo->descripcion_articulo}}</b></h1>
                                <p class="text-success" style="text-transform:uppercase;">{{$articulo->unidad}}</p>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <!-- <label><b>PRECIO AL PÚBLICO</b></label> -->
                                        
                                    </div>
                                    <div class="col d-none">
                                        <label>MAYOREO APARTIR DE:</label>
                                        <p>{{$articulo->mayoreo_apartir}} <sub>unidades</sub></p>
                                        <h3 class="text-danger">${{$articulo->pre_mayoreo}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div> 
</div>


<style type="text/css">

    .doc{
          display: flex;
          flex-flow: column wrap;
          width: 100%;
          height: 50vh;
          justify-content: center;
          align-items: center;
          /*background: #333944;*/
        }
        .box{
          width: 100%;
          height: 400px;
          /*background: #CCC;*/
          overflow: hidden;
        }


        .box img{
          width: auto;
          max-width: 100%;
          height: auto;
          max-height: 49vh;
        }
        @supports(object-fit: cover){
            .box img{
              height: 100%;
              object-fit: cover;
              object-position: center center;
            }
        }


    #btn_galeria_left{
        position: absolute;
        left: 2px;
        top: 40%;
        border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
    }
    #btn_galeria_right{
        position: absolute;
        right: 2px;
        top: 40%;
        border: 1px solid #ccc;border-radius: 100%;height: 30px;width: 30px; padding: 4px;
    }
</style>





</body>
<script type="text/javascript" src="{{asset('js/jquery.3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    

    //#################################funciones para galeria de imagenes#########################
    @if(isset($articulo))
    var array_imgs="{{$articulo->fotos}}";
    array_imgs=array_imgs.split(",");
    if(array_imgs.length==1){
        $("#btn_galeria_right").attr("disabled","true");
    }else{
        $("#btn_galeria_right").removeAttr("disabled");
    }
    @endif
    var cont_img=0;

    for(var a=0;a<array_imgs.length;a++){
        if(a==0){//agregando puntos el primero coloreado
            $("#div_point").append("<i class='fas fa-dot-circle text-primary' style='font-size:10px;margin:3px'></i>");
        }else{
            $("#div_point").append("<i class='fas fa-dot-circle text-secondary' style='font-size:10px;margin:3px'></i>");
        }
    }
    

    $("#btn_galeria_left").click(function(){
            click_button_left($(this));
    });

    $("#btn_galeria_right").click(function(){
            click_button_right($(this));
    });

    function click_button_left(btn){
        if(cont_img-1==0){
            btn.attr("disabled","true");
        }
        $("#btn_galeria_right").removeAttr("disabled");
            cont_img--;
            $("#img_principal").removeAttr("src");
            $("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
            
            $("#div_point i").removeClass("text-primary");
            $("#div_point i").addClass("text-secondary");

            $("#div_point i").each(function(i){
                if(i==cont_img){
                    $(this).removeClass("text-secondary");
                    $(this).addClass("text-primary");
                }
            });
    }
    function click_button_right(btn){
        if(cont_img+2==array_imgs.length){
                btn.attr("disabled","true");
            }
            $("#btn_galeria_left").removeAttr("disabled");
            cont_img++;
            $("#img_principal").removeAttr("src");

            $("#img_principal").attr("src","{{asset('')}}"+"img/articulos/"+array_imgs[cont_img]);
            $("#img_principal").fadeIn('slow','swing');

            $("#div_point i").removeClass("text-primary");
            $("#div_point i").addClass("text-secondary");

            $("#div_point i").each(function(i){
                if(i==cont_img){
                    $(this).removeClass("text-secondary");
                    $(this).addClass("text-primary");
                }
        });
    }

</script>
</html>

