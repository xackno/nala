@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
   @if(session('msj'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>Advertencia!</strong> {{session('msj')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif







    <div class="row">
       <p class="col-4">Usuario: <b>{{Auth::user()->name}}</b></p>
       <div class="col-3"></div>
        <p class="text-secondary col-5 text-right">FECHA: <b>{{date('d-m-Y')}}</b></p> 
    </div>
    <div class="row">
        <div class="col col-md-4 col-lg-4 col-xl-4">
            <a  href="{{url('/informes_venta')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <img src="{{asset('img/informes2.png')}}" width="100">
                </div>
                <div class="card-footer">
                    <b>INFORME DE VENTAS</b>
                </div>
            </div>
            </a>
        </div>
        <div class="col col-md-4 col-lg-4 col-xl-4">
            <a  href="{{url('/salidas')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <img src="{{asset('img/salidas.png')}}" width="100">
                </div>
                <div class="card-footer">
                    <b>INFORME DE SALIDAS</b>
                </div>
            </div>
            </a>
        </div>

        <div class="col col-md-4 col-lg-4 col-xl-4">
            <a  href="{{url('/turnos')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <img src="{{asset('img/turnos.png')}}" width="100">
                </div>
                <div class="card-footer">
                    <b>INFORMES DE TURNOS</b>
                </div>
            </div>
            </a>
        </div>

    </div>

<div id="footer" class="text-center bg-secondary">
    &copy Xackno, {{date('Y')}}.
</div>

<style type="text/css">
    #footer{
        width: 99%;
        position: absolute;
        bottom: 0;
    }
</style>

@endsection
