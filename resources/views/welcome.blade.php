@extends('layouts.app')

@section('content')
            <div class=" text-center" >
                <img src="{{asset('img/logo1.png')}}" style="width: 250px;">
                <div class="motto text-center">
                    <h1>RETAIL 2</h1>
                    <h3 class="text-secondary">SUPERMERCADO NALA EXPRESS</h3>
                    <br>
                    <a href="{{url('/catalogo')}}" class="btn btn-primary"><i class="fas fa-book"></i> CATÁLOGO</a>
                    <a href="{{route('verificador')}}" class="btn btn-success"><i class="fas fa-search"></i> VERIFICADOR</a>
                    <h4><!-- Sistema de punto de venta --></h4>
                    <p> </p>

                </div>

            </div>
            <div class="text-center">
               <strong style="position:absolute;bottom: 5px;left:43%"  class="text-secondary"> @version &copy Stehs</strong> 
            </div>
            

@endsection