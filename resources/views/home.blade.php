@extends('layouts.app')

@section('content')
<div class="container-fluid" style="background-image: linear-gradient(15deg, #1E90FF 0%, #00BFFF  100%);height:91vh;">
    
   @if(session('msj'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>Advertencia!</strong> {{session('msj')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif







    <div class="row">
       <p class="col-4">Usuario: <b>{{Auth::user()->name}}</b></p>
       <div class="col-3"></div>
        <p class="text-white col-5 text-right">FECHA: <b>{{date('d-m-Y')}}</b></p> 
    </div>
    <div class="row">
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/ventas')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3><img src="{{asset('img/ventas.png')}}" width="100"></h3>
                </div>
                <div class="card-footer">
                    <h3><b>VENTAS</b></h3>
                </div>
            </div>
            </a>
        </div>
        @if(Auth::user()->type=="superadmin")
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/articulos')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3><img src="{{asset('img/articulos.png')}}" width="100"></h3>
                </div>
                <div class="card-footer">
                    <h3><b>ARTÍCULOS</b></h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/inventarios')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3>
                        <i class="fas fa-clipboard-list fas fa-3x text-danger"></i>
                        <!-- <img src="{{asset('img/clientes.png')}}" width="100"> -->
                    </h3>
                </div>
                <div class="card-footer">
                    <h3><b>INVENTARIO</b></h3>
                </div>
            </div>
            </a>
        </div>
        
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/clientes')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3><img src="{{asset('img/clientes.png')}}" width="100"></h3>
                </div>
                <div class="card-footer">
                    <h3><b>CLIENTES</b></h3>
                </div>
            </div>
            </a>
        </div>
        @endif
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/pedidos')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3>
                        <i class="fas fa-book-reader fas fa-3x text-danger"></i>
                        <!-- <img src="{{asset('img/clientes.png')}}" width="100"> -->
                    </h3>
                </div>
                <div class="card-footer">
                    <h3><b>PEDIDOS</b></h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/catalogo')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3>
                        <i class="fas fa-book fa-3x"></i>
                        <!-- <img src="{{asset('img/clientes.png')}}" width="100"> -->
                    </h3>
                </div>
                <div class="card-footer">
                    <h3><b>CATÁLOGO</b></h3>
                </div>
            </div>
            </a>
        </div>
        @if(Auth::user()->type=="superadmin")
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/ofertas')}}" class="text-center">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3>
                        <i class="fas fa-star text-warning fa-3x"></i>
                        <!-- <img src="{{asset('img/clientes.png')}}" width="100"> -->
                    </h3>
                </div>
                <div class="card-footer">
                    <h3><b>OFERTAS</b></h3>
                </div>
            </div>
            </a>
        </div>
        @endif
        @if(Auth::user()->type=="superadmin")
        <!-- <div class="col col-md-3 col-lg-3 col-xl-3 ">
            <a  href="{{url('/cajas')}}" class="text-center btn-link disabled" >
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3><img src="{{asset('img/cajas.png')}}" width="100"></h3>
                </div>
                <div class="card-footer">
                    <h3><b>CAJAS</b></h3>
                </div>
            </div>
            </a>
        </div> -->
        @endif
        <div class="col col-md-3 col-lg-3 col-xl-3">
            <a  href="{{url('/informes')}}" class="text-center" disabled="true">
                <div class="card" style="margin:40px;margin-top:5px">
                <div class="card-body text-center">
                    <h3><img src="{{asset('img/informes.png')}}" width="100"></h3>
                </div>
                <div class="card-footer">
                    <h3><b>INFORMES</b></h3>
                </div>
            </div>
            </a>
        </div>
    </div>

<div id="footer" class="text-center text-white">
   @version &copy Stehs, {{date('Y')}}.
</div>

<style type="text/css">
    #footer{
        width: 99%;
        position: absolute;
        bottom: 0;
    }
</style>

@endsection
