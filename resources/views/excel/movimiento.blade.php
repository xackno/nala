

@if(isset($abonos))

		@php $total_abono=0;@endphp
		@foreach($abonos as $ab)
			@php $total_abono+=$ab->cantidad; @endphp
		@endforeach

	@endif

	@php $total_cobrar=0;  $con_intereses=0;  @endphp
	@if(isset($id))
		@foreach($pedidos as $r)
			@php
				$desc=0;
				if($r->descuento=='true'){
					$desc=20;
				}else{ $desc=0;}
				$x_cobrar=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$total_cobrar+=$x_cobrar;

				$por_cobrar3=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
				$fecha_limite=new DateTime($r->fecha_limite);

				if($r->status_recuperacion=="liquidado"){
					$ahora=explode(" ",$r->fechaliquidacion);
					$ahora=new DateTime($ahora[0]);
				}else{
					$ahora=new DateTime(date('Y-m-d'));
				}

				$dias_transcurrido0=0;
				if($fecha_limite < $ahora){
					$diff = $fecha_limite->diff($ahora);
					$dias_transcurrido0=$diff->days;
				}else{
					$dias_transcurrido0=0;
				}

				$intereses=($r->interes/100)*$por_cobrar3;
				$con_intereses+=number_format(($por_cobrar3)+($intereses*$dias_transcurrido0),2);
			@endphp
		@endforeach
	@endif

	@php
		$permiso_set_efectivo="deshabilitado";
		$por_cobrar=0;
		if(isset($con_intereses) &&isset($total_abono)){
			$por_cobrar=$con_intereses-$total_abono;
		}
	@endphp



<br><br>
<table>
	<tbody>
		<tr style="text-transform: uppercase;">
			<td colspan="4" height="80px">CLIENTE: {{strtoupper($cliente->nombre. " ". $cliente->apellidos)}} <br>{{strtoupper($cliente->localidad.", ".$cliente->municipio)}}</td>
			<td colspan="3">
				@if(isset($id))
							@if($con_intereses-$total_abono==0 && $total_abono!=0)
							<h4>LIQUIDADO</h4>
							@elseif($con_intereses-$total_abono==0 && $total_abono==0)
							<h4 class="text-success" style="text-decoration:underline;">Sin pedidos</h4>
							@else
							Pendiente:$ <span id="pendiente" style="text-decoration: underline;">{{number_format($con_intereses-$total_abono,2)}}</span> MXN
							@endif
						@endif
			</td>
			<td colspan="3">
				<strong style="float:right;text-align:center">
					<div>
						@if($cliente->id!='' || $cliente->id!=null){!! DNS1D::getBarcodeHTML(str_pad($cliente->id, 8, '0', STR_PAD_LEFT), 'EAN13',3,50) !!}@endif
					</div>
					{{str_pad($cliente->id, 8, '0', STR_PAD_LEFT)}}
				</strong>
			</td>
		</tr>
	</tbody>
</table>
<br>
<tr><th colspan="10" style="text-align:center;"><b>PEDIDOS</b></th></tr>

	<table class="table table-striped table-bordered" id="tabla_pedidos">
			<thead class="bg-primary text-center">
				<tr>
					<th width="100px">No.PEDIDO</th>
					<th width="150px">FECHA de PEDIDO</th>
					<th width="80px">IMPORTE</th>
					<th width="80px">ENVIO</th>
					<th width="80px">DESCUENTO</th>
					<th width="80px">PAGO INICIAL</th>
					<th width="80px">TOTAL</th>
					<th width="150px">FECHA ENTREGADO</th>
					<th width="100px">ATENDIO</th>
					<th width="150px">FACTURACIÓN</th>
				</tr>
			</thead>
			<tbody>
			@php  $total_descuento=0; $total_importe=0; $total_pago_inicial=0; $total_por_cobrar=0; $total_costo_envio=0; @endphp
				
				@foreach($pedidos as $r)
					@php
					$desc=0;
					if($r->descuento=='true'){
						$desc=20;
					}else{ $desc=0;}

						$por_cobrar=($r->subtotal_pedido+$r->costo_envio)-($desc + $r->pago_inicial);
						$total_descuento+=$desc;
						$total_importe+=$r->subtotal_pedido;
						$total_pago_inicial+=$r->pago_inicial;
						$total_por_cobrar+=$por_cobrar;
						$total_costo_envio+=$r->costo_envio;
					@endphp
					<tr>
						<td class="text-center"> {{ str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
						<td class="text-center">{{$r->created_at}}</td>
						<td class="text-right">$ {{$r->subtotal_pedido}}</td>
						<td class="text-right">$ {{$r->costo_envio}}</td>
						<td class="text-right">$ {{number_format($desc,2)}}</td>
						<td class="text-right">$ {{$r->pago_inicial}}</td>
						<td class="text-right text-danger" >$ 
							{{number_format($por_cobrar,2)}}
						</td>
						<td class="text-center">
							@php
								$status=explode("=>",$r->status);
							@endphp
							@foreach($status as $s)
								@php
									$nom_status=explode("&",$s);
								@endphp
									@if($nom_status[0]=="recibido_x_pagar" || $nom_status[0]=="recibido_c_devolucion" || $nom_status[0]=="recibido_y_pagado")
										{{$nom_status[1];}}
									@endif
							@endforeach
						</td>
						<td class="text-center">{{$r->cajero}}</td>
						<td class="text-center">
							@if($r->factura=="no" || $r->factura==null)
								{{"NO"}}
							@else
								{{"SI"}}
							@endif
						</td>
					</tr>
				@endforeach
				<tr id="tr_totales">
					<td></td>
					<td></td>
					<td class="text-right table-primary"><b>$ {{number_format($total_importe,2)}}</b></td>
					<td class="text-right table-primary"><b>$ {{number_format($total_costo_envio,2)}}</b></td>
					<td class="text-right table-warning"><b>$ {{number_format($total_descuento,2)}}</b></td>
					<td class="text-right table-success"><b>$ {{number_format($total_pago_inicial,2)}}</b></td>
					<td class="text-right table-danger"><b>$ {{number_format($total_por_cobrar,2)}}</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
<br>
<tr><th colspan="10"><b>CÁLCULO DE INTERÉSES</b></th></tr>
<table >
	<thead >
		<tr>
			<th>No.Pedido</th>
			<th>fecha/pedido</th>
			<th>Límite</th>
			<th>%</th>
			<th>TOTAL</th>
			<th>Dias/T</th>
			<th>Int. diarios</th>
			<th>Total int.</th>
			<th>TOTAL c/int.</th>
			<th>pendiente</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($pedidos as $r)
		<tr >
			<td>{{str_pad($r->id, 8, '0', STR_PAD_LEFT)}}</td>
			<td >{{$r->created_at}}</td>
			<td @if($r->fecha_limite < date("Y-m-d")) class="table-danger" title="vencido" @endif>{{$r->fecha_limite}}</td>
			<td>{{$r->interes}}%</td>
			<td>${{number_format($r->por_cobrar2,2)}}</td>
			<td>@if($r->dias_transcurrido==1){{$r->dias_transcurrido}} Día @else {{$r->dias_transcurrido}} Días @endif</td>
			<td >{{number_format($r->intereses_diarios,2)}}</td>
			<td >${{number_format($r->intereses,2)}}</td>
			<td ><b> $ {{number_format($r->total_con_intereses,2)}}</b></td>
			<td >${{$r->diferencia}}</td>
			<td  width="150px">{{$r->status_recuperacion}} @if($r->status_recuperacion=="liquidado") <p ><span >{{$r->fechaliquidacion}}</span></p> @endif </td>
		</tr>
		@endforeach
	</tbody>
</table>






<tr><th colspan="10" style="text-align:center;"><b>ABONOS</b></th></tr>
<table id="tabla_abonos">
			<thead >
				<tr>
					<th>Fecha</th>
					<th>Tipo</th>
					<th>Pedido</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>
				@foreach($abonos as $ab)
				<tr> 
					<td>{{$ab->created_at}}</td>
					<td>{{$ab->tipo}}</td>
					<td>{{str_pad($ab->id_pedido, 8, '0', STR_PAD_LEFT)}}</td>
					<td >${{$ab->cantidad}}</td>
				</tr>
				@endforeach
				<tr style="border-top:1.5px solid black !important">
					<td  colspan="3" >TOTAL:</td>
					<td >$ {{number_format($total_abono,2)}}</td>
				</tr>
			</tbody>
		</table>


