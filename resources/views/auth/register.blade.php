@extends('layouts.app')

@section('content')
<br><br><br>

<div class="card" style="width: 60%;margin:auto">
    <div class="card-header bg-info text-white"><H3> <i class="fas fa-user"></i> REGISTRAR USUARIOS DE SISTEMA</H3></div>

    <div class="card-body">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre completo">

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>

                <div class="col-md-6">
                    <input id="user" type="text" class="form-control @error('user') is-invalid @enderror" name="user"  required autocomplete="user" autofocus placeholder="Ejem. Juan">

                    @error('user')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Tipo') }}</label>

                <div class="col-md-6">
                    <select class="form-control" name="type" id="type">
                        <option value="superadmin">Super-administrador</option>
                        <option value="administrador">Administrador</option>
                        <option value="vendedor" selected>Vendedor</option>
                    </select>

                </div>
            </div>
            <div class="form-group row" name="div_caja">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Cajas') }}</label>

                <div class="col-md-6">
                    <select name="caja" class="form-control">
                        <option disabled selected>Seleccione una caja</option>
                        <option value="caja1">Caja1</option>
                        <option value="caja2">Caja2</option>
                        <option value="caja3">Caja3</option>
                        <option value="caja4">Caja4</option>
                        <option value="caja5">Caja5</option>
                        <option value="caja6">Caja6</option>
                    </select>

                </div>
            </div>
            <input type="hidden" name="status" id="status" value="enable">

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="ejem. ejemplo@1234.com">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary float-right">
                       <i class="fas fa-user"></i> {{ __('Registrar') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
   
@endsection
@section("script")
<script type="text/javascript">
     $("[name=div_caja]").hide();

    $("#type").change(function(){
        if($(this).val()=="vendedor"){
            $("[name=div_caja]").show();
        }else{
            $("[name=div_caja]").hide();
        }
    });
</script>
@endsection
