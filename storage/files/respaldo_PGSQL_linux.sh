#!/bin/bash
# variables
directorio="/home/respaldos/postgresql";
database="dbbasedatos";
fecha="`date +%Y%m%d%H%M%S`";
usuario="miusuario"
host="11.1.1.1"
export PGPASSWORD=miclave

# generar el dump y crear el log
pg_dump -U $usuario -h $host -F t -d $database|gzip -9 > $directorio/$fecha.tar.gz && echo 'Respaldo realizado con exito.' >> $directorio/bitacora.log;